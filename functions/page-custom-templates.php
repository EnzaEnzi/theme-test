<?php

/*
Name:   Page Custom Template
Description: page pour personnalisé l'affichage du template
Author: Enza Lombardo
Author URI: www.enzalombardo.be
copyright : 2019 © Enza Lombardo
Version: 1.0
*/


/* -------------------------------------------------------------------------- */
/* ADD MENU PAGE */
/* -------------------------------------------------------------------------- */

// initialisation de la page ---------------------------------------------------
add_action('admin_menu', 'add_page_custom_template');

// construire la page ----------------------------------------------------------
function add_page_custom_template(){

    // Menu level 1 ------------------------------------------------------------
    add_menu_page(
        'Template',                             // page_title
        'Template',                             // menu_title
        'manage_options',                       // capability
        'custom_templates',                     // slug_menu
        'theme_page_custom_homepage',            // function qui rendra la sortie
        'dashicons-admin-customizer',           // icon
        90                                      // position
    ); // END -> add_menu_page


    // Menu level 2 ------------------------------------------------------------
    add_submenu_page(
        'custom_templates',                     // slug parent
        'Page albums',                          // page_title
        'Page albums',                          // menu_title
        'manage_options',                       // capability
        'custom_page_albums',                   // slug_menu
        'theme_page_custom_page_albums'         // function
    ); // END -> add_submenu_page

} // END => add_page_custom_template

/* -------------------------------------------------------------------------- */
/* THEME PAGE */
/* -------------------------------------------------------------------------- */

// PAGE LEVEL 1 ----------------------------------------------------------------
function theme_page_custom_homepage(){
    ?>
    <div class="wrap">
        <h2 class="wp-heading-inline">Pesonnaliser la page d'accueil</h2>
        <div class="description">Sur cette page, vous pouvez choisir si tel ou tel élément sera affiché</div>
        <?php settings_errors(); ?>


        <form class="form-custom"  method="post" action="options.php">

            <div class="form-table form-table-custom" >
                <?php settings_fields( 'homepage-group' );?>
            </div>

            <?php
            do_settings_sections( 'custom_templates' );
            submit_button();
            ?>
        </form>


    </div>
    <?php
} // END => theme_page_custom_homepage

// PAGE LEVEL 2 ----------------------------------------------------------------
function theme_page_custom_page_albums(){
    ?>
    <div class="wrap">
        <h2 class="wp-heading-inline">Pesonnaliser la page albums</h2>
        <div class="description">Sur cette page, vous pouvez choisir si tel ou tel élément sera affiché</div>
        <?php settings_errors(); ?>


        <form class="form-custom"  method="post" action="options.php">

            <div class="form-table form-table-custom" >
                <?php settings_fields( 'section-group' );?>
            </div>

            <?php
            do_settings_sections( 'custom_page_albums' );
            submit_button();
            ?>
        </form>


    </div>
    <?php
}


/* -------------------------------------------------------------------------- */
/* PAGE LEVEL 1 -->  SETTING SECTION AND FIED */
/* -------------------------------------------------------------------------- */

// initialisation des paramattre -----------------------------------------------
add_action('admin_init', 'custom_settings_homepage');

// contruire des paramettres ---------------------------------------------------
function custom_settings_homepage(){

    // -------------------------------------------------------------------------
    // SECTION 1 - BANNER (homepage) -------------------------------------------
    // -------------------------------------------------------------------------
    /* --- SECTION --- */
    add_settings_section(
        'section_banner_homepage',                                              // ID (id used to identify the field throughout the theme)
        __('Section 1 - Banner', 'section_banner_homepage'),                    // TITLE (title to be displayed on the administration page)
        'option_section_banner_homepage',                                       // CALLBACK (callback used to render the description of the section)
        'custom_templates'                                                      // PAGE (page on which to add this section of options)
    ); // end -> section_banner_homepage (add_settings_section)

    /* --- FIELDS --- */
    add_settings_field(
        'hiden_banner_homepage',                                                // ID -- ID used to identify the field throughout the theme
        __('Section cachée', 'section_banner_homepage'),                        // LABEL -- The label to the left of the option interface element
        'custom_field_banner_view_homepage',                                    // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'custom_templates',                                                     // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_banner_homepage'                                               // SECTION ID -- The name of the section to which this field belongs
    ); // end -> hiden_banner_homepage (add_settings_field)

    add_settings_field(
        'title_banner_homepage',                                                // ID -- ID used to identify the field throughout the theme
        __('Titre de la Section', 'section_banner_homepage'),                   // LABEL -- The label to the left of the option interface element
        'custom_field_banner_title_homepage',                                   // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'custom_templates',                                                     // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_banner_homepage'                                               // SECTION ID -- The name of the section to which this field belongs
    ); // end -> title_banner_homepage (add_settings_field)

    add_settings_field(
        'description_banner_homepage',                                          // ID -- ID used to identify the field throughout the theme
        __('Texte à afficher', 'section_banner_homepage'),                      // LABEL -- The label to the left of the option interface element
        'custom_field_banner_description_homepage',                             // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'custom_templates',                                                     // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_banner_homepage'                                               // SECTION ID -- The name of the section to which this field belongs
    ); // end -> description_banner_homepage (add_settings_field)

    add_settings_field(
        'button_banner_homepage',                                               // ID -- ID used to identify the field throughout the theme
        __('Bouton', 'section_banner_homepage'),                                // LABEL -- The label to the left of the option interface element
        'custom_field_banner_button_homepage',                                  // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'custom_templates',                                                     // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_banner_homepage'                                               // SECTION ID -- The name of the section to which this field belongs
    ); // end -> button_banner_homepage (add_settings_field)



    // -------------------------------------------------------------------------
    // SECTION 2 - BUFFET (homepage) -------------------------------------------
    // -------------------------------------------------------------------------
    add_settings_section(
        'section_buffet_homepage',                                              // ID (id used to identify the field throughout the theme)
        __('Section 2 - Buffet', 'section_buffet_homepage'),                    // TITLE (title to be displayed on the administration page)
        'option_section_buffet_homepage',                                       // CALLBACK (callback used to render the description of the section)
        'custom_templates'                                                      // PAGE (page on which to add this section of options)
    ); // end -> section_buffet_homepage (add_settings_section)

    /* --- FIELDS --- */
    add_settings_field(
        'hiden_buffet_homepage',                                                // ID -- ID used to identify the field throughout the theme
        __('Section cachée', 'section_buffet_homepage'),                        // LABEL -- The label to the left of the option interface element
        'custom_field_buffet_view_homepage',                                    // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'custom_templates',                                                     // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_buffet_homepage'                                               // SECTION ID -- The name of the section to which this field belongs
    ); // end -> hiden_buffet_homepage (add_settings_field)

    add_settings_field(
        'title_buffet_homepage',                                                // ID -- ID used to identify the field throughout the theme
        __('Titre de la Section', 'section_buffet_homepage'),                   // LABEL -- The label to the left of the option interface element
        'custom_field_buffet_title_homepage',                                   // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'custom_templates',                                                     // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_buffet_homepage'                                               // SECTION ID -- The name of the section to which this field belongs
    ); // end -> title_buffet_homepage (add_settings_field)

    add_settings_field(
        'description_buffet_homepage',                                          // ID -- ID used to identify the field throughout the theme
        __('Texte à afficher', 'section_buffet_homepage'),                      // LABEL -- The label to the left of the option interface element
        'custom_field_buffet_description_homepage',                             // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'custom_templates',                                                     // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_buffet_homepage'                                               // SECTION ID -- The name of the section to which this field belongs
    ); // end -> description_buffet_homepage (add_settings_field)

    add_settings_field(
        'button_buffet_homepage',                                               // ID -- ID used to identify the field throughout the theme
        __('Bouton', 'section_buffet_homepage'),                                // LABEL -- The label to the left of the option interface element
        'custom_field_buffet_button_homepage',                                  // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'custom_templates',                                                     // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_buffet_homepage'                                               // SECTION ID -- The name of the section to which this field belongs
    ); // end -> button_buffet_homepage (add_settings_field)


    // -------------------------------------------------------------------------
    // SECTION 3 - CARTE (homepage) --------------------------------------------
    // -------------------------------------------------------------------------
    add_settings_section(
        'section_carte_homepage',                                               // ID (id used to identify the field throughout the theme)
        __('Section 3 - Carte', 'section_carte_homepage'),                      // TITLE (title to be displayed on the administration page)
        'option_section_carte_homepage',                                        // CALLBACK (callback used to render the description of the section)
        'custom_templates'                                                      // PAGE (page on which to add this section of options)
    ); // end -> section_carte_homepage (add_settings_section)

    /* --- FIELDS --- */
    add_settings_field(
        'hiden_carte_homepage',                                                 // ID -- ID used to identify the field throughout the theme
        __('Section cachée', 'section_carte_homepage'),                         // LABEL -- The label to the left of the option interface element
        'custom_field_carte_view_homepage',                                     // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'custom_templates',                                                     // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_carte_homepage'                                                // SECTION ID -- The name of the section to which this field belongs
    ); // end -> hiden_carte_homepage (add_settings_field)

    add_settings_field(
        'title_carte_homepage',                                                 // ID -- ID used to identify the field throughout the theme
        __('Titre de la Section', 'section_carte_homepage'),                    // LABEL -- The label to the left of the option interface element
        'custom_field_carte_title_homepage',                                    // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'custom_templates',                                                     // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_carte_homepage'                                                // SECTION ID -- The name of the section to which this field belongs
    ); // end -> title_carte_homepage (add_settings_field)

    add_settings_field(
        'description_carte_homepage',                                           // ID -- ID used to identify the field throughout the theme
        __('Texte à afficher', 'section_carte_homepage'),                       // LABEL -- The label to the left of the option interface element
        'custom_field_carte_description_homepage',                              // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'custom_templates',                                                     // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_carte_homepage'                                                // SECTION ID -- The name of the section to which this field belongs
    ); // end -> description_carte_homepage (add_settings_field)

    add_settings_field(
        'button_carte_homepage',                                                // ID -- ID used to identify the field throughout the theme
        __('Bouton', 'section_carte_homepage'),                                 // LABEL -- The label to the left of the option interface element
        'custom_field_carte_button_homepage',                                   // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'custom_templates',                                                     // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_carte_homepage'                                                // SECTION ID -- The name of the section to which this field belongs
    ); // end -> button_carte_homepage (add_settings_field)


    // -------------------------------------------------------------------------
    // SECTION 4 - EVENT (homepage) --------------------------------------------
    // -------------------------------------------------------------------------

    add_settings_section(
        'section_event_homepage',                                               // ID (id used to identify the field throughout the theme)
        __('Section 3 - event', 'section_event_homepage'),                      // TITLE (title to be displayed on the administration page)
        'option_section_event_homepage',                                        // CALLBACK (callback used to render the description of the section)
        'custom_templates'                                                      // PAGE (page on which to add this section of options)
    ); // end -> section_event_homepage (add_settings_section)

    /* --- FIELDS --- */
    add_settings_field(
        'hiden_event_homepage',                                                 // ID -- ID used to identify the field throughout the theme
        __('Section cachée', 'section_event_homepage'),                         // LABEL -- The label to the left of the option interface element
        'custom_field_event_view_homepage',                                     // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'custom_templates',                                                     // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_event_homepage'                                                // SECTION ID -- The name of the section to which this field belongs
    ); // end -> hiden_event_homepage (add_settings_field)

    add_settings_field(
        'title_event_homepage',                                                 // ID -- ID used to identify the field throughout the theme
        __('Titre de la Section', 'section_event_homepage'),                    // LABEL -- The label to the left of the option interface element
        'custom_field_event_title_homepage',                                    // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'custom_templates',                                                     // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_event_homepage'                                                // SECTION ID -- The name of the section to which this field belongs
    ); // end -> title_event_homepage (add_settings_field)



    // -------------------------------------------------------------------------
    // REGISTER  (homepage) ----------------------------------------------------
    // -------------------------------------------------------------------------
    /* --- homepage-section-banner --- */
    register_setting('homepage-group', 'banner_hiden');
    register_setting('homepage-group', 'banner_title');
    register_setting('homepage-group', 'banner_description');
    register_setting('homepage-group', 'banner_btn_actif');
    register_setting('homepage-group', 'banner_btn_txt');
    register_setting('homepage-group', 'banner_btn_url');

    /* --- homepage-section-buffet --- */
    register_setting('homepage-group', 'buffet_hiden');
    register_setting('homepage-group', 'buffet_title');
    register_setting('homepage-group', 'buffet_description');
    register_setting('homepage-group', 'buffet_btn_actif');
    register_setting('homepage-group', 'buffet_btn_txt');
    register_setting('homepage-group', 'buffet_btn_url');

    /* --- homepage-section-carte --- */
    register_setting('homepage-group', 'carte_hiden');
    register_setting('homepage-group', 'carte_title');
    register_setting('homepage-group', 'carte_description');
    register_setting('homepage-group', 'carte_btn_actif');
    register_setting('homepage-group', 'carte_btn_txt');
    register_setting('homepage-group', 'carte_btn_url');

    /* --- homepage-section-event --- */
    register_setting('homepage-group', 'event_hiden');
    register_setting('homepage-group', 'event_title');

} // END => custom_settings_homepage

/* -------------------------------------------------------------------------- */
/* PAGE LEVEL 1 -->  FIELD CALLBACK */
/* -------------------------------------------------------------------------- */

// SECTION 1 - BANNER ----------------------------------------------------------
/* --- SECTION --- */
function option_section_banner_homepage(){}

/* --- FIELDS --- */
function custom_field_banner_view_homepage(){
    $banner_hiden = esc_attr(get_option('banner_hiden'));
    ?>
        <div class="">
            <input type="checkbox" id="banner_hiden" name="banner_hiden" value="1" <?php checked(1, get_option('banner_hiden'), true); ?> />
            <span>Masquer cette section sur la page d'acceuil</span>
        </div>
    <?php

} // END => custom_field_view_homepage

function custom_field_banner_title_homepage(){
    $banner_title = esc_attr(get_option('banner_title'));
    ?>
        <div class="">
            <input type="text" id="banner_title" name="banner_title" value="<?php echo( get_option('banner_title') ); ?>" /> Insérer un titre pour cette section
        </div>
    <?php
} // END => custom_field_title_homepage

function custom_field_banner_description_homepage(){
    $banner_description = esc_attr(get_option('banner_description'));
    ?>
        <div class="">
            <textarea id="banner_description" name="banner_description" ><?php echo( get_option('banner_description') ); ?></textarea>
        </div>
    <?php
} // END => custom_field_description_homepage

function custom_field_banner_button_homepage(){
    $banner_btn_actif = esc_attr( get_option('banner_btn_actif') );
    $banner_btn_txt = esc_attr(get_option('banner_btn_txt'));
    $banner_btn_url = esc_attr(get_option('banner_btn_url'));
    ?>
        <table>
            <tbody>
                <tr>
                    <td>
                        <input type="checkbox" id="banner_btn_actif" name="banner_btn_actif" value="1" <?php checked(1, get_option('banner_btn_actif'), true); ?> />
                        <span>Afficher le bouton</span>
                    </td>
                </tr>
                <tr>
                    <td><label for="">Texte</label></td>
                    <td><input type="text" id="banner_btn_txt" name="banner_btn_txt" value="<?php echo( get_option('banner_btn_txt') ); ?>" /></td>
                </tr>
                <tr>
                    <td><label for="">Lien (url)</label></td>
                    <td><input type="text" id="banner_btn_url" name="banner_btn_url" value="<?php echo( get_option('banner_btn_url') ); ?>" /></td>
                </tr>
            </tbody>
        </table>

    <?php
} // END => custom_field_button_homepage

// SECTION 2 - BUFFET ----------------------------------------------------------
/* --- SECTION --- */
function option_section_buffet_homepage(){}

/* --- FIELDS --- */
function custom_field_buffet_view_homepage(){
    $buffet_hiden = esc_attr(get_option('buffet_hiden'));
    ?>
        <div class="">
            <input type="checkbox" id="buffet_hiden" name="buffet_hiden" value="1" <?php checked(1, get_option('buffet_hiden'), true); ?> />
            <span>Masquer cette section sur la page d'acceuil</span>
        </div>
    <?php

} // END => custom_field_view_homepage

function custom_field_buffet_title_homepage(){
    $buffet_title = esc_attr(get_option('buffet_title'));
    ?>
        <div class="">
            <input type="text" id="buffet_title" name="buffet_title" value="<?php echo( get_option('buffet_title') ); ?>" /> Insérer un titre pour cette section
        </div>
    <?php
} // END => custom_field_title_homepage

function custom_field_buffet_description_homepage(){
    $buffet_description = esc_attr(get_option('buffet_description'));
    ?>
        <div class="">
            <textarea id="buffet_description" name="buffet_description" ><?php echo( get_option('buffet_description') ); ?></textarea>
        </div>
    <?php
} // END => custom_field_description_homepage

function custom_field_buffet_button_homepage(){
    $buffet_btn_actif = esc_attr( get_option('buffet_btn_actif') );
    $buffet_btn_txt = esc_attr(get_option('buffet_btn_txt'));
    $buffet_btn_url = esc_attr(get_option('buffet_btn_url'));
    ?>
        <table>
            <tbody>
                <tr>
                    <td>
                        <input type="checkbox" id="buffet_btn_actif" name="buffet_btn_actif" value="1" <?php checked(1, get_option('buffet_btn_actif'), true); ?> />
                        <span>Afficher le bouton</span>
                    </td>
                </tr>
                <tr>
                    <td><label for="">Texte</label></td>
                    <td><input type="text" id="buffet_btn_txt" name="buffet_btn_txt" value="<?php echo( get_option('buffet_btn_txt') ); ?>" /></td>
                </tr>
                <tr>
                    <td><label for="">Lien (url)</label></td>
                    <td><input type="text" id="buffet_btn_url" name="buffet_btn_url" value="<?php echo( get_option('buffet_btn_url') ); ?>" /></td>
                </tr>
            </tbody>
        </table>

    <?php
} // END => custom_field_button_homepage

// SECTION 3 - CARTE -----------------------------------------------------------
/* --- SECTION --- */
function option_section_carte_homepage(){}

/* --- FIELDS --- */
function custom_field_carte_view_homepage(){
    $carte_hiden = esc_attr(get_option('carte_hiden'));
    ?>
        <div class="">
            <input type="checkbox" id="carte_hiden" name="carte_hiden" value="1" <?php checked(1, get_option('carte_hiden'), true); ?> />
            <span>Masquer cette section sur la page d'acceuil</span>
        </div>
    <?php

} // END => custom_field_view_homepage

function custom_field_carte_title_homepage(){
    $carte_title = esc_attr(get_option('carte_title'));
    ?>
        <div class="">
            <input type="text" id="carte_title" name="carte_title" value="<?php echo( get_option('carte_title') ); ?>" /> Insérer un titre pour cette section
        </div>
    <?php
} // END => custom_field_title_homepage

function custom_field_carte_description_homepage(){
    $carte_description = esc_attr(get_option('carte_description'));
    ?>
        <div class="">
            <textarea id="carte_description" name="carte_description" ><?php echo( get_option('carte_description') ); ?></textarea>
        </div>
    <?php
} // END => custom_field_description_homepage

function custom_field_carte_button_homepage(){
    $carte_btn_actif = esc_attr( get_option('carte_btn_actif') );
    $carte_btn_txt = esc_attr(get_option('carte_btn_txt'));
    $carte_btn_url = esc_attr(get_option('carte_btn_url'));
    ?>
        <table>
            <tbody>
                <tr>
                    <td>
                        <input type="checkbox" id="carte_btn_actif" name="carte_btn_actif" value="1" <?php checked(1, get_option('carte_btn_actif'), true); ?> />
                        <span>Afficher le bouton</span>
                    </td>
                </tr>
                <tr>
                    <td><label for="">Texte</label></td>
                    <td><input type="text" id="carte_btn_txt" name="carte_btn_txt" value="<?php echo( get_option('carte_btn_txt') ); ?>" /></td>
                </tr>
                <tr>
                    <td><label for="">Lien (url)</label></td>
                    <td><input type="text" id="carte_btn_url" name="carte_btn_url" value="<?php echo( get_option('carte_btn_url') ); ?>" /></td>
                </tr>
            </tbody>
        </table>

    <?php
} // END => custom_field_button_homepage

// SECTION 4 - EVENT -----------------------------------------------------------

/* --- SECTION --- */
function option_section_event_homepage(){}

/* --- FIELDS --- */
function custom_field_event_view_homepage(){
    $event_hiden = esc_attr(get_option('event_hiden'));
    ?>
        <div class="">
            <input type="checkbox" id="event_hiden" name="event_hiden" value="1" <?php checked(1, get_option('event_hiden'), true); ?> />
            <span>Masquer cette section sur la page d'acceuil</span>
        </div>
    <?php

} // END => custom_field_view_homepage

function custom_field_event_title_homepage(){
    $event_title = esc_attr(get_option('event_title'));
    ?>
        <div class="">
            <input type="text" id="event_title" name="event_title" value="<?php echo( get_option('event_title') ); ?>" /> Insérer un titre pour cette section
        </div>
    <?php
} // END => custom_field_title_homepage


/* -------------------------------------------------------------------------- */
/* PAGE LEVEL 2 -->  SETTING SECTION AND FIED */
/* -------------------------------------------------------------------------- */



/* -------------------------------------------------------------------------- */
/* PAGE LEVEL 2 -->  FIELD CALLBACK */
/* -------------------------------------------------------------------------- */
