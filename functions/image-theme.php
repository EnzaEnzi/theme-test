<?php
/*
Name:   Page image theme
Description: cette permet d'ajouté les image du theme
Author: Enza Lombardo
Author URI: www.enzalombardo.be
copyright : 2019 © Enza Lombardo
Version: 1.8
*/


/* -------------------------------------------------------------------------- */
/* ADD MENU PAGE */
/* -------------------------------------------------------------------------- */

// initialisation de la page ---------------------------------------------------
add_action('admin_menu', 'add_page_image_theme');

// construire la page ----------------------------------------------------------
function add_page_image_theme(){

    // Menu level 1 ------------------------------------------------------------
    add_menu_page(
        'Image Theme',                      // page_title
        'Image Theme',                      // menu_title
        'manage_options',                   // capability
        'image-theme',                      // slug_menu
        'theme_page_image_theme',           // function qui rendra la sortie
        'dashicons-visibility',             // icon
        121                                 // position
    );  // END -> add_menu_page

} // end -> add_page_image_theme

/* -------------------------------------------------------------------------- */
/* THEME PAGE */
/* -------------------------------------------------------------------------- */

// PAGE LEVEL 1 ----------------------------------------------------------------
function theme_page_image_theme(){
    ?>
    <div class="wrap">
        <h2 class="wp-heading-inline">Image theme</h2>
        <div class="description">Page pour gerer les différentes image du theme</div>
        <?php settings_errors(); ?>

        <form method="post" action="options.php" enctype="multipart/form-data">
            <div class="">
                <?php settings_fields("image-group");?>
            </div><!-- / -->

            <?php
                do_settings_sections("image-theme");
                submit_button();
             ?>
        </form><!-- / -->
    </div><!-- / .wrap -->
    <?php
} // end -> theme_page_image_theme



/* -------------------------------------------------------------------------- */
/* PAGE LEVEL 1 -->  SETTING SECTION AND FIED */
/* -------------------------------------------------------------------------- */

// initialisation des paramattre -----------------------------------------------
add_action("admin_init", "display_image_theme");

// contruire des paramettres ---------------------------------------------------
function display_image_theme(){

    /* ---------------------------------------------------------------------- */
    // 1ER SECTION
    /* ---------------------------------------------------------------------- */

    /* --- SECTION --- */
    add_settings_section(
        'section_image_theme', // ID
        __('Section 1 - xxx', 'section_image_theme'), // TITLE
        'option_section_image_theme', // CALLBACK
        'image-theme' // PAGE
    ); // end -> section : section_image_theme


    /* --- FIELDS --- */
    //add a new setting for file upload
    add_settings_field(
        'bg_image_theme', // ID
        __('Ajouter une image', 'section_image_theme'), // LABEL
        'field_bg_image_theme', // CALLBACK FUNCTION
        'image-theme', // MENU PAGE SLUG
        'section_image_theme' // SECTION ID
    ); // end -> field : bg_image_theme


    /* --- REGISTER --- */
    //register a callback which will retirve the url to the file and then save the value.
    //register_setting only saves the value attribute of the form field....if you need anything more than that then use the callback to find the value t be stored.
    register_setting(
        'image-group',    // group (element display in the form)
        'bg_image_theme', // field ID
        'handle_bg_image_theme' // Callback function
    ); // end -> register : handle_bg_image_theme

    /* ---------------------------------------------------------------------- */
    // 2e SECTION
    /* ---------------------------------------------------------------------- */

    /* --- SECTION --- */
    add_settings_section(
        'section_disney', // ID
        __('Section 2 - Disney', 'section_disney'), // TITLE
        'option_section_disney', // CALLBACK
        'image-theme' // PAGE
    ); // end -> section : section_disney


    /* --- FIELDS --- */
    add_settings_field(
        'display_bg_disney', // ID
        __('Afficher l\'image', 'section_disney'), // LABEL
        'custom_display_bg_disney', // CALLBACK FUNCTION
        'image-theme', // MENU PAGE SLUG
        'section_disney' // SECTION ID
    ); // end -> field : bg_disney

    //add a new setting for file upload
    add_settings_field(
        'bg_disney', // ID
        __('Ajouter une image', 'section_disney'), // LABEL
        'field_bg_disney', // CALLBACK FUNCTION
        'image-theme', // MENU PAGE SLUG
        'section_disney' // SECTION ID
    ); // end -> field : bg_disney


    /* --- REGISTER --- */
    //register a callback which will retirve the url to the file and then save the value.
    //register_setting only saves the value attribute of the form field....if you need anything more than that then use the callback to find the value t be stored.
    register_setting(
        'image-group',    // group (element display in the form)
        'bg_disney', // field ID
        'handle_bg_disney' // Callback function
    ); // end -> register : handle_bg_disney

    register_setting('image-group', 'display_bg_disney');

} // end -> display_image_theme

/* -------------------------------------------------------------------------- */
/* PAGE LEVEL 1 -->  FIELD CALLBACK */
/* -------------------------------------------------------------------------- */

/* --- CALLBACK SECTION --- */
function option_section_image_theme(){
    ?>
        <p>Mettre une description de la section ICI</p>
    <?php
} // END => option_section_image_theme


/* --- CALLBACK REGISTER --- */
function handle_bg_image_theme($options){
    //check if user had uploaded a file and clicked save changes button
    if(!empty($_FILES["bg_image_theme"]["tmp_name"])){
        $urls = wp_handle_upload($_FILES["bg_image_theme"], array('test_form' => FALSE));
        $temp = $urls["url"];
        return $temp;
    } // end -> if(!empty($_FILES["bg_image_theme"]["tmp_name"]))

    //no upload. old file url is the new value.
    return get_option("bg_image_theme");

} // END => handle_bg_image_theme


/* --- CALLBACK FIELDS --- */
function field_bg_image_theme(){
    //echo form element for file upload

    ?>
    <div class="">
        <input type="file" name="bg_image_theme" id="bg_image_theme" value="<?php echo get_option('bg_image_theme'); ?>" />
        <?php echo get_option("bg_image_theme"); ?>
    </div>
    <div class="">
        <img src="<?php echo get_option("bg_image_theme"); ?>" alt="" style="width: 200px; height:200px;" />
    </div>
    <?php
} // END => field_bg_image_theme


/* ---------------------------------------------------------------------- */
    // 2e SECTION
    /* ---------------------------------------------------------------------- */
    /* --- CALLBACK SECTION --- */
function option_section_disney(){
    ?>
        <p>Mettre une description de la section ICI</p>
    <?php
} // END => option_section_disney


/* --- CALLBACK REGISTER --- */
function handle_bg_disney($options){
    //check if user had uploaded a file and clicked save changes button
    if(!empty($_FILES["bg_disney"]["tmp_name"])){
        $urls = wp_handle_upload($_FILES["bg_disney"], array('test_form' => FALSE));
        $temp = $urls["url"];
        return $temp;
    } // end -> if(!empty($_FILES["bg_disney"]["tmp_name"]))

    //no upload. old file url is the new value.
    return get_option("bg_disney");

} // END => handle_bg_disney


/* --- CALLBACK FIELDS --- */
function custom_display_bg_disney(){
    $display_bg_disney = esc_attr(get_option('display_bg_disney'));
    ?>
    <div class="">
        <input type="checkbox" id="display_bg_disney" name="display_bg_disney" value="1" <?php checked(1, get_option('display_bg_disney'), true); ?> />
        <span>OUI Afficher l'image <?php echo get_option("bg_disney"); ?></span>
    </div>
    <?php

} // END => custom_display_bg_disney

function field_bg_disney(){
    //echo form element for file upload

    ?>
    <div class="">
        <input type="file" name="bg_disney" id="bg_disney" value="<?php echo get_option('bg_disney'); ?>" />
        <?php //echo get_option("bg_disney"); ?>
    </div>
    <div class="">
        <img src="<?php echo get_option("bg_disney"); ?>" alt="" />
    </div>
    <?php
} // END => field_bg_disney
