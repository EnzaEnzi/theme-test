<?php
/*
Name:   Page Custom Template
Description: page pour personnalisé l'affichage du template
Author: Enza Lombardo
Author URI: www.enzalombardo.be
copyright : 2019 © Enza Lombardo
Version: 1.0
*/


/* -------------------------------------------------------------------------- */
/* ADD MENU PAGE */
/* -------------------------------------------------------------------------- */

// initialisation de la page ---------------------------------------------------
add_action('admin_menu', 'add_page_config_page');

// construire la page ----------------------------------------------------------
function add_page_config_page(){

    // Menu level 1 ------------------------------------------------------------
    add_menu_page(
        'Config Page',                             // page_title
        'Config Page',                             // menu_title
        'manage_options',                       // capability
        'config_pages',                     // slug_menu
        'theme_page_config_page',            // function qui rendra la sortie
        '',           // icon
        90                                      // position
    ); // END -> add_menu_page


} // END => add_page_config_page

/* -------------------------------------------------------------------------- */
/* THEME PAGE */
/* -------------------------------------------------------------------------- */

// PAGE LEVEL 1 ----------------------------------------------------------------
function theme_page_config_page(){
    ?>
    <div class="wrap">
        <h2 class="wp-heading-inline">Configuration des pages</h2>
        <div class="description">Sur cette page, vous pouvez choisir si tel ou tel élément sera affiché</div>
        <?php settings_errors(); ?>


        <form class="form-custom"  method="post" action="options.php">

            <div class="form-table form-table-custom" >
                <?php settings_fields( 'config-group' );?>
            </div>

            <?php
            do_settings_sections( 'config_pages' );
            submit_button();
            ?>
        </form>


    </div>
    <?php
} // END => theme_page_config_page



/* -------------------------------------------------------------------------- */
/* PAGE LEVEL 1 -->  SETTING SECTION AND FIED */
/* -------------------------------------------------------------------------- */

// initialisation des paramattre -----------------------------------------------
add_action('admin_init', 'custom_settings_config_page');

// contruire des paramettres ---------------------------------------------------
function custom_settings_config_page(){

    // -------------------------------------------------------------------------
    // SECTION 1 -  (config_page) -------------------------------------------
    // -------------------------------------------------------------------------
    /* --- SECTION --- */
    add_settings_section(
        'section_config_page',                                              // ID (id used to identify the field throughout the theme)
        __('Section 1', 'section_config_page'),                    // TITLE (title to be displayed on the administration page)
        'option_section_config_page',                                       // CALLBACK (callback used to render the description of the section)
        'config_pages'                                                      // PAGE (page on which to add this section of options)
    ); // end -> section_config_page (add_settings_section)

    /* --- FIELDS --- */
    add_settings_field(
        'affiche_avatar_config_page',                                                // ID -- ID used to identify the field throughout the theme
        __('Afficher l\'avatar', 'section_config_page'),                        // LABEL -- The label to the left of the option interface element
        'custom_field_avatar_config_page',                                    // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'config_pages',                                                     // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_config_page'                                               // SECTION ID -- The name of the section to which this field belongs
    ); // end -> affiche_avatar_config_page (add_settings_field)

    add_settings_field(
        'description_config_page',                                                // ID -- ID used to identify the field throughout the theme
        __('Description', 'section_config_page'),                        // LABEL -- The label to the left of the option interface element
        'custom_field_description_config_page',                                    // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'config_pages',                                                     // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_config_page'                                               // SECTION ID -- The name of the section to which this field belongs
    ); // end -> description_config_page (add_settings_field)

    /* --- FIELDS --- */
    register_setting('config-group', 'config_affiche_avatar');
    register_setting('config-group', 'config_description');


} // END => custom_settings_config_page

/* -------------------------------------------------------------------------- */
/* PAGE LEVEL 1 -->  FIELD CALLBACK */
/* -------------------------------------------------------------------------- */

// SECTION 1 -  ----------------------------------------------------------
/* --- SECTION --- */
function option_section_config_page(){}

/* --- FIELDS --- */
function custom_field_avatar_config_page(){
    $config_affiche_avatar = esc_attr(get_option('config_affiche_avatar'));
    ?>
    <div class="">
        <input type="checkbox" id="config_affiche_avatar" name="config_affiche_avatar" value="1" <?php checked(1, get_option('config_affiche_avatar'), true); ?> />
        <span>Aficher l'avatar dans cette section</span>
    </div>

    <?php

} // END => custom_field_avatar_config_page

function custom_field_description_config_page(){
    $config_description = esc_attr(get_option('config_description'));


    $content = get_option('config_description');
    wp_editor($content, 'config_description', array('media_buttons'=> false ,'textarea_rows'=>12, 'editor_class'=>'config_description_class'));

    ?>
    <?php
} // END => custom_field_description_config_page
