<?php

/*
Plugin Name: TEST add-fields
Plugin URI:
Description:
Author: Enza Lombardo
Author URI: www.enzalombardo.be
Copyright: 2019 © Enza Lombardo
Version: 1.5
*/

// 1 - Initialer la metabox ----------------------------------------------------

add_action('admin_init', 'add_meta_boxes', 1);

function add_meta_boxes() {
	add_meta_box(
        'metabox_repeatable_titre',						// ID_META_BOX
        'Playlist Audio',								// TITLE_META_BOX
        'repeatable_metabox_titre',						// CALLBACK
        'albums',										// WP_SCREEN
        'normal',										// CONTEXT [ normal | advanced | side ]
        'default'									    // PRIORITY [ high | core | default | low ]
    );
} // END ==> add_meta_boxes

// 2 -  construction de la metabox ---------------------------------------------
function repeatable_metabox_titre($POST) {
    	// global $POST;
    	$repeatable_fields = get_post_meta($POST->ID, 'repeatable_fields', true);
    	wp_nonce_field( 'repeatable_metabox_nonce', 'repeatable_metabox_nonce' );
    ?>

    <!-- Code HTML ICI -->

	<!-- START : JAVASCRIPT -->
	<script type="text/javascript">
        jQuery(document).ready(function($) {
        	$('.btn-save').click(function(e) {
        		e.preventDefault();
        		$('#publish').click();
        	}); // END -> click .btn-save

        	$('#add-row').on('click', function() {
        		var row = $('.empty-row.screen-reader-text').clone(true);
        		row.removeClass('empty-row screen-reader-text');
        		row.insertBefore('#table-repeatable-fieldset tbody>tr:last');
        		return false;
        	}); // END -> on click #add-row

        	$('.remove-row').on('click', function() {
        		$(this).parents('tr').remove();
        		return false;
        	}); // END -> on click .remove-row

        	// $('#table-repeatable-fieldset tbody').sortable({
        	// 	opacity: 0.6,
        	// 	revert: true,
        	// 	cursor: 'move',
        	// 	handle: '.sort'
        	// });// END -> sortable #table-repeatable-fieldset
        });
	</script><!-- /javascript -->


	<!-- START : table -->
	<table id="table-repeatable-fieldset" width="100%">
		<thead>
			<tr>
				<th width="5%"></th>
				<th width="15%">Numéro</th>
				<th width="60%">Nom</th>
				<th width="15%">Prix</th>
			</tr>
		</thead>
		<tbody>
		<?php
			if ( $repeatable_fields ) :
				foreach ( $repeatable_fields as $field ) {
			?>
					<tr>
						<td><a class="button remove-row" href="#">-</a></td>
						<td><input type="text" class="widefat" name="numb[]" value="<?php if($field['numb'] != '') echo esc_attr( $field['numb'] ); ?>" /></td>
						<td><input type="text" class="widefat" name="nom[]" value="<?php if ($field['nom'] != '') echo esc_attr( $field['nom'] );  ?>" /></td>
						<td><input type="text" class="widefat" name="price[]" value="<?php if ($field['price'] != '') echo esc_attr( $field['price'] );  ?>" /></td>
						<!-- <td><a class="sort">|||</a></td> -->

					</tr>
					<?php
							}
						else :
							// show a blank one
					?>
					<tr>
						<td><a class="button remove-row" href="#">-</a></td>
						<td><input type="text" class="widefat" name="numb[]" /></td>
						<td><input type="text" class="widefat" name="nom[]" value="" /></td>
						<td><input type="text" class="widefat" name="price[]" value="" /></td>
						<!-- <td><a class="sort">|||</a></td> -->
					</tr>
				<?php endif; ?>

				<!-- empty hidden one for jQuery -->
				<tr class="empty-row screen-reader-text">
					<td><a class="button remove-row" href="#">-</a></td>
					<td><input type="text" class="widefat" name="numb[]" /></td>
					<td><input type="text" class="widefat" name="nom[]" value="" /></td>
					<td><input type="text" class="widefat" name="price[]" value="" /></td>
					<!-- <td><a class="sort">|||</a></td> -->

				</tr><!-- / .empty-row .screen-reader-text-->
		</tbody><!-- / -->
	</table><!-- / #table-repeatable-fieldset -->

	<!-- START : bouton -->
	<p>
		<a id="add-row" class="button button-large" href="#">Ajouter</a>
		<input type="submit" class="button btn-save button-large" value="Sauvegarder" />
	</p><!-- / bouton -->

	<?php
} // END ==> repeatable_metabox_titre


// 3 - Sauvegarder la metabox --------------------------------------------------
add_action('save_post', 'repeatable_meta_box_save');
function repeatable_meta_box_save($POST_ID) {
	if ( ! isset( $_POST['repeatable_metabox_nonce'] ) ||
		! wp_verify_nonce( $_POST['repeatable_metabox_nonce'], 'repeatable_metabox_nonce' ) )
		return;

	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;

	if (!current_user_can('edit_post', $POST_ID))
		return;
	$old = get_post_meta($POST_ID, 'repeatable_fields', true);
	$new = array();
	$numbs = $_POST['numb'];
	$noms = $_POST['nom'];
	$prices = $_POST['price'];
	$count = count( $numbs );

	for ( $i = 0; $i < $count; $i++ ) {
		if ( $numbs[$i] != '' ) :
			$new[$i]['numb'] = stripslashes( strip_tags( $numbs[$i] ) );
		if ( $noms[$i] == '' )
			$new[$i]['nom'] = '';
		else
			$new[$i]['nom'] = stripslashes( $noms[$i] ); // and however you want to sanitize
		if ( $prices[$i] == '' )
			$new[$i]['price'] = '';
		else
				$new[$i]['price'] = stripslashes( $prices[$i] ); // and however you want to sanitize
		endif;
	}

	if ( !empty( $new ) && $new != $old )
		update_post_meta( $POST_ID, 'repeatable_fields', $new );
	elseif ( empty($new) && $old )
		delete_post_meta( $POST_ID, 'repeatable_fields', $old );
} // END ==> repeatable_meta_box_save
