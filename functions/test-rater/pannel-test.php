<?php
// Créer un pannel de teste



add_action('admin_menu', 'pannel_test');

function pannel_test(){
    add_menu_page(
        'Panel test',             // titre de la page
        'Panel test',             // titre du menu
        'activate_plugin',          // niveau d’autorisation utilisateur pour voir le menu
        'panel-test',               // nom référence du menu
        'render_pannel',            // fonction d’affiche de la page
        'dashicons-pressthis',      // icône du menu
        81                          // position dans l’interface d’administration
    );
}

function render_pannel(){
    if(isset($_POST['pannel_update'])){
        // update_option('adresse', $_POST['adresse']);
        // update_option('phone', $_POST['phone']);

        if (!wp_verify_nonce($_POST['pannel_noncename'], 'pannel_test')) {
            die('Token non valide');
        }
        foreach($_POST['options'] as $name => $value){
            if(empty($value)){
                delete_option($name);
            }else{
                update_option($name, $value);
            }
        }
        ?>
            <div id="message" class="updated fade">
                <p>Options sauvegardées avec succès</p>
            </div>

        <?php
    }


    ?>

    <div class="wrap theme-options-page">
        <h2 class="title-pannel">Panneau de test</h2>
    </div>

    <form action="" method="post" id="box-info-general">
        <!-- DEBUT :  info_general -> coordonnee -->
        <div id="location" class="theme-options-group">

            <table cellspacing="0" class="widefat options-table">
                <thead>
                    <tr>
                        <th colspan="2" class="titre-pannel">Coordonnées</th>
                    </tr>
                </thead><!-- .thead -->
                <tbody>
                    <tr class="item-location">
                        <th scrop="row">
                            <label for="adresse">Adresse</label>
                        </th>
                        <td>
                            <input type="text" id="adresse" name="options[adresse]" value="<?php echo get_option('adresse', '') ?>" size="75">
                        </td>
                    </tr><!-- .item-location -->

                    <tr class="item-location">
                        <th scrop="row">
                            <label for="phone">Téléphone</label>
                        </th>
                        <td>
                            <input type="text" id="phone" name="options[phone]" value="<?php echo get_option('phone', '') ?>" size="75">
                        </td>
                    </tr><!-- .item-location -->
                </tbody><!-- .tbody -->
            </table><!-- .widefat .options-table -->

        </div> <!-- ./ #location .theme-options-group -->
        <!-- fin : info_general -> coordonnee -->

        <!-- DEBUT :  info_general -> horaire -->
        <div id="horaire" class="theme-options-group">

            <table cellspacing="0" class="widefat options-table">
                <thead>
                    <tr>
                        <th colspan="2" class="titre-table">Réseaux sociaux</th>
                    </tr>
                </thead><!-- .thead -->
                <tbody>
                    <tr class="item-social">
                        <th scrop="row">
                            <label for="facebook">Facebook</label>
                        </th>
                        <td>
                            <input type="text" id="facebook" name="options[facebook]" value="<?php echo get_option('facebook', '') ?>" size="75">
                        </td>
                    </tr><!-- .item-social -->
                    <tr class="item-social">
                        <th scrop="row">
                            <label for="twitter">Twitter</label>
                        </th>
                        <td>
                            <input type="text" id="twitter" name="options[twitter]" value="<?php echo get_option('twitter', '') ?>" size="75">
                        </td>
                    </tr><!-- .item-social -->
                    <tr class="item-social">
                        <th scrop="row">
                            <label for="instagram">Instagram</label>
                        </th>
                        <td>
                            <input type="text" id="instagram" name="options[instagram]" value="<?php echo get_option('instagram', '') ?>" size="75">
                        </td>
                    </tr><!-- .item-social -->



                </tbody><!-- .tbody -->
            </table><!-- .widefat .options-table -->

        </div> <!-- ./ #location .theme-options-group -->
        <!-- fin : info_general -> horaire -->


        <!-- DEBUT : BUTON de sauvegarde -->
        <input type="hidden" name="pannel_noncename" value="<?php echo wp_create_nonce('pannel_test');  ?>">

        <p class="submit">
            <input type="submit" name="pannel_update" class="button-primary autowidth" value="Enregistrer">
        </p>

    <!-- FIN : BUTON de sauvegarde -->
    </form>

    <?php
}
