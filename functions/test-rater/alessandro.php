<?php
/*
* Tutto de Alessandro Castellani
* Create a premium WP Theme
* Part 4 - create custom option with settings API
*
*/


/*

@package sunsettheme

========================
ADMIN PAGE
========================
*/
function sunset_add_admin_page() {

	//Generate Sunset Admin Page
	add_menu_page(
		'Sunset Theme Options',			// page title
		'Sunset',						// menu title
		'manage_options',				// capability
		'alecaddd_sunset',				// slug
		'sunset_theme_create_page',		// function
		'',								// icon
		110								// position
	);

	//Generate Sunset Admin Sub Pages
	add_submenu_page(
		'alecaddd_sunset',				// parent
		'Sunset Theme Options',			// page title
		'General',						// menu title
		'manage_options',				// capability
		'alecaddd_sunset',				// slug
		'sunset_theme_create_page'		// function
	);

	add_submenu_page(
		'alecaddd_sunset',				// parent
		'Sunset CSS Options',			// page title
		'Custom CSS',					// menu title
		'manage_options',				// capability
		'alecaddd_sunset_css',			// slug
		'sunset_theme_settings_page'	// function
	);



}

add_action( 'admin_menu', 'sunset_add_admin_page' );

//Activate custom settings
add_action( 'admin_init', 'sunset_custom_settings' );

// add_action admin_init
function sunset_custom_settings() {
	// register
	register_setting( 'sunset-settings-group', 'first_name' );

	register_setting( 'sunset-settings-group', 'last_name' );
	register_setting( 'sunset-settings-group', 'twitter_handler', 'sunset_sanitize_twitter_handler' );
	register_setting( 'sunset-settings-group', 'facebook_handler' );
	register_setting( 'sunset-settings-group', 'gplus_handler' );

	// settings section
	add_settings_section(
		'sunset-sidebar-options',		// id
		'Sidebar Option',				// title
		'sunset_sidebar_options',		// callback
		'alecaddd_sunset'				// page
	);


	// settings field
	add_settings_field(
		'sidebar-name',					// id
		'First Name',					// title
		'table_test',					// callback
		'alecaddd_sunset',				// page
		'sunset-sidebar-options'		// section
	);

	// add_settings_field(
	// 	'sidebar-twitter', 				// id
	// 	'Twitter handler', 				// title
	// 	'table_test', 		// callback
	// 	'alecaddd_sunset', 				// page
	// 	'sunset-sidebar-options'		// section
	// );

	// add_settings_field(
	// 	'sidebar-facebook', 			// id
	// 	'Facebook handler', 			// title
	// 	'table_test', 		// callback
	// 	'alecaddd_sunset', 				// page
	// 	'sunset-sidebar-options'		// section
	// );

	// add_settings_field(
	// 	'sidebar-gplus', 				// id
	// 	'Google+ handler', 				// title
	// 	'table_test', 		// callback
	// 	'alecaddd_sunset', 				// page
	// 	'sunset-sidebar-options'		// section
	// );

}

// funcion créer dans add_settings_section
function sunset_sidebar_options() {
	echo 'Customize your Sidebar Information';
}

// // function créer dans add_settings_field
// function sunset_sidebar_name() {
// 	$firstName = esc_attr( get_option( 'first_name' ) );
// 	echo '<input type="text" name="first_name" value="'.$firstName.'" placeholder="First Name" />';
// }
//
// function sunset_sidebar_twitter() {
// 	$twitter = esc_attr( get_option( 'twitter_handler' ) );
// 	echo '<input type="text" name="twitter_handler" value="'.$twitter.'" placeholder="Twitter handler" /><p class="description">Input your Twitter username without the @ character.</p>';
// }
// function sunset_sidebar_facebook() {
// 	$facebook = esc_attr( get_option( 'facebook_handler' ) );
// 	echo '<input type="text" name="facebook_handler" value="'.$facebook.'" placeholder="Facebook handler" />';
// }
// function sunset_sidebar_gplus() {
// 	$gplus = esc_attr( get_option( 'gplus_handler' ) );
// 	echo '<input type="text" name="gplus_handler" value="'.$gplus.'" placeholder="Google+ handler" />';
// }

function table_test(){

	// appel
	$firstName = esc_attr( get_option( 'first_name' )) ;
	$twitter = esc_attr( get_option( 'twitter_handler' )) ;
	$facebook = esc_attr( get_option( 'facebook_handler')  );
	$gplus = esc_attr( get_option( 'gplus_handler' ) );

	?>
		<p>
			<label for="first_name"></label>
			<input type="text" id="first_name" name="first_name" value="<?php echo( get_option( 'first_name' ) ); ?>" />
		</p>
		<p>
			<label for="twitter_handler"></label>
			<input type="text" id="twitter_handler" name="twitter_handler" value="<?php echo( get_option( 'twitter_handler' ) ); ?>" />
		</p>
		<p>
			<label for="facebook_handler"></label>
			<input type="text" id="facebook_handler" name="facebook_handler" value="<?php echo( get_option( 'facebook_handler' ) ) ?>" />
		</p>
		<p>
			<label for="gplus_handler"></label>
			<input type="text" id="gplus_handler" name="gplus_handler" value="<?php echo( get_option( 'gplus_handler' ) ) ?>" />
		</p>


	<?php
}


// function créer dans add_menu_page
function sunset_theme_create_page() {
	require_once( get_template_directory() . '/functions/templates/sunset-admin.php' );
}



// function créer dans add_submenu_page
function sunset_theme_settings_page() {

	echo '<h1>Sunset Custom CSS</h1>';

}
