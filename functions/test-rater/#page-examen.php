<?php

/*
Name:   Page examen
Description: Page horaire d'examen
Author: Enza Lombardo
Author URI:
Version: 1.0
*/



/* ---------------------------------------- */
/* -----------    CREER PAGE    ----------- */
/* ---------------------------------------- */

/* ----  1 - initialisation de la page  ---- */
add_action('admin_menu', 'add_page_examen');


/* ----  2 - construire la page  ---- */
function add_page_examen(){

    // Menu 1er niveau
    add_menu_page(
        'Examen',                      // page_title
        'Examen',                      // menu_title
        'manage_options',               // capability
        'examen',                       // slug_menu
        'examen_theme_create_page',    // function
        'dashicons-clock',              // icon
        98                              // position
    );

}


/* ---------------------------------------- */
/* --------    SERVICE DU LUNDI    -------- */
/* ---------------------------------------- */


/* ------------ initialisation des paramattre ------------ */

add_action('admin_init', 'examen_lundi_custom_settings');

/* ------------ contruire des paramettres ------------ */

function examen_lundi_custom_settings(){

    // REGISTER ------------------------------

    /*  -- MIDI -- */
    register_setting('examen-lundi-group', 'lundi_examen_midi');
    register_setting('examen-lundi-group', 'lundi_examen_midi_de');
    register_setting('examen-lundi-group', 'lundi_examen_midi_a');

    /*  -- SOIR -- */
    register_setting('examen-lundi-group', 'lundi_examen_soir');
    register_setting('examen-lundi-group', 'lundi_examen_soir_de');
    register_setting('examen-lundi-group', 'lundi_examen_soir_a');


    /*  -- FERMER-- */
    register_setting('examen-lundi-group', 'lundi_examen_fermer');


    // SETTINGS ------------------------------
    add_settings_section(
        'option-examen-lundi',                           // id
        __('Lundi', 'option-examen-lundi'),                     // title
        'option_examen_lundi',          // callback
        'examen'                // page
    );

    // FIELDS ------------------------------

    /* -- lundi_examen_midi -- */
    add_settings_field(
        'lundi-midi',                            // id
        __('Midi', 'option-examen-lundi'),     // title
        'custom_field_lundi_examen_midi',               // callback
        'examen',                               // page
        'option-examen-lundi'                          // section
    );

    /* -- lundi_examen_soir-- */
    add_settings_field(
        'lundi-soir',                            // id
        __('Soir', 'option-examen-lundi'),     // title
        'custom_field_lundi_examen_soir',               // callback
        'examen',                               // page
        'option-examen-lundi'                          // section
    );

    /* -- lundi_examen_fermer-- */
    add_settings_field(
        'lundi-fermer',                            // id
        __('Fermer', 'option-examen-lundi'),     // title
        'custom_field_lundi_examen_fermer',               // callback
        'examen',                               // page
        'option-examen-lundi'                          // section
    );



}


/* ------------ construire la sections ------------ */
function option_examen_lundi(){

}


/* ------------ construire les différent champs ------------ */
// callback [custom_field_lundi_examen_midi]
function custom_field_lundi_examen_midi(){
    $lundi_examen_midi_de = esc_attr( get_option('lundi_examen_midi_de') );
    $lundi_examen_midi_a = esc_attr( get_option('lundi_examen_midi_a') );

    ?>
    <span>
        <span style="margin-right: 15px;">de</span>
        <input type="time" id="lundi_examen_midi_de" name="lundi_examen_midi_de" value="<?php echo( get_option('lundi_examen_midi_de') ); ?>" />
    </span>
    <span>
        <span style="margin: 0 15px;">à</span>
        <input type="time" id="lundi_examen_midi_a" name="lundi_examen_midi_a" value="<?php echo( get_option('lundi_examen_midi_a') ); ?>" />
    </span>
    <?php
}

// callback [custom_field_lundi_examen_soir]
function custom_field_lundi_examen_soir(){
    $lundi_examen_soir_de = esc_attr( get_option('lundi_examen_soir_de') );
    $lundi_examen_soir_a = esc_attr( get_option('lundi_examen_soir_a') );

    ?>
    <span>
        <span style="margin-right: 15px;">de</span>
        <input type="time" id="lundi_examen_midi_de" name="lundi_examen_soir_de" value="<?php echo( get_option('lundi_examen_soir_de') ); ?>" />
    </span>
    <span>
        <span style="margin: 0 15px;">à</span>
        <input type="time" id="lundi_examen_soir_a" name="lundi_examen_soir_a" value="<?php echo( get_option('lundi_examen_soir_a') ); ?>" />
    </span>
    <?php
}

// callback [custom_field_lundi_examen_fermer]
function custom_field_lundi_examen_fermer(){
    $lundi_examen_fermer = esc_attr( get_option('lundi_examen_fermer') );

    ?>
        <input type="checkbox" id="lundi_examen_fermer" name="lundi_examen_fermer" value="1" <?php checked(1, get_option('lundi_examen_fermer'), true); ?> >
    <?php
}



/* -------------------------------------------- */
/* --------    AFFICHAGE THEME PAGE    -------- */
/* -------------------------------------------- */


function examen_theme_create_page(){
    ?>

    <div class="wrap">
        <h2 class="wp-heading-inline">Horaire d'examen</h2>
        <?php settings_errors(); ?>

        <div class="form-table form-table-custom">
            <form class="form-custom"  method="post" action="options.php">

                <div class="form-table form-table-custom" >
                    <?php settings_fields( 'examen-lundi-group' );?>
                </div>

                <div class="form-table form-table-custom" >
                    <?php //settings_fields( 'examen-mardi-group' );?>
                </div>

                <?php
                do_settings_sections( 'examen' );
                submit_button();
                ?>
            </form>

        </div>
    </div>

    <?php
}
