<?php

// Hook for adding admin menus
add_action('admin_menu', 'add_page_manga');


// action function for above hook
function add_page_manga(){

    // Menu principal (menu de 1er niveau)
    add_menu_page(
        'Mangas',                   // page title
        'Mangas',                   // menu title
        'manage_options',           // capability
        'mangas',                   // slug
        'manga_theme_create_page',  // function
        'dashicons-visibility',     // icon
        111                         // position
    );

}


//Activete custom settings
add_action('admin_init', 'manga_custom_settings');

add_action('admin_init', 'auteur_custom_settings');
add_action('admin_init', 'anime_custom_settings');


// --------------------------------
//    section - Info auteur
//
// --------------------------------

function manga_custom_settings(){

    // register

    /* --- manga --- */
    register_setting('manga-setting-group', 'nom_manga');
    register_setting('manga-setting-group', 'auteur_manga');
    register_setting('manga-setting-group', 'genre_manga');
    register_setting('manga-setting-group', 'statut_manga');

    // settings
    add_settings_section(
        'option-manga',        // id
        '',
        'option_manga',
        'mangas'
    );


    // field manga
    add_settings_field(
        'nom-manga',
        '',
        'fiche_manga',
        'mangas',
        'option-manga'
    );

}

function option_manga(){

}


function fiche_manga(){

    $nom_manga = esc_attr( get_option('nom_manga') );
    $auteur_manga = esc_attr( get_option('auteur_manga') );
    $genre_manga = esc_attr( get_option('genre_manga') );
    $statut_manga = esc_attr( get_option('statut_manga') );

    ?>

    <h2>Mangas</h2>
    <p>Voici une belle page pour ajouter nos mangas préféré</p>
        <table>
            <tbody>
                <tr>
                    <td><label for="nom_manga">Nom du manga</label></td>
                    <td><input type="text" id="nom_manga" name="nom_manga" value="<?php echo( get_option('nom_manga') ); ?>"></td>
                </tr>
                <tr>
                    <td><label for="auteur_manga">Autor</label></td>
                    <td><input type="text" id="auteur_manga" name="auteur_manga" value="<?php echo(get_option('auteur_manga')); ?>"></td>
                </tr>
                <tr>
                    <td><label for="genre_manga">Genre</label></td>
                    <td><input type="text" id="genre_manga" name="genre_manga" value="<?php echo(get_option('genre_manga')); ?>"></td>
                </tr>
                <tr>
                    <td><label for="statut_manga">statut</label></td>
                    <td><input type="text" id="statut_manga" name="statut_manga" value="<?php echo(get_option('statut_manga')); ?>"></td>
                </tr>
            </tbody>
        </table>

    <?php
}

// --------------------------------
//    section - Info auteur
//
// --------------------------------


function auteur_custom_settings(){
    // register
    register_setting('auteur-setting-group', 'sexe_auteur');
    register_setting('auteur-setting-group', 'birthday_auteur');
    register_setting('auteur-setting-group', 'age_auteur');

    // settings
    add_settings_section(
        'info-auteur',
        '',
        'info_auteur',
        'mangas'
    );

    // field manga
    add_settings_field(
        'auteur-info',
        '',
        'fiche_auteur',
        'mangas',
        'info-auteur'
    );
}


function info_auteur(){

}


function fiche_auteur(){

    $sexe_auteur = esc_attr( get_option('sexe_auteur') );
    $birthday_auteur = esc_attr( get_option('birthday_auteur') );
    $age_auteur = esc_attr( get_option('age_auteur') );


    ?>

    <h2>Information relatif à l'auter</h2>
        <table>
            <tbody>
                <tr>
                    <td><label for="sexe_auteur">Sexe</label></td>
                    <td>
                        <input type="radio" <?php checked($sexe_auteur, 'homme'); ?> name="sexe_auteur" value="homme">homme
                        <input type="radio" <?php checked($sexe_auteur, 'femme'); ?> name="sexe_auteur" value="femme">femme
                    </td>
                </tr>
                <tr>
                    <td><label for="birthday_auteur">Date de naissance</label></label></td>
                    <td>
                        <input type="date" id="birthday_auteur" name="birthday_auteur" value="<?php echo( get_option('birthday_auteur') ); ?>">
                    </td>
                    <td><label for="age_auteur">Age</label></td>
                    <td>
                        <input type="text" id="age_auteur" name="age_auteur" value="<?php echo( get_option('age_auteur') ); ?>">
                    </td>
                </tr>
            </tbody>
        </table>
    <?php
}


// -------------------------------
//         section anime
// -------------------------------


function anime_custom_settings(){

    //register
    /* --- anime --- */
    register_setting('anime-setting-group', 'manga_anime');
    register_setting('anime-setting-group', 'episode_anime');
    register_setting('anime-setting-group', 'annee_anime');


    // settings
    add_settings_section(
        'info-anime',
        '',
        'info_anime',
        'mangas'
    );

    // anime
    add_settings_field(
        'anime-manga',
        '',
        'fiche_anime',
        'mangas',
        'option-manga'
    );

}

function info_anime(){

}

function fiche_anime(){

    $manga_anime = esc_attr( get_option('manga_anime') );
    $episode_anime = esc_attr( get_option('episode_anime') );
    $annee_anime = esc_attr( get_option('annee_anime') );

    ?>
        <h2>Dessin Animé</h2>
        <p>Lorem ipsum dolor sit amet.</p>
        <table>
            <tbody>
                <tr>
                    <td><label for="manga_anime">Le manga est-il anime</label></td>
                    <td>
                        <input type="radio" <?php checked($manga_anime, 'oui'); ?> name="manga_anime" value="oui">oui
                        <input type="radio" <?php checked($manga_anime, 'non'); ?> name="manga_anime" value="non">non
                    </td>
                </tr>
                <tr>
                    <td><label for="episode_anime">Nombre d'épisode</label></td>
                    <td><input type="text" id="episode_anime" name="episode_anime" value="<?php echo(get_option('episode_anime')); ?>" /></td>
                </tr>
                <tr>
                    <td><label for="annee_anime">Année</label></td>
                    <td><input type="text" id="annee_anime" name="annee_anime" value="<?php echo(get_option('annee_anime')); ?>" /></td>
                </tr>
            </tbody>
        </table>
    <?php
}

// ------------------------------------------
//  AFFICHAGE
//
//  affichage du formulaire dans une page dédier
// ------------------------------------------

function manga_theme_create_page(){
    require_once(get_template_directory().'/functions/templates/theme-manga.php');
}
