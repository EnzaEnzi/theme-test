<?php
/*
Name:   Page Manga
Description:
Author: Enza Lombardo
Author URI:
Version: 1.0
*/


/* ---------------------------------------- */
/* -----------    CREER PAGE    ----------- */
/* ---------------------------------------- */

/* ----  1 - initialisation de la page  ---- */
add_action('admin_menu', 'add_page_manga');


/* ----  2 - construire la page  ---- */
function add_page_manga(){

    // Menu 1er niveau
    add_menu_page(
        'Mangas',                   // page_title
        'Mangas',                   // menu_title
        'manage_options',           // capability
        'mangas',                   // slug_menu
        'manga_theme_create_page',  // function
        'dashicons-visibility',     // icon
        111                         // position
    );

}



/* ---------------------------------------- */
/* -------    DEFINIR LES CHAMPS    ------- */
/* ---------------------------------------- */

/* ----  3 - initialisation des paramattre  ---- */
add_action('admin_init', 'manga_custom_settings');
add_action('admin_init', 'auteur_custom_settings');




/* ---------------------------------------- */
/* ----  4 - contruire des paramettre  ---- */
/* -------------    MANGAS    ------------- */
/* ---------------------------------------- */

function manga_custom_settings(){

    // REGISTER ------------------------------

    // manga
    register_setting('manga-setting-group', 'nom_manga');
    register_setting('manga-setting-group', 'auteur_manga');
    register_setting('manga-setting-group', 'genre_manga');
    register_setting('manga-setting-group', 'statut_manga');

    // SETTINGS ------------------------------
    add_settings_section(
        'option-manga',        // id
        __('Fiche manga', 'option-manga'),
        'option_manga',
        'mangas'
    );


    // FIELD ------------------------------

    // Nom du manga
    add_settings_field(
        'nom-manga',                            // id
        __('Nom du manga', 'option-manga'),     // title
        'custom_field_nom_manga',               // callback
        'mangas',                               // page
        'option-manga'                          // section
    );

    // Nom de l'auteur
    add_settings_field(
        'auteur-manga',                            // id
        __('Auteur', 'option-manga'),     // title
        'custom_field_auteur_manga',               // callback
        'mangas',                               // page
        'option-manga'                          // section
    );

    // Le Genre du manga
    add_settings_field(
        'genre-manga',                            // id
        __('Genre', 'option-manga'),     // title
        'custom_field_genre_manga',               // callback
        'mangas',                               // page
        'option-manga'                          // section
    );

    // Le statut du manga
    add_settings_field(
        'statut-manga',                            // id
        __('Statut', 'option-manga'),     // title
        'custom_field_statut_manga',               // callback
        'mangas',                               // page
        'option-manga'                          // section
    );


}

// callback [settings_sections]
function option_manga(){

}


// callback [custom_field_nom_manga]
function custom_field_nom_manga(){
    $nom_manga = esc_attr( get_option('nom_manga') );
    ?>
    <input type="text" id="nom_manga" name="nom_manga" value="<?php echo( get_option('nom_manga') ); ?>">
    <?php
}

// callback [custom_field_auteur_manga]
function custom_field_auteur_manga(){
    $nom_manga = esc_attr( get_option('auteur_manga') );
    ?>
    <input type="text" id="auteur_manga" name="auteur_manga" value="<?php echo( get_option('auteur_manga') ); ?>">
    <?php
}

// callback [custom_field_genre_manga]
function custom_field_genre_manga(){
    $nom_manga = esc_attr( get_option('genre_manga') );
    ?>
    <input type="text" id="genre_manga" name="genre_manga" value="<?php echo( get_option('genre_manga') ); ?>">
    <?php
}

// callback [custom_field_statut_manga]
function custom_field_statut_manga(){
    $nom_manga = esc_attr( get_option('statut_manga') );
    ?>
    <input type="text" id="statut_manga" name="statut_manga" value="<?php echo( get_option('statut_manga') ); ?>">
    <?php
}


/* ---------------------------------------- */
/* ----  4 - contruire des paramettre  ---- */
/* -------------    AUTEUR    ------------- */
/* ---------------------------------------- */

function auteur_custom_settings(){

    // REGISTER ------------------------------
    register_setting('auteur-setting-group', 'sexe_auteur');
    register_setting('auteur-setting-group', 'birthday_auteur');
    register_setting('auteur-setting-group', 'age_auteur');

    // SETTINGS ------------------------------
    add_settings_section(
        'option-auteur',          // id
        __('Info sur l\'auteur ', 'option-auteur'),                     // title
        'option_auteur',          // callback
        'mangas'                // page
    );

    // FIELD ------------------------------

    /* -- sexe -- */
    add_settings_field(
        'sexe-auteur',                            // id
        __('Sexe', 'option-auteur'),     // title
        'custom_field_sexe_auteur',               // callback
        'mangas',                               // page
        'option-auteur'                          // section
    );

    /* -- aniversaire-- */
    add_settings_field(
        'birthday-auteur',                            // id
        __('Sexe', 'option-auteur'),     // title
        'custom_field_birthday_auteur',               // callback
        'mangas',                               // page
        'option-auteur'                          // section
    );


    /* -- age -- */
    add_settings_field(
        'age-auteur',                            // id
        __('Agée de', 'option-auteur'),     // title
        'custom_field_age_auteur',               // callback
        'mangas',                               // page
        'option-auteur'                          // section
    );
}

// callback [settings_sections]
function option_auteur(){

}

// callback [custom_field_sexe_auteur]
function custom_field_sexe_auteur(){
    $sexe_auteur = esc_attr( get_option('sexe_auteur') );
    ?>
        <input type="radio" <?php checked($sexe_auteur, 'homme'); ?> name="sexe_auteur" value="homme">Homme
        <input type="radio" <?php checked($sexe_auteur, 'femme'); ?> name="sexe_auteur" value="femme">Femme
    <?php
}

// callback [custom_field_birthday_auteur]
function custom_field_birthday_auteur(){
    $birthday_auteur = esc_attr( get_option('birthday_auteur') );
    ?>
        <input type="date" id="birthday_auteur" name="birthday_auteur" value="<?php echo( get_option('birthday_auteur') ); ?>">
    <?php
}

// callback [custom_field_age_auteur]
function custom_field_age_auteur(){
    $age_auteur = esc_attr( get_option('age_auteur') );
    ?>
        <input type="text" id="age_auteur" name="age_auteur" value="<?php echo( get_option('age_auteur') ); ?>">
    <?php
}




/* -------------------------------------------- */
/* --------    AFFICHAGE THEME PAGE    -------- */
/* -------------------------------------------- */

function manga_theme_create_page(){
    //require_once(get_template_directory().'/functions/templates/theme-manga.php');

    ?>
    <div class="wrap">
        <h2 class="wp-heading-inline">Page Manga</h2>
        <?php settings_errors(); ?>

        <div class="form-table form-table-custom">
            <form class="form-custom"  method="post" action="options.php">

                <div class="form-table form-table-custom" >
                    <?php settings_fields( 'manga-setting-group' );?>
                </div>

                <div class="form-table form-table-custom" >
                    <?php settings_fields( 'auteur-setting-group' );?>
                </div>

                <?php
                do_settings_sections( 'mangas' );
                submit_button();
                ?>
            </form>

        </div>
    </div>
    <?php
}
