<?php
/*
Name:   Page Horaire
Description: Page horaire
Author: Enza Lombardo
Author URI:
Version: 1.0
*/

/* ---------------------------------------- */
/* -----------    CREER PAGE    ----------- */
/* ---------------------------------------- */

/* ----  1 - initialisation de la page  ---- */
add_action('admin_menu', 'add_page_service');


/* ----  2 - construire la page  ---- */
function add_page_service(){

    // Menu 1er niveau
    add_menu_page(
        'Service',                      // page_title
        'Service',                      // menu_title
        'manage_options',               // capability
        'service',                       // slug_menu
        'service_theme_create_page',    // function
        'dashicons-clock',              // icon
        98                              // position
    );

}

/* ---------------------------------------- */
/* --------    SERVICE DU LUNDI    -------- */
/* ---------------------------------------- */


/* ------------ initialisation des paramattre ------------ */
add_action('admin_init', 'service_lundi_custom_settings');

/* ------------ contruire des paramettres ------------ */

function service_lundi_custom_settings(){

    // REGISTER ------------------------------

    /*  -- MIDI-- */
    register_setting('service-lundi-group', 'lundi_midi');
    register_setting( 'service-lundi-group', 'lundi_midi_de' );
    register_setting( 'service-lundi-group', 'lundi_midi_a' );

    /* -- SOIR -- */
    register_setting('service-lundi-group', 'lundi_soir');
    register_setting( 'service-lundi-group', 'lundi_soir_de' );
    register_setting( 'service-lundi-group', 'lundi_soir_a' );

    // SETTINGS ------------------------------
    add_settings_section(
        'option-service-lundi',                           // id
        __('Lundi', 'option-service-lundi'),                     // title
        'option_service_lundi',          // callback
        'service'                // page
    );


    // FIELDS ------------------------------

    /* -- lundi_midi -- */
    add_settings_field(
        'lundi-midi',                            // id
        __('Midi', 'option-service-lundi'),     // title
        'custom_field_lundi_midi',               // callback
        'service',                               // page
        'option-service-lundi'                          // section
    );

    /* -- lundi_midi -- */
    add_settings_field(
        'lundi-soir',                            // id
        __('Soir', 'option-service-lundi'),     // title
        'custom_field_lundi_soir',               // callback
        'service',                               // page
        'option-service-lundi'                          // section
    );
}

/* ------------ construire la sections ------------ */
function option_service_lundi(){

}

/* ------------ construire les différent champs ------------ */
// callback [custom_field_lundi_midi]
function custom_field_lundi_midi(){
    $lundi_midi_de = esc_attr( get_option('lundi_midi_de') );
    $lundi_midi_a = esc_attr( get_option('lundi_midi_a') );

    ?>
        <span>
            <span style="margin-right: 15px;">de</span>
            <input type="time" id="lundi_midi_de" name="lundi_midi_de" value="<?php echo( get_option('lundi_midi_de') ); ?>" />
        </span>
        <span>
            <span style="margin: 0 15px;">à</span>
            <input type="time" id="lundi_midi_a" name="lundi_midi_a" value="<?php echo( get_option('lundi_midi_a') ); ?>" />
        </span>
    <?php
}

// callback [custom_field_lundi_soir]
function custom_field_lundi_soir(){
    $lundi_soir_de = esc_attr( get_option('lundi_soir_de') );
    $lundi_soir_a = esc_attr( get_option('lundi_soir_a') );
    ?>
    <span>
        <span style="margin-right: 15px;">de</span>
        <input type="time" id="lundi_soir_de" name="lundi_soir_de" value="<?php echo( get_option('lundi_soir_de') ); ?>" />
    </span>
    <span>
        <span style="margin: 0 15px;">à</span>
        <input type="time" id="lundi_soir_a" name="lundi_soir_a" value="<?php echo( get_option('lundi_soir_a') ); ?>" />
    </span>
    <?php
}


/* ---------------------------------------- */
/* --------    SERVICE DU MARDI    -------- */
/* ---------------------------------------- */


/* ------------ initialisation des paramattre ------------ */
add_action('admin_init', 'service_mardi_custom_settings');

/* ------------ contruire des paramettres ------------ */

function service_mardi_custom_settings(){

    // REGISTER ------------------------------

    /*  -- MIDI-- */
    register_setting('service-mardi-group', 'mardi_midi');
    register_setting( 'service-mardi-group', 'mardi_midi_de' );
    register_setting( 'service-mardi-group', 'mardi_midi_a' );

    /* -- SOIR -- */
    register_setting('service-mardi-group', 'mardi_soir');
    register_setting( 'service-mardi-group', 'mardi_soir_de' );
    register_setting( 'service-mardi-group', 'mardi_soir_a' );

    // SETTINGS ------------------------------
    add_settings_section(
        'option-service-mardi',                           // id
        __('mardi', 'option-service-mardi'),                     // title
        'option_service_mardi',          // callback
        'service'                // page
    );


    // FIELDS ------------------------------

    /* -- mardi_midi -- */
    add_settings_field(
        'mardi-midi',                            // id
        __('Midi', 'option-service-mardi'),     // title
        'custom_field_mardi_midi',               // callback
        'service',                               // page
        'option-service-mardi'                          // section
    );

    /* -- mardi_midi -- */
    add_settings_field(
        'mardi-soir',                            // id
        __('Soir', 'option-service-mardi'),     // title
        'custom_field_mardi_soir',               // callback
        'service',                               // page
        'option-service-mardi'                          // section
    );
}

/* ------------ construire la sections ------------ */
function option_service_mardi(){

}

/* ------------ construire les différent champs ------------ */
// callback [custom_field_mardi_midi]
function custom_field_mardi_midi(){
    $mardi_midi_de = esc_attr( get_option('mardi_midi_de') );
    $mardi_midi_a = esc_attr( get_option('mardi_midi_a') );
    $mardi_soir_de = esc_attr( get_option('mardi_soir_de') );
    $mardi_soir_a = esc_attr( get_option('mardi_soir_a') );
    ?>
        <span>
            <span style="margin-right: 15px;">de</span>
            <input type="time" id="mardi_midi_de" name="mardi_midi_de" value="<?php echo( get_option('mardi_midi_de') ); ?>" />
        </span>
        <span>
            <span style="margin: 0 15px;">à</span>
            <input type="time" id="mardi_midi_a" name="mardi_midi_a" value="<?php echo( get_option('mardi_midi_a') ); ?>" />
        </span>
    <?php
}

// callback [custom_field_mardi_soir]
function custom_field_mardi_soir(){
    ?>
    <span>
        <span style="margin-right: 15px;">de</span>
        <input type="time" id="mardi_soir_de" name="mardi_soir_de" value="<?php echo( get_option('mardi_soir_de') ); ?>" />
    </span>
    <span>
        <span style="margin: 0 15px;">à</span>
        <input type="time" id="mardi_soir_a" name="mardi_soir_a" value="<?php echo( get_option('mardi_soir_a') ); ?>" />
    </span>
    <?php
}





/* -------------------------------------------- */
/* --------    AFFICHAGE THEME PAGE    -------- */
/* -------------------------------------------- */


function service_theme_create_page(){
    ?>

        <div class="wrap">
            <h2 class="wp-heading-inline">Horaire</h2>
            <?php settings_errors(); ?>

            <div class="form-table form-table-custom">
                <form class="form-custom" action="option.php" method="post">

                    <div class="form-table form-table-custom">
                        <?php settings_fields('service-lundi-group') ?>
                    </div>

                    <div class="form-table form-table-custom">
                        <?php settings_fields('service-mardi-group') ?>
                    </div>

                    <?php
                    do_settings_sections( 'service' );
                    submit_button();
                     ?>
                </form>
            </div>

        </div>

    <?php
}
