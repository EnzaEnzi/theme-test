<?php

/*
Name:   Page Info Général
Description: Page dédier pour les musiques pratiques du site
Author: Enza Lombardo
Author URI:
Version: 1.0


1 - initialisation de la page
2 - construire la page
3 - initialisation des paramattre
4 - contruire des paramettre

affichage du formulaire (theme page)

*/

/* ---------------------------------------- */
/* -----------    CREER PAGE    ----------- */
/* ---------------------------------------- */

/* ----  1 - initialisation de la page  ---- */
add_action('admin_menu', 'add_page_musique');

/* ----  2 - construire la page  ---- */
function add_page_musique(){

    // Menu 1er niveau
    add_menu_page(
        'Musique',                         // page_title
        'Musique',                         // menu_title
        'manage_options',                      // capability
        'musique',                        // menu_slug
        'music_theme_page',                     // function
        'dashicons-playlist-audio',                      // icon_url
        100                                    // position
    );

    // Menu 2e niveau
    // add_submenu_page(
    //     'info-general',                        // parent
    //     'Horaires',                            // page title
    //     'Horaires',                            // menu title
    //     'manage_options',                      // capability
    //     'horaires',                            // slug
    //     'horaire_theme_page'                   // function
    // );
}

/* ----  3 - initialisation des paramattre  ---- */
add_action('admin_init', 'musique_custom_settings');
//add_action('admin_init', 'social_custom_settings');
// add_action('admin_init', 'horaire_custom_settings');



/* ---------------------------------------- */
/* ----  4 - contruire des paramettre  ---- */
/* --------        -------- */
/* ---------------------------------------- */

function musique_custom_settings(){
    // REGISTER ----------------------
    register_setting(
        'musique_option_group',                         // option_group
        'musique_option_name',                          // option_name
        array( $musique_options, 'musique_sanitize' )   // sanitize_callback
    );

    // SETTING ----------------------
    add_settings_section(
        'musique_setting_section',                        // id
        '',                                       // title
        'musique_section_info',  // callback
        'musique'                                         // page
    );

    // FIELD ----------------------

    // titre chanson
    add_settings_field(
        'titre_chanson',                                // id
        'Titre de la chanson',                          // titre
        'titre_chanson',       // callback
        'musique',                                      // page
        'musique_setting_section'                       // section
    );


}


// callback [setting] ----------
function musique_section_info() {

}





function titre_chanson(){
    isset( $$sanitary_values->musique_options['titre_chanson'] ) ? esc_attr( $$sanitary_values->musique_options['titre_chanson']) : ''
    ?>
		<input class="regular-text" type="text" name="musique_option_name[titre_chanson]" id="titre_chanson" value="<?php echo $input['titre_chanson'];?>">
	<?php

}

function musique_sanitize($input){
    $sanitary_values = array();
	if ( isset( $input['titre_chanson'] ) ) {
		$sanitary_values['titre_chanson'] = sanitize_text_field( $input['titre_chanson'] );
	}

    return $sanitary_values;
}


/* ---------------------------------------- */
/* ----  4 - contruire des paramettre  ---- */
/* ---------    INFO -> SOCIAL    --------- */
/* ---------------------------------------- */


/* ---------------------------------------- */
/* ----  4 - contruire des paramettre  ---- */
/* -------    SUBMENU -> HORAIRE    ------- */
/* ---------------------------------------- */



/* -------------------------------------------- */
/* --------    AFFICHAGE THEME PAGE    -------- */
/* -------------------------------------------- */

// theme page => info general
function music_theme_page(){
    ?>

        <div class="wrap">
            <h2>Mes musique</h2>
            <p></p>
            <?php settings_errors(); ?>

            <form method="post" action="options.php">
                <?php
                settings_fields( 'musique_option_group' );
                do_settings_sections( 'musique' );
                submit_button();
                ?>
            </form>
        </div>
    <?php
}
