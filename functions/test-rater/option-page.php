<?php
// create custom plugin settings menu
add_action('admin_menu', 'enza_create_menu');

function enza_create_menu() {

	//create new top-level menu
	add_menu_page(
		'My Cool Plugin Settings', 		// page title
		'Info général',					// menu title
		'administrator',				// capability
		__FILE__,						// menu slug
		'enza_settings_page' ,			// function
		'dashicons-visibility',			// icon
		112								// position
	);

	add_submenu_page(
		__FILE__,						// menu slug [parent]
		'Horaire',						// page title
		'Horaire',						// submenu title
		'administrator',				// capability
		'horaire-sub-page',				// submenu slug
		'horaire_settings_page'			// function
	);

	//call register settings function
	add_action( 'admin_init', 'register_info_settings' );
	add_action( 'admin_init', 'register_horaire_settings' );
}


function register_info_settings() {
	//register our settings
	register_setting( 'enza-settings-group', 'nom' );
	register_setting( 'enza-settings-group', 'prenom' );
	register_setting( 'enza-settings-group', 'age' );
}

function enza_settings_page() {
	require_once(get_template_directory().'/functions/templates/enza-settings.php');
}

function register_horaire_settings(){
	//register our settings
	register_setting( 'horaire-settings-group', 'lundi_midi_de' );
	register_setting( 'horaire-settings-group', 'lundi_midi_a' );
	register_setting( 'horaire-settings-group', 'lundi_soir_de' );
	register_setting( 'horaire-settings-group', 'lundi_soir_a' );
}


function horaire_settings_page() {
	require_once(get_template_directory().'/functions/templates/horaire-settings.php');
}
