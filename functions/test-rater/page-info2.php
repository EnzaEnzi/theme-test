<?php

/*
Name:   Page Info Général
Description: Page dédier pour les informations pratiques du site
Author: Enza Lombardo
Author URI:
Version: 1.0


1 - initialisation de la page
2 - construire la page
3 - initialisation des paramattre
4 - contruire des paramettre

affichage du formulaire (theme page)

*/

/* ---------------------------------------- */
/* -----------    CREER PAGE    ----------- */
/* ---------------------------------------- */

/* ----  1 - initialisation de la page  ---- */
add_action('admin_menu', 'add_page_information');

/* ----  2 - construire la page  ---- */
function add_page_information(){

    // Menu 1er niveau
    add_menu_page(
        'Information',                         // page title
        'Infos resto',                         // menu title
        'manage_options',                      // capability
        'info-general',                        // slug
        'info_theme_page',                     // function
        'dashicons-info',                      // icon
        100                                    // position
    );

    // Menu 2e niveau
    add_submenu_page(
        'info-general',                        // parent
        'Horaires',                            // page title
        'Horaires',                            // menu title
        'manage_options',                      // capability
        'horaires',                            // slug
        'horaire_theme_page'                   // function
    );
}

/* ----  3 - initialisation des paramattre  ---- */
add_action('admin_init', 'location_custom_settings');
add_action('admin_init', 'social_custom_settings');
// add_action('admin_init', 'horaire_custom_settings');



/* ---------------------------------------- */
/* ----  4 - contruire des paramettre  ---- */
/* --------    INFO -> LOCATION    -------- */
/* ---------------------------------------- */

function location_custom_settings(){

    // REGISTER ------------------------------
    register_setting( 'location-group', 'information_settings' );

    // SETTINGS ------------------------------
    add_settings_section(
        'option-location',          // id
        __('Coordonnée', 'info-general'),                         // title
        'option_location',          // callback
        'info-general'              // page (parent)
    );

    // adresse ------------------------------
    add_settings_field(
        'Adressee',            // id
        __('Google Map', 'info-general'),                                 // title
        'custom_field_info_address',            // callback
        'info-general',                     // page (parent)
        'option-location'                   // section
    );

    // MAP ------------------------------
    add_settings_field(
        'map',            // id
        __('Google Map', 'info-general'),                                 // title
        'custom_field_map',            // callback
        'info-general',                     // page (parent)
        'option-location'                   // section
    );



}

// callback [option-location] ----------
function option_location(){

}

// callback [option-info-address] ----------
function custom_field_info_address(){
    $options = get_option('information_settings');
    ?>
        <input type="text" id="information_settings[info-address]" name="information_settings[info-address]" value="<?php echo $options['info-address']; ?>">
    <?php

}


// callback [field -> map] ----------
function custom_field_map(){
    $options = get_option('information_settings');
    ?>
        <textarea id="information_settings[map]" name="information_settings[map]"><?php echo $options['map']; ?></textarea>
    <?php

}



/* ---------------------------------------- */
/* ----  4 - contruire des paramettre  ---- */
/* ---------    INFO -> SOCIAL    --------- */
/* ---------------------------------------- */

function social_custom_settings(){

    // REGISTER ------------------------------
    register_setting( 'social-group', 'social_settings' );

    // SETTINGS ------------------------------
    add_settings_section(
        'option-social',          // id
        __('Réseau Sociaux', 'info-general'),                         // title
        'option_social',          // callback
        'info-general'              // page (parent)
    );

    // FACEBOOK ------------------------------
    add_settings_field(
        'facebook2',            // id
        __('Facebook', 'info-general'),                                 // title
        'custom_field_facebook2',            // callback
        'info-general',                     // page (parent)
        'option-social'                   // section
    );

    // TWITTER ------------------------------
    add_settings_field(
        'Twitter',            // id
        __('twitter', 'info-general'),                                 // title
        'custom_field_twitter',            // callback
        'info-general',                     // page (parent)
        'option-social'                   // section
    );

    // INTAGRAM ------------------------------
    add_settings_field(
        'instagram',            // id
        __('Intagram', 'info-general'),                                 // title
        'custom_field_instagram',            // callback
        'info-general',                     // page (parent)
        'option-social'                   // section
    );


}

// callback [option-social] ----------
function option_social(){

}

// callback [option-facebook2] ----------
function custom_field_facebook2(){
    $options = get_option('social_settings');
    ?>
        <input type="text" id="social_settings[facebook2]" name="social_settings[facebook2]" value="<?php echo $options['facebook2']; ?>">
    <?php

}

// callback [option-twitter] ----------
function custom_field_twitter(){
    $options = get_option('social_settings');
    ?>
        <input type="text" id="social_settings[twitter]" name="social_settings[twitter]" value="<?php echo $options['twitter']; ?>">
    <?php

}

// callback [option-instagram] ----------
function custom_field_instagram(){
    $options = get_option('social_settings');
    ?>
        <input type="text" id="social_settings[instagram]" name="social_settings[instagram]" value="<?php echo $options['instagram']; ?>">
    <?php

}
/* ---------------------------------------- */
/* ----  4 - contruire des paramettre  ---- */
/* -------    SUBMENU -> HORAIRE    ------- */
/* ---------------------------------------- */



/* -------------------------------------------- */
/* --------    AFFICHAGE THEME PAGE    -------- */
/* -------------------------------------------- */

// theme page => info general
function info_theme_page(){
    require_once( get_template_directory() . '/functions/templates/theme-page-information.php' );
}



// theme page => horaire
function horaire_theme_page(){
    ?>
        <h2>horaire</h2>

        <div class="">
            lundi

        </div>
    <?php
}
