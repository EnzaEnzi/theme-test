<?php

// create custom plugin settings menu
add_action('admin_menu', 'create_page_option');

function create_page_option() {

	//create new top-level menu
	add_menu_page(
			'Stuff', 		// page title
			'Stuff',					// menu title
			'activate_plugin',				// capability
			'stuff',						// menu slug
			'stuff_settings_page' ,			// function
			'',			// icon
			113								// position
		);

	add_submenu_page(
		'stuff',						// menu slug [parent]
		'Spazi',						// page title
		'Spazi',						// submenu title
		'activate_plugin',				// capability
		'spazi-sub-page',				// submenu slug
		'spazi_settings_page'			// function
	);

	//call register settings function
	// add_action( 'admin_init', 'register_info_settings' );
	// add_action( 'admin_init', 'register_spazi_settings' );

	//add_action('admin_init', 'spazi_settings_page');
	add_action('add_meta_boxes', 'add_metabox_horaires');
}

function stuff_settings_page() {
	?>
	<h1>Page Stuff</h1>
	<?php
}

function add_metabox_horaires(){
    add_meta_box('metabox_horaires', 'Heure d\'ouverture', 'spazi_settings_page', '');
}

function spazi_settings_page($POST){
    wp_nonce_field(basename(__FILE__), 'metabox_horaires_nonce');
	if (!current_user_can('activate_plugin'))  {
		wp_die( __("Vous n'avez pas les autorisations suffisantes pour accéder à cette page") );
	}


    ?>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">

	<div class="container">
		<h1>Spazi</h1>

	    <table class="table table-bordered">
	        <thead>
	            <tr>
	                <th scope="col">Jour</th>
	                <th scope="col">Service du Midi</th>
	                <th scope="col">Service du Soir</th>
	            </tr>
	        </thead>
	        <tbody>
	            <tr class="item-day">
	                <td class="day">Lundi</td>
	                <td>
	                    <span>
	                        <label for="lundi_midi_de"> de </label>
	                        <input id="lundi_midi_de" type="time" name="lundi_midi_de" value="<?php echo wp_create_nonce('lundi_midi_de'); ?>"/>
	                    </span>
	                    <span>
	                        <label for="lundi_midi_a"> à </label>
	                        <input id="lundi_midi_a" type="time" name="lundi_midi_a" value="<?php echo wp_create_nonce('lundi_midi_a'); ?>"/>
	                    </span>
	                </td>
	                <td>
	                    <span>
	                        <label for="lundi_soir_de"> de </label>
	                        <input id="lundi_soir_de" type="time" name="lundi_soir_de" value="<?php echo wp_create_nonce('lundi_soir_de'); ?>"/>
	                    </span>
	                    <span>
	                        <label for="lundi_soir_a"> à </label>
	                        <input id="lundi_soir_a" type="time" name="lundi_soir_a" value="<?php echo wp_create_nonce('lundi_soir_a'); ?>"/>
	                    </span>
	                </td>
	            </tr><!-- ./ itemday -->

	        </tbody>
	    </table>

		<?php submit_button(); ?>
	</div>




    <?php
}

// 3 - Sauvegarde des données de la métabox

add_action('save_post', 'save_metabox_horaires');

function save_metabox_horaires($POST_ID){
    // lundi -------------------------
    if(isset($_POST['lundi_midi_de'])){
        update_post_meta($POST_ID, 'lundi_midi_de', esc_html($_POST['lundi_midi_de']));
    }
    if(isset($_POST['lundi_midi_a'])){
        update_post_meta($POST_ID, 'lundi_midi_a', esc_html($_POST['lundi_midi_a']));
    }
    if(isset($_POST['lundi_soir_de'])){
        update_post_meta($POST_ID, 'lundi_soir_de', esc_html($_POST['lundi_soir_de']));
    }
    if(isset($_POST['lundi_soir_a'])){
        update_post_meta($POST_ID, 'lundi_soir_a', esc_html($_POST['lundi_soir_a']));
    }
}
