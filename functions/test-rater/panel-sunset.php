<?php
/*

@package sunsettheme

========================
ADMIN PAGE
========================
*/
function sunset_add_admin_page() {

    //Generate Sunset Admin Page
    add_menu_page( 'Sunset Theme Options', 'Sunset', 'manage_options', 'alecaddd_sunset', 'sunset_theme_create_page', 'dashicons-universal-access', 110 );

    //Generate Sunset Admin Sub Pages
    add_submenu_page( 'alecaddd_sunset', 'Sunset Theme Options', 'General', 'manage_options', 'alecaddd_sunset', 'sunset_theme_create_page' );
    add_submenu_page( 'alecaddd_sunset', 'Page 2', 'Horaire', 'manage_options', 'alecaddd_sunset_horaire', 'sunset_theme_settings_page' );

}
add_action( 'admin_menu', 'sunset_add_admin_page' );

//Activete custom settings
add_action('admin_init', 'sunset_custom_settings');

function sunset_custom_settings(){
    register_setting('sunset-settings-group', 'first_name');
    register_setting('sunset-settings-group', 'second_name');
    add_settings_section( 'sunset-sidebar-options', 'Coordonnee', 'sunset_sidebar_options', 'alecaddd_sunset');
}


function sunset_sidebar_options(){
    $firstName = esc_attr( get_option( 'first_name' ));
    $secondName = esc_attr( get_option( 'second_name' ));
	?>
        <div>
            <label for="first_name">First Name</label>
            <input id="first_name" type="text" name="first_name" value="<?php echo $firstName; ?>"  />
        </div>

        <div>
            <label for="second_name">Secon Name</label>
            <input id="second_name" type="text" name="second_name" value="<?php echo $secondName; ?>"  />
        </div>
    <?php
}





// Afficher le formulaire sur une nouvelle page
function sunset_theme_create_page() {
    require_once( get_template_directory() . '/functions/templates/sunset-admin.php' );
}


function sunset_theme_settings_page() {

	?>
        <h1>Heure d'ouverture</h1>
    <?php

}
