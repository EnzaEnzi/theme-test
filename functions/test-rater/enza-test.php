<?php

/*
Plugin Name: Menu Test
Plugin URI: http://codex.wordpress.org/Adding_Administration_Menus
Description: Menu Test
Author: Codex authors
Author URI: http://example.com
*/

// Hook for adding admin menus
add_action('admin_menu', 'enza_test_add_pages');

// action function for above hook
function enza_test_add_pages() {
	// Add a new submenu under Réglages:
	add_options_page(
		__('Enza Réglages','menu-enza'),		// titre de la page
		__('Enza Réglages','menu-enza'),		// titre du menu
		'manage_options',						// niveau d’autorisation
		'enzasettings',							// slug
		'settings_page_test'							// function d'affiche de la page
	);

	// Add a new submenu under outils:
	add_management_page(
		__('Enza outils','menu-enza'),		// titre de la page
		__('Enza outils','menu-enza'),		// titre du menu
		'manage_options',					// niveau d’autorisation
		'enzatools',						// slug
		'page_outil_test'					// function d'affiche de la page
	);

	// Add a new top-level menu (ill-advised): TITRE DU MENU
	add_menu_page(
		__('Enza','menu-enza'),		// titre de la page
		__('Enza','menu-enza'),		// titre du menu
		'manage_options',			// niveau d’autorisation
		'test-enza',		 		// slug
		'title_accueil',				// function d'affiche de la page
		'dashicons-format-chat',      // icône
		111                          // position
	);

	// Add a submenu to the custom top-level menu:
	add_submenu_page(
		'test-enza',
		__('Level 2','menu-enza'),		// titre de la page
		__('Level 2','menu-enza'),		// titre du menu
		'manage_options',				// niveau d’autorisation
		'sub-page',						// slug
		'title_sublevel'				// function d'affiche de la page
	);

	// Add a second submenu to the custom top-level menu:
	add_submenu_page(
		'test-enza',
		__('Level 3','menu-enza'),		// titre de la page
		__('Level 3','menu-enza'),		// titre du menu
		'manage_options',				// niveau d’autorisation
		'sub-page2',					// slug
		'title_sublevel_2'				// function d'affiche de la page
	);
}

function settings_page_test(){
	echo "<h2>" . __( 'Test Enza (Page Réglages)', 'menu-enza' ) . "</h2>";
	?>
	<div>
	    <h4>TEST</h4>
	    <p>
	        Ce test permet de créer un sous-menu dans le menu
	        par defaut "Réglages" (options-general.php)
	    </p>
	    <p>
	        Grace à la fonction => <strong>add_options_page</strong>
	    </p>
	</div>
	<?php
}



require_once( get_template_directory() . '/functions/templates/enzas-page-ettings.php' );

// page_outil_test() displays the page content for the Enza outils submenu
function page_outil_test() {
	echo "<h2>" . __( 'Test Enza (page outil)', 'menu-enza' ) . "</h2>";
	?>
	<div>
		<h4>TEST</h4>
		<p>
			Ce test permet de créer un sous-menu dans le menu
			par defaut "Outils" (tools.php)
		</p>
		<p>
			Grace à la fonction => <strong>add_management_page</strong>
		</p>
	</div>
	<?php
}

// title_accueil() displays the page content for the custom Enza Toplevel menu
// function title_accueil() {
// 	echo "<h2>" . __( 'Test Enza', 'menu-enza' ) . "</h2>";
//
//
// }

// title_sublevel() displays the page content for the first submenu
// of the custom Enza Toplevel menu
function title_sublevel() {
	echo "<h2>" . __( 'Level 2', 'menu-enza' ) . "</h2>";
}

// title_sublevel2() displays the page content for the second submenu
// of the custom Enza Toplevel menu
function title_sublevel_2() {
	echo "<h2>" . __( 'Level 3', 'menu-enza' ) . "</h2>";
}
