<?php


// Menu personnalisé dans la barre d'administration de WordPress

add_action('admin_bar_menu', 'wpm_add_custom_items', 100);

function wpm_add_custom_items($admin_bar){

// On définit d'abord notre menu principal
    $admin_bar->add_menu( array(
        'id'    => 'mon-menu-perso',  // On défini l'identifiant du menu
        'title' => 'Mon menu perso',  // On défini le titre du menu
        'href'  => 'http://localhost:8888/TFE/test/ma-ressource',  // On défini le lien vers quoi pointera le menu
        'meta'  => array(
            'title' => __('Mon menu perso'),
        ),
    ));

// On définit un premier sous-menu
    $admin_bar->add_menu( array(
        'id'    => 'mon-sous-menu',
        'parent' => 'mon-menu-perso',  // On défini le menu parent
        'title' => 'Ajouter un plugin',  // Titre de mon sous menu
        'href'  => 'http://localhost:8888/TFE/test/wp-admin/plugin-install.php',
        'meta'  => array(
            'title' => __('Ajouter un plugin'),
            'target' => '_blank',  // Cela signifie que le lien s'ouvrira dans un nouvel onglet
            'class' => 'menu-perso'  // On définit une class CSS si jamais on souhaite le personnaliser dans un second temps
        ),
    ));

// On définit un deuxieme sous-menu
    $admin_bar->add_menu( array(
        'id'    => 'mon-second-sous-menu',
        'parent' => 'mon-menu-perso',
        'title' => 'Réglages WooCommerce',
        'href'  => 'http://localhost:8888/TFE/test/wp-admin/admin.php?page=wc-settings',
        'meta'  => array(
            'title' => __('Réglages WooCommerce'),
            'target' => '_blank',
            'class' => 'menu-perso'
        ),
    ));
}
