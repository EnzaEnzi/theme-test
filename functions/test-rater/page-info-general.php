<?php

/*
Name:   Page Info Général
Description: Page dédier pour les informations pratiques du site
Author: Enza Lombardo
Author URI:
Version: 1.0


1 - initialisation de la page
2 - construire la page
3 - initialisation des paramattre
4 - contruire des paramettre

affichage du formulaire (theme page)

*/

/* ---------------------------------------- */
/* -----------    CREER PAGE    ----------- */
/* ---------------------------------------- */

/* ----  1 - initialisation de la page  ---- */
add_action('admin_menu', 'add_page_information');

/* ----  2 - construire la page  ---- */
function add_page_information(){

    // Menu 1er niveau
    add_menu_page(
        'Information',                         // page title
        'Infos resto',                         // menu title
        'manage_options',                      // capability
        'info-general',                        // slug
        'info_theme_page',                     // function
        'dashicons-info',                      // icon
        100                                    // position
    );

    // Menu 2e niveau
    add_submenu_page(
        'info-general',                        // parent
        'Horaires',                            // page title
        'Horaires',                            // menu title
        'manage_options',                      // capability
        'horaires',                            // slug
        'horaire_theme_page'                   // function
    );
}

/* ----  3 - initialisation des paramattre  ---- */
add_action('admin_init', 'location_custom_settings');
add_action('admin_init', 'social_custom_settings');
add_action('admin_init', 'horaire_custom_settings');



/* ---------------------------------------- */
/* ----  4 - contruire des paramettre  ---- */
/* --------    INFO -> LOCATION    -------- */
/* ---------------------------------------- */

function location_custom_settings(){
    // register
    register_setting( 'location-group', 'adresse' );
    register_setting( 'location-group', 'map' );
    register_setting( 'location-group', 'phone' );

    // settings
    add_settings_section(
        'option-location',          // id
        '',                         // title
        'option_location',          // callback
        'info-general'              // page (parent)
    );

    // field
    add_settings_field(
        'custom-field-location',            // id
        '',                                 // title
        'custom_field_location',            // callback
        'info-general',                     // page (parent)
        'option-location'                   // section
    );
}

function option_location(){

}

function custom_field_location(){

    $adresse = esc_attr( get_option('adresse') );
    $map = esc_attr( get_option('map') );
    $phone = esc_attr( get_option('phone') );

    ?>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">


    <table cellspacing="0">
        <thead>
            <tr>
                <th colspan="2" class="titre-pannel">Coordonnées</th>
            </tr>
        </thead><!-- ./ thead -->
        <tbody>
            <!-- star -> item-location -->
            <tr>
                <td>
                    <label for="adresse">Adresse</label>
                </td>
                <td>
                    <input type="text" id="adresse" name="adresse" value="<?php echo( get_option('adresse') ); ?>" size="75" />
                </td>
            </tr><!-- ./ item-location -->
            <!-- star -> item-location -->
            <tr>
                <td>
                    <label for="map">Google map</label>
                </td>
                <td>
                    <textarea id="map" name="map" cols="75"><?php echo( get_option('map') ); ?></textarea>
                </td>
            </tr><!-- ./ item-location -->
            <!-- star -> item-location -->
            <tr>
                <td>
                    <label for="phone">Téléphone</label>
                </td>
                <td>
                    <input type="text" id="phone" name="phone" value="<?php echo( get_option('phone') ); ?>" size="75" />
                </td>
            </tr><!-- ./ item-location -->
        </tbody><!-- ./ tbody -->
    </table><!-- ./ table -->

    <?php


}


/* ---------------------------------------- */
/* ----  4 - contruire des paramettre  ---- */
/* ---------    INFO -> SOCIAL    --------- */
/* ---------------------------------------- */

function social_custom_settings(){
    // register
    register_setting( 'social-group', 'facebook' );
    register_setting( 'social-group', 'twitter' );
    register_setting( 'social-group', 'instagram' );

    // settings
    add_settings_section(
        'option-social',                // id
        '',                             // title
        'option_social',                // callback
        'info-general'                  // page (parent)
    );

    // field
    add_settings_field(
        'custom-field-social',           // id
        '',                              // title
        'custom_field_social',           // callback
        'info-general',                  // page (parent)
        'option-social'                  // section
    );

}

function option_social(){

}

function custom_field_social(){

    $facebook = esc_attr( get_option('facebook') );
    $twitter = esc_attr( get_option('twitter') );
    $instagram = esc_attr( get_option('instagram') );

    ?>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">


    <table cellspacing="0">
        <thead>
            <tr>
                <th colspan="2" class="titre-pannel">Réseaux sociaux</th>
            </tr>
        </thead><!-- ./ -->
        <tbody>
            <!-- start -> item-social -->
            <tr>
                <td>
                    <label for="facebook">Facebook</label>
                </td>
                <td>
                    <input type="text" id="facebook" name="facebook" value="<?php echo( get_option('facebook') ); ?>" size="75" />
                </td>
            </tr><!-- ./ item-social -->
            <!-- start -> item-social -->
            <tr>
                <td>
                    <label for="twitter">Twitter</label>
                </td>
                <td>
                    <input type="text" id="twitter" name="twitter" value="<?php echo( get_option('twitter') ); ?>" size="75" />
                </td>
            </tr><!-- ./ item-social -->
            <!-- start -> item-social -->
            <tr>
                <td>
                    <label for="instagram">Instagram</label>
                </td>
                <td>
                    <input type="text" id="instagram" name="instagram" value="<?php echo( get_option('instagram') ); ?>" size="75" />
                </td>
            </tr><!-- ./ item-social -->
        </tbody><!-- ./ -->
    </table><!-- ./ -->
    <?php
}

/* ---------------------------------------- */
/* ----  4 - contruire des paramettre  ---- */
/* -------    SUBMENU -> HORAIRE    ------- */
/* ---------------------------------------- */

function horaire_custom_settings(){

    // LUNDI ----------------------------
    register_setting( 'horaire-group', 'lundi_midi_de' );
    register_setting( 'horaire-group', 'lundi_midi_a' );
    register_setting( 'horaire-group', 'lundi_soir_de' );
    register_setting( 'horaire-group', 'lundi_soir_a' );

    // MARDI ----------------------------
    register_setting( 'horaire-group', 'mardi_midi_de' );
    register_setting( 'horaire-group', 'mardi_midi_a' );
    register_setting( 'horaire-group', 'mardi_soir_de' );
    register_setting( 'horaire-group', 'mardi_soir_a' );

    // MERCREDI -------------------------
    register_setting( 'horaire-group', 'mercredi_midi_de' );
    register_setting( 'horaire-group', 'mercredi_midi_a' );
    register_setting( 'horaire-group', 'mercredi_soir_de' );
    register_setting( 'horaire-group', 'mercredi_soir_a' );

    // JEUDI ----------------------------
    register_setting( 'horaire-group', 'jeudi_midi_de' );
    register_setting( 'horaire-group', 'jeudi_midi_a' );
    register_setting( 'horaire-group', 'jeudi_soir_de' );
    register_setting( 'horaire-group', 'jeudi_soir_a' );

    // VENDREDI -------------------------
    register_setting( 'horaire-group', 'vendredi_midi_de' );
    register_setting( 'horaire-group', 'vendredi_midi_a' );
    register_setting( 'horaire-group', 'vendredi_soir_de' );
    register_setting( 'horaire-group', 'vendredi_soir_a' );

    // SAMEDI ---------------------------
    register_setting( 'horaire-group', 'samedi_midi_de' );
    register_setting( 'horaire-group', 'samedi_midi_a' );
    register_setting( 'horaire-group', 'samedi_soir_de' );
    register_setting( 'horaire-group', 'samedi_soir_a' );

    // DIMANDE --------------------------
    register_setting( 'horaire-group', 'dimanche_midi_de' );
    register_setting( 'horaire-group', 'dimanche_midi_a' );
    register_setting( 'horaire-group', 'dimanche_soir_de' );
    register_setting( 'horaire-group', 'dimanche_soir_a' );


    // settings
    add_settings_section(
        'option-horaire',               // id
        '',                             // title
        'option_horaire',               // callback
        'horaires'                      // page (parent -> submenu)
    );


    // field
    add_settings_field(
        'custom-field-horaire',            // id
        '',                                // title
        'custom_field_horaire',            // callback
        'horaires',                        // page (parent -> submenu)
        'option-horaire'                   // section
    );
}

function option_horaire(){

}

function custom_field_horaire(){

    // LUNDI ----------------------------
    $lundi_midi_de = esc_attr( get_option('lundi_midi_de') );
    $lundi_midi_a = esc_attr( get_option('lundi_midi_a') );
    $lundi_soir_de = esc_attr( get_option('lundi_soir_de') );
    $lundi_soir_a = esc_attr( get_option('lundi_soir_a') );

    // MARDI ----------------------------
    $mardi_midi_de = esc_attr( get_option('mardi_midi_de') );
    $mardi_midi_a = esc_attr( get_option('mardi_midi_a') );
    $mardi_soir_de = esc_attr( get_option('mardi_soir_de') );
    $mardi_soir_a = esc_attr( get_option('mardi_soir_a') );

    // MERCREDI -------------------------
    $mercredi_midi_de = esc_attr( get_option('mercredi_midi_de') );
    $mercredi_midi_a = esc_attr( get_option('mercredi_midi_a') );
    $mercredi_soir_de = esc_attr( get_option('mercredi_soir_de') );
    $mercredi_soir_a = esc_attr( get_option('mercredi_soir_a') );

    // JEUDI ----------------------------
    $jeudi_midi_de = esc_attr( get_option('jeudi_midi_de') );
    $jeudi_midi_a = esc_attr( get_option('jeudi_midi_a') );
    $jeudi_soir_de = esc_attr( get_option('jeudi_soir_de') );
    $jeudi_soir_a = esc_attr( get_option('jeudi_soir_a') );

    // VENDREDI -------------------------
    $vendredi_midi_de = esc_attr( get_option('vendredi_midi_de') );
    $vendredi_midi_a = esc_attr( get_option('vendredi_midi_a') );
    $vendredi_soir_de = esc_attr( get_option('vendredi_soir_de') );
    $vendredi_soir_a = esc_attr( get_option('vendredi_soir_a') );

    // SAMEDI ---------------------------
    $samedi_midi_de = esc_attr( get_option('samedi_midi_de') );
    $samedi_midi_a = esc_attr( get_option('samedi_midi_a') );
    $samedi_soir_de = esc_attr( get_option('samedi_soir_de') );
    $samedi_soir_a = esc_attr( get_option('samedi_soir_a') );

    // DIMANDE --------------------------
    $dimanche_midi_de = esc_attr( get_option('dimanche_midi_de') );
    $dimanche_midi_a = esc_attr( get_option('dimanche_midi_a') );
    $dimanche_soir_de = esc_attr( get_option('dimanche_soir_de') );
    $dimanche_soir_a = esc_attr( get_option('dimanche_soir_a') );

    ?>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">Jour</th>
                    <th scope="col">Service du MIDI</th>
                    <th scope="col">Service du SOIR</th>
                </tr>
            </thead><!-- ./ thead -->
            <tbody>

                <!-- lundi -->
                <tr class="item-day">
                    <th>Lundi</th>
                    <td>
                        <span>
                            <label for="lundi_midi_de">de </label>
                            <input type="time" id="lundi_midi_de" name="lundi_midi_de" value="<?php echo( get_option('lundi_midi_de') ); ?>" />
                        </span>
                        <span>
                            <label for="lundi_midi_a">à </label>
                            <input type="time" id="lundi_midi_a" name="lundi_midi_a" value="<?php echo( get_option('lundi_midi_a') ); ?>" />
                        </span>
                    </td><!-- ./ 2e colonne -->
                    <td>
                        <span>
                            <label for="lundi_soir_de">de </label>
                            <input type="time" id="lundi_soir_de" name="lundi_soir_de" value="<?php echo( get_option('lundi_soir_de') ); ?>" />
                        </span>
                        <span>
                            <label for="lundi_soir_a">à </label>
                            <input type="time" id="lundi_soir_a" name="lundi_soir_a" value="<?php echo( get_option('lundi_soir_a') ); ?>" />
                        </span>
                    </td><!-- ./ 3e colonne -->
                </tr><!-- ./ item-day -->

                <!-- mardi -->
                <tr class="item-day">
                    <th>Mardi</th>
                    <td>
                        <span>
                            <label for="mardi_midi_de">de </label>
                            <input type="time" id="mardi_midi_de" name="mardi_midi_de" value="<?php echo( get_option('mardi_midi_de') ); ?>" />
                        </span>
                        <span>
                            <label for="mardi_midi_a">à </label>
                            <input type="time" id="mardi_midi_a" name="mardi_midi_a" value="<?php echo( get_option('mardi_midi_a') ); ?>" />
                        </span>
                    </td><!-- ./ 2e colonne -->
                    <td>
                        <span>
                            <label for="mardi_soir_de">de </label>
                            <input type="time" id="mardi_soir_de" name="mardi_soir_de" value="<?php echo( get_option('mardi_soir_de') ); ?>" />
                        </span>
                        <span>
                            <label for="mardi_soir_a">à </label>
                            <input type="time" id="mardi_soir_a" name="mardi_soir_a" value="<?php echo( get_option('mardi_soir_a') ); ?>" />
                        </span>
                    </td><!-- ./ 3e colonne -->
                </tr><!-- ./ item-day -->

                <!-- mercredi -->
                <tr class="item-day">
                    <th>Mercredi</th>
                    <td>
                        <span>
                            <label for="mercredi_midi_de">de </label>
                            <input type="time" id="mercredi_midi_de" name="mercredi_midi_de" value="<?php echo( get_option('mercredi_midi_de') ); ?>" />
                        </span>
                        <span>
                            <label for="mercredi_midi_a">à </label>
                            <input type="time" id="mercredi_midi_a" name="mercredi_midi_a" value="<?php echo( get_option('mercredi_midi_a') ); ?>" />
                        </span>
                    </td><!-- ./ 2e colonne -->
                    <td>
                        <span>
                            <label for="mercredi_soir_de">de </label>
                            <input type="time" id="mercredi_soir_de" name="mercredi_soir_de" value="<?php echo( get_option('mercredi_soir_de') ); ?>" />
                        </span>
                        <span>
                            <label for="mercredi_soir_a">à </label>
                            <input type="time" id="mercredi_soir_a" name="mercredi_soir_a" value="<?php echo( get_option('mercredi_soir_a') ); ?>" />
                        </span>
                    </td><!-- ./ 3e colonne -->
                </tr><!-- ./ item-day -->

                <!-- jeudi -->
                <tr class="item-day">
                    <th>Jeudi</th>
                    <td>
                        <span>
                            <label for="jeudi_midi_de">de </label>
                            <input type="time" id="jeudi_midi_de" name="jeudi_midi_de" value="<?php echo( get_option('jeudi_midi_de') ); ?>" />
                        </span>
                        <span>
                            <label for="jeudi_midi_a">à </label>
                            <input type="time" id="jeudi_midi_a" name="jeudi_midi_a" value="<?php echo( get_option('jeudi_midi_a') ); ?>" />
                        </span>
                    </td><!-- ./ 2e colonne -->
                    <td>
                        <span>
                            <label for="jeudi_soir_de">de </label>
                            <input type="time" id="jeudi_soir_de" name="jeudi_soir_de" value="<?php echo( get_option('jeudi_soir_de') ); ?>" />
                        </span>
                        <span>
                            <label for="jeudi_soir_a">à </label>
                            <input type="time" id="jeudi_soir_a" name="jeudi_soir_a" value="<?php echo( get_option('jeudi_soir_a') ); ?>" />
                        </span>
                    </td><!-- ./ 3e colonne -->
                </tr><!-- ./ item-day -->

                <!-- vendredi -->
                <tr class="item-day">
                    <th>Vendredi</th>
                    <td>
                        <span>
                            <label for="vendredi_midi_de">de </label>
                            <input type="time" id="vendredi_midi_de" name="vendredi_midi_de" value="<?php echo( get_option('vendredi_midi_de') ); ?>" />
                        </span>
                        <span>
                            <label for="vendredi_midi_a">à </label>
                            <input type="time" id="vendredi_midi_a" name="vendredi_midi_a" value="<?php echo( get_option('vendredi_midi_a') ); ?>" />
                        </span>
                    </td><!-- ./ 2e colonne -->
                    <td>
                        <span>
                            <label for="vendredi_soir_de">de </label>
                            <input type="time" id="vendredi_soir_de" name="vendredi_soir_de" value="<?php echo( get_option('vendredi_soir_de') ); ?>" />
                        </span>
                        <span>
                            <label for="vendredi_soir_a">à </label>
                            <input type="time" id="vendredi_soir_a" name="vendredi_soir_a" value="<?php echo( get_option('vendredi_soir_a') ); ?>" />
                        </span>
                    </td><!-- ./ 3e colonne -->
                </tr><!-- ./ item-day -->

                <!-- samedi -->
                <tr class="item-day">
                    <th>Samedi</th>
                    <td>
                        <span>
                            <label for="samedi_midi_de">de </label>
                            <input type="time" id="samedi_midi_de" name="samedi_midi_de" value="<?php echo( get_option('samedi_midi_de') ); ?>" />
                        </span>
                        <span>
                            <label for="samedi_midi_a">à </label>
                            <input type="time" id="samedi_midi_a" name="samedi_midi_a" value="<?php echo( get_option('samedi_midi_a') ); ?>" />
                        </span>
                    </td><!-- ./ 2e colonne -->
                    <td>
                        <span>
                            <label for="samedi_soir_de">de </label>
                            <input type="time" id="samedi_soir_de" name="samedi_soir_de" value="<?php echo( get_option('samedi_soir_de') ); ?>" />
                        </span>
                        <span>
                            <label for="samedi_soir_a">à </label>
                            <input type="time" id="samedi_soir_a" name="samedi_soir_a" value="<?php echo( get_option('samedi_soir_a') ); ?>" />
                        </span>
                    </td><!-- ./ 3e colonne -->
                </tr><!-- ./ item-day -->

                <!-- dimanche -->
                <tr class="item-day">
                    <th>Dimanche</th>
                    <td>
                        <span>
                            <label for="dimanche_midi_de">de </label>
                            <input type="time" id="dimanche_midi_de" name="dimanche_midi_de" value="<?php echo( get_option('dimanche_midi_de') ); ?>" />
                        </span>
                        <span>
                            <label for="dimanche_midi_a">à </label>
                            <input type="time" id="dimanche_midi_a" name="dimanche_midi_a" value="<?php echo( get_option('dimanche_midi_a') ); ?>" />
                        </span>
                    </td><!-- ./ 2e colonne -->
                    <td>
                        <span>
                            <label for="dimanche_soir_de">de </label>
                            <input type="time" id="dimanche_soir_de" name="dimanche_soir_de" value="<?php echo( get_option('dimanche_soir_de') ); ?>" />
                        </span>
                        <span>
                            <label for="dimanche_soir_a">à </label>
                            <input type="time" id="dimanche_soir_a" name="dimanche_soir_a" value="<?php echo( get_option('dimanche_soir_a') ); ?>" />
                        </span>
                    </td><!-- ./ 3e colonne -->
                </tr><!-- ./ item-day -->

            </tbody><!-- ./ tbody -->
        </table><!-- ./ table -->

    <?php

}

/* -------------------------------------------- */
/* --------    AFFICHAGE THEME PAGE    -------- */
/* -------------------------------------------- */

// theme page => info general
function info_theme_page(){
    require_once( get_template_directory() . '/functions/templates/theme-page-information.php' );
}



// theme page => horaire
function horaire_theme_page(){
    require_once( get_template_directory() . '/functions/templates/theme-page-horaire.php' );
}
