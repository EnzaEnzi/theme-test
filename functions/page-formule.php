<?php

/*
Name:   Page Formule
Description: Création d'une page pour gérer les différente formules du buffet
Author: Enza Lombardo
Author URI: www.enzalombardo.be
copyright : 2019 © Enza Lombardo
Version: 1.2
*/


/* ----------------------------------------------------------------------------- */
/* ADD MENU PAGE */
/* ----------------------------------------------------------------------------- */

// initialisation de la page -----------------------------
add_action('admin_menu', 'add_page_formule');


// construire la page -----------------------------
function add_page_formule(){

    // Menu 1er niveau
    add_menu_page(
        'Formules',                             // page_title
        'Formules',                             // menu_title
        'manage_options',                       // capability
        'formule',                              // slug_menu
        'theme_page_formule',                   // function qui rendra la sortie
        'dashicons-admin-page',                 // icon
        100                                     // position
    );
} // END => add_page_formule

// TAB : créer un tableau dynamique
function tabs_formule(){
    $tabs = array(
        'sans_boissons' => 'Sans Boissons',
        'avec_boissons' => 'Avec Boissons',
        'full_boissons'	=> 'Full Boissons'
    );
    return apply_filters('tabs_formule', $tabs);
} // END =>  tabs_formule

/* ----------------------------------------------------------------------------- */
/* THEME PAGE */
/* ----------------------------------------------------------------------------- */

// PAGE 1er NIVEAU -----------------------------
function theme_page_formule(){
    ?>

    <?php
        $tabs = tabs_formule();
        $current = sanitize_text_field($_GET['tab']);
    ?>

    <div class="wrap">
        <h1 class="wp-heading-inline">Formules</h1>
        <div class="description">This is description of the page.</div>
        <?php settings_errors(); ?>


        <h2 class="nav-tab-wrapper">
            <?php
            if(!empty($tabs)){
                foreach($tabs as $key => $value){
                    $class = ( $key == $current ) ? ' nav-tab-active' : '';
                    ?>
                    <a href="?page=formule&tab=<?php echo $key; ?>" class="nav-tab<?php echo $class; ?>"><?php echo $value; ?></a>
                    <?php
                }
            }
            ?>
        </h2><!-- / .nav-tab-wrapper -->

        <div class="">
            <form class=""  method="post" action="options.php">

                <?php

                    if($_GET['tab'] == 'avec_boissons'){
                        ?>
                            <h3>Buffet avec boissons</h3>
                            <div class="" >
                                <?php settings_fields( 'avec-boissons-group' );?>
                                <?php do_settings_sections( 'formule-avec-boissons' ); ?>
                            </div>
                        <?php
                    } elseif($_GET['tab'] == 'full_boissons'){
                        ?>
                            <h3>Buffet full boissons</h3>
                            <div class="" >
                                <?php settings_fields( 'full-boissons-group' );?>
                                <?php do_settings_sections( 'formule-full-boissons' ); ?>
                            </div>
                        <?php
                    } else {
                        ?>
                        <h3>Buffet sans boissons</h3>
                        <div class="" >
                            <?php settings_fields( 'sans-boissons-group' );?>
                            <?php do_settings_sections( 'formule' ); ?>
                        </div>
                        <?php
                    }

                 ?>

                <?php submit_button(); ?>
            </form><!-- / -->

        </div><!-- / -->
    </div><!-- / .wrap -->
    <?php
} // END => theme_page_formule


/* ----------------------------------------------------------------------------- */
/* SETTING SECTION AND FIED */
/* ----------------------------------------------------------------------------- */

// initialisation des paramattre -----------------
add_action('admin_init', 'custom_settings_formule');


// contruire des paramettres -----------------------------
function custom_settings_formule(){

    /* ----------------------------------------------------------------------------- */
    /* Option 1 -- SANS boissons */
    /* ----------------------------------------------------------------------------- */

    // SETTINGS : Option 1 -- SANS boissons ------------------------------------
    add_settings_section(
        'section_sans_boisson',                                                 // ID (id used to identify the field throughout the theme)
        __('', 'section_sans_boisson'),                                         // TITLE (title to be displayed on the administration page)
        'option_section_sans_boisson',                                          // CALLBACK (callback used to render the description of the section)
        'formule'                                                               // PAGE (page on which to add this section of options)
    );

    // FIELDS : Checklist ==> Option 1 -- SANS boissons   ----------------------
    add_settings_field(
        'sans_checklist',                                                       // ID -- ID used to identify the field throughout the theme
        __('La formule sans boissons comprend', 'section_sans_boisson'),        // LABEL -- The label to the left of the option interface element
        'custom_field_checklist_sans_boissons',                                 // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'formule',                                                              // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_sans_boisson'                                                  // SECTION ID -- The name of the section to which this field belongs
    );

    // FIELDS : Tarif ==> Option 1 -- SANS boissons   --------------------------
    add_settings_field(
        'sans_tarif',                                                           // ID -- ID used to identify the field throughout the theme
        __('Les tarifs par jour', 'section_sans_boisson'),                      // LABEL -- The label to the left of the option interface element
        'custom_field_tarif_sans_boissons',                                     // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'formule',                                                              // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_sans_boisson'                                                  // SECTION ID -- The name of the section to which this field belongs
    );


    // REGISTER : Option 1 -- SANS boissons   ---------------------------------
    // checklist
    register_setting('sans-boissons-group', 'sans_apero');
    register_setting('sans-boissons-group', 'sans_vin');
    register_setting('sans-boissons-group', 'sans_soft');
    register_setting('sans-boissons-group', 'sans_cafe');
    register_setting('sans-boissons-group', 'sans_digestif');
    // tarif - lundi
    register_setting('sans-boissons-group', 'sans_lundi_midi');
    register_setting('sans-boissons-group', 'sans_lundi_soir');
    // tarif - mardi
    register_setting('sans-boissons-group', 'sans_mardi_midi');
    register_setting('sans-boissons-group', 'sans_mardi_soir');
    // tarif - mercredi
    register_setting('sans-boissons-group', 'sans_mercredi_midi');
    register_setting('sans-boissons-group', 'sans_mercredi_soir');
    // tarif - jeudi
    register_setting('sans-boissons-group', 'sans_jeudi_midi');
    register_setting('sans-boissons-group', 'sans_jeudi_soir');
    // tarif - vendredi
    register_setting('sans-boissons-group', 'sans_vendredi_midi');
    register_setting('sans-boissons-group', 'sans_vendredi_soir');
    // tarif - samedi
    register_setting('sans-boissons-group', 'sans_samedi_midi');
    register_setting('sans-boissons-group', 'sans_samedi_soir');
    // tarif - dimanche
    register_setting('sans-boissons-group', 'sans_dimanche_midi');
    register_setting('sans-boissons-group', 'sans_dimanche_soir');

    /* ----------------------------------------------------------------------------- */
    /* Option 2 -- AVEC boissons */
    /* ----------------------------------------------------------------------------- */

    // SETTINGS : Option 2 -- AVEC boissons ------------------------------------
    add_settings_section(
        'section_avec_boisson',                                                 // ID (id used to identify the field throughout the theme)
        __('', 'section_avec_boisson'),                                         // TITLE (title to be displayed on the administration page)
        'option_section_avec_boisson',                                          // CALLBACK (callback used to render the description of the section)
        'formule-avec-boissons'                                                 // PAGE (page on which to add this section of options)
    );

    // FIELDS : Checklist ==> Option 2 -- AVEC boissons   ----------------------
    add_settings_field(
        'avec_checklist',                                                       // ID -- ID used to identify the field throughout the theme
        __('La formule avec boissons comprend', 'section_avec_boisson'),        // LABEL -- The label to the left of the option interface element
        'custom_field_checklist_avec_boissons',                                 // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'formule-avec-boissons',                                                // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_avec_boisson'                                                  // SECTION ID -- The name of the section to which this field belongs
    );

    // FIELDS : Tarif ==> Option 2 -- AVEC boissons   --------------------------
    add_settings_field(
        'avec_tarif',                                                           // ID -- ID used to identify the field throughout the theme
        __('Les tarifs par jour', 'section_avec_boisson'),                      // LABEL -- The label to the left of the option interface element
        'custom_field_tarif_avec_boissons',                                     // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'formule-avec-boissons',                                                // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_avec_boisson'                                                  // SECTION ID -- The name of the section to which this field belongs
    );

    /// REGISTER : Option 2 -- AVEC boissons   ---------------------------------
    //checklist
    register_setting('avec-boissons-group', 'avec_apero');
    register_setting('avec-boissons-group', 'avec_vin');
    register_setting('avec-boissons-group', 'avec_soft');
    register_setting('avec-boissons-group', 'avec_cafe');
    register_setting('avec-boissons-group', 'avec_digestif');
    // tarif - lundi
    register_setting('avec-boissons-group', 'avec_lundi_midi');
    register_setting('avec-boissons-group', 'avec_lundi_soir');
    // tarif - mardi
    register_setting('avec-boissons-group', 'avec_mardi_midi');
    register_setting('avec-boissons-group', 'avec_mardi_soir');
    // tarif - mercredi
    register_setting('avec-boissons-group', 'avec_mercredi_midi');
    register_setting('avec-boissons-group', 'avec_mercredi_soir');
    // tarif - jeudi
    register_setting('avec-boissons-group', 'avec_jeudi_midi');
    register_setting('avec-boissons-group', 'avec_jeudi_soir');
    // tarif - vendredi
    register_setting('avec-boissons-group', 'avec_vendredi_midi');
    register_setting('avec-boissons-group', 'avec_vendredi_soir');
    // tarif - samedi
    register_setting('avec-boissons-group', 'avec_samedi_midi');
    register_setting('avec-boissons-group', 'avec_samedi_soir');
    // tarif - dimanche
    register_setting('avec-boissons-group', 'avec_dimanche_midi');
    register_setting('avec-boissons-group', 'avec_dimanche_soir');

    /* ----------------------------------------------------------------------------- */
    /* Option 3 -- FULL boissons */
    /* ----------------------------------------------------------------------------- */

    // SETTINGS : Option 3 -- FULL boissons ------------------------------------
    add_settings_section(
        'section_full_boisson',                                                 // ID (id used to identify the field throughout the theme)
        __('', 'section_full_boisson'),                                         // TITLE (title to be displayed on the administration page)
        'option_section_full_boisson',                                          // CALLBACK (callback used to render the description of the section)
        'formule-full-boissons'                                                 // PAGE (page on which to add this section of options)
    );

    // FIELDS : Checklist ==> Option 3 -- FULL boissons   ----------------------
    add_settings_field(
        'full_checklist',                                                       // ID -- ID used to identify the field throughout the theme
        __('La formule full boissons comprend', 'section_full_boisson'),        // LABEL -- The label to the left of the option interface element
        'custom_field_checklist_full_boissons',                                 // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'formule-full-boissons',                                                // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_full_boisson'                                                  // SECTION ID -- The name of the section to which this field belongs
    );

    // FIELDS : Tarif ==> Option 3 -- FULL boissons   --------------------------
    add_settings_field(
        'full_tarif',                                                           // ID -- ID used to identify the field throughout the theme
        __('Les tarifs par jour', 'formule_full_boissons'),                     // LABEL -- The label to the left of the option interface element
        'custom_field_tarif_full_boissons',                                     // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface
        'formule-full-boissons',                                                // MENU PAGE SLUG -- The page on which this option will be displayed
        'section_full_boisson'                                                  // SECTION ID -- The name of the section to which this field belongs
    );

    // REGISTER : Option 3 -- FULL boissons ----------------------------
    // checklist
    register_setting('full-boissons-group', 'full_apero');
    register_setting('full-boissons-group', 'full_vin');
    register_setting('full-boissons-group', 'full_soft');
    register_setting('full-boissons-group', 'full_cafe');
    register_setting('full-boissons-group', 'full_digestif');
    // tarif - lundi
    register_setting('full-boissons-group', 'full_lundi_midi');
    register_setting('full-boissons-group', 'full_lundi_soir');
    // tarif - mardi
    register_setting('full-boissons-group', 'full_mardi_midi');
    register_setting('full-boissons-group', 'full_mardi_soir');
    // tarif - mercredi
    register_setting('full-boissons-group', 'full_mercredi_midi');
    register_setting('full-boissons-group', 'full_mercredi_soir');
    // tarif - jeudi
    register_setting('full-boissons-group', 'full_jeudi_midi');
    register_setting('full-boissons-group', 'full_jeudi_soir');
    // tarif - vendredi
    register_setting('full-boissons-group', 'full_vendredi_midi');
    register_setting('full-boissons-group', 'full_vendredi_soir');
    // tarif - samedi
    register_setting('full-boissons-group', 'full_samedi_midi');
    register_setting('full-boissons-group', 'full_samedi_soir');
    // tarif - dimanche
    register_setting('full-boissons-group', 'full_dimanche_midi');
    register_setting('full-boissons-group', 'full_dimanche_soir');


} // END => custom_settings_formule


/* ----------------------------------------------------------------------------- */
/* FIELD CALLBACK --> SECTION */
/* ----------------------------------------------------------------------------- */

// CALLBACK SETTINGS :  Option 1 -- SANS boissons
function option_section_sans_boisson(){}
// CALLBACK SETTINGS :  Option 2 -- AVEC boissons
function option_section_avec_boisson(){}
// CALLBACK SETTINGS :  Option 3 -- FULL boissons
function option_section_full_boisson(){}

/* ----------------------------------------------------------------------------- */
/* Option 1 -- SANS boissons */
/* ----------------------------------------------------------------------------- */
// CALLBACK FIELD : Cheklist  [Option 1 -- SANS boissons]
function custom_field_checklist_sans_boissons(){
    $sans_apero = esc_attr(get_option('sans_apero'));
    $sans_vin = esc_attr(get_option('sans_vin'));
    $sans_soft = esc_attr(get_option('sans_soft'));
    $sans_cafe = esc_attr(get_option('sans_cafe'));
    $sans_digestif = esc_attr(get_option('sans_digestif'));
    ?>

        <div class="checklist">
            <input type="checkbox" id="sans_apero" name="sans_apero" value="1" <?php checked(1, get_option('sans_apero'), true); ?> /> Apérifif
        </div><!-- / .checklist -->

        <div class="checklist">
            <input type="checkbox" id="sans_vin" name="sans_vin" value="1" <?php checked(1, get_option('sans_vin'), true); ?> /> 1/2l de vin par personne
        </div><!-- / .checklist -->

        <div class="checklist">
            <input type="checkbox" id="sans_soft" name="sans_soft" value="1" <?php checked(1, get_option('sans_soft'), true); ?> /> Soft à volonter
        </div><!-- / .checklist -->

        <div class="checklist">
            <input type="checkbox" id="sans_cafe" name="sans_cafe" value="1" <?php checked(1, get_option('sans_cafe'), true); ?> /> Thé ou Café
        </div><!-- / .checklist -->

        <div class="checklist">
            <input type="checkbox" id="sans_digestif" name="sans_digestif" value="1" <?php checked(1, get_option('sans_digestif'), true); ?> /> Digestif
        </div><!-- / .checklist -->


    <?php
} // END => custom_field_checklist_sans_boissons

// CALLBACK FIELD : Tarif  [Option 1 -- SANS boissons]
function custom_field_tarif_sans_boissons(){
    // lundi
    $sans_lundi_midi = esc_attr( get_option('sans_lundi_midi') );
    $sans_lundi_soir = esc_attr( get_option('sans_lundi_soir') );
    // mardi
    $sans_mardi_midi = esc_attr( get_option('sans_mardi_midi') );
    $sans_mardi_soir = esc_attr( get_option('sans_mardi_soir') );
    // mercredi
    $sans_mercredi_midi = esc_attr( get_option('sans_mercredi_midi') );
    $sans_mercredi_soir = esc_attr( get_option('sans_mercredi_soir') );
    // jeudi
    $sans_jeudi_midi = esc_attr( get_option('sans_jeudi_midi') );
    $sans_jeudi_soir = esc_attr( get_option('sans_jeudi_soir') );
    // vendredi
    $sans_vendredi_midi = esc_attr( get_option('sans_vendredi_midi') );
    $sans_vendredi_soir = esc_attr( get_option('sans_vendredi_soir') );
    // samedi
    $sans_samedi_midi = esc_attr( get_option('sans_samedi_midi') );
    $sans_samedi_soir = esc_attr( get_option('sans_samedi_soir') );
    // dimanche
    $sans_dimanche_midi = esc_attr( get_option('sans_dimanche_midi') );
    $sans_dimanche_soir = esc_attr( get_option('sans_dimanche_soir') );
    ?>

    <table class="table-tarif">
        <thead>
            <th scope="col" class="day"></th>
            <th scope="col" class="tarif">Midi</th>
            <th scope="col" class="tarif">Soir</th>
        </thead>
        <tbody>
            <tr class="item-tarif-day">
                <td class="day">Lundi</td>
                <td class="tarif"><input type="text" id="sans_lundi_midi" name="sans_lundi_midi" value="<?php echo( get_option('sans_lundi_midi') ); ?>" /> <span>€</span></td>
                <td class="tarif"><input type="text" id="sans_lundi_soir" name="sans_lundi_soir" value="<?php echo( get_option('sans_lundi_soir') ); ?>" /> <span>€</span></td>
            </tr>
            <tr class="item-tarif-day">
                <td class="day">Mardi</td>
                <td class="tarif"><input type="text" id="sans_mardi_midi" name="sans_mardi_midi" value="<?php echo( get_option('sans_mardi_midi') ); ?>" /> <span>€</span></td>
                <td class="tarif"><input type="text" id="sans_mardi_soir" name="sans_mardi_soir" value="<?php echo( get_option('sans_mardi_soir') ); ?>" /> <span>€</span></td>
            </tr>
            <tr class="item-tarif-day">
                <td class="day">Mercredi</td>
                <td class="tarif"><input type="text" id="sans_mercredi_midi" name="sans_mercredi_midi" value="<?php echo( get_option('sans_mercredi_midi') ); ?>" /> <span>€</span></td>
                <td class="tarif"><input type="text" id="sans_mercredi_soir" name="sans_mercredi_soir" value="<?php echo( get_option('sans_mercredi_soir') ); ?>" /> <span>€</span></td>
            </tr>
            <tr class="item-tarif-day">
                <td class="day">Jeudi</td>
                <td class="tarif"><input type="text" id="sans_jeudi_midi" name="sans_jeudi_midi" value="<?php echo( get_option('sans_jeudi_midi') ); ?>" /> <span>€</span></td>
                <td class="tarif"><input type="text" id="sans_jeudi_soir" name="sans_jeudi_soir" value="<?php echo( get_option('sans_jeudi_soir') ); ?>" /> <span>€</span></td>
            </tr>
            <tr class="item-tarif-day">
                <td class="day">Vendredi</td>
                <td class="tarif"><input type="text" id="sans_vendredi_midi" name="sans_vendredi_midi" value="<?php echo( get_option('sans_vendredi_midi') ); ?>" /> <span>€</span></td>
                <td class="tarif"><input type="text" id="sans_vendredi_soir" name="sans_vendredi_soir" value="<?php echo( get_option('sans_vendredi_soir') ); ?>" /> <span>€</span></td>
            </tr>
            <tr class="item-tarif-day">
                <td class="day">Samedi</td>
                <td class="tarif"><input type="text" id="sans_samedi_midi" name="sans_samedi_midi" value="<?php echo( get_option('sans_samedi_midi') ); ?>" /> <span>€</span></td>
                <td class="tarif"><input type="text" id="sans_samedi_soir" name="sans_samedi_soir" value="<?php echo( get_option('sans_samedi_soir') ); ?>" /> <span>€</span></td>
            </tr>
            <tr class="item-tarif-day">
                <td class="day">Dimanche</td>
                <td class="tarif"><input type="text" id="sans_dimanche_midi" name="sans_dimanche_midi" value="<?php echo( get_option('sans_dimanche_midi') ); ?>" /> <span>€</span></td>
                <td class="tarif"><input type="text" id="sans_dimanche_soir" name="sans_dimanche_soir" value="<?php echo( get_option('sans_dimanche_soir') ); ?>" /> <span>€</span></td>
            </tr>
        </tbody>
    </table>

    <?php
} // END => custom_field_tarif_sans_boissons

/* ----------------------------------------------------------------------------- */
/* FIELD CALLBACK --> Option 2 -- AVEC boissons */
/* ----------------------------------------------------------------------------- */
// CALLBACK FIELD : Cheklist  [Option 2 -- AVEC boissons]
function custom_field_checklist_avec_boissons(){
    $avec_apero = esc_attr(get_option('avec_apero'));
        $avec_vin = esc_attr(get_option('avec_vin'));
        $avec_soft = esc_attr(get_option('avec_soft'));
        $avec_cafe = esc_attr(get_option('avec_cafe'));
        $avec_digestif = esc_attr(get_option('avec_digestif'));
        ?>

            <div class="checklist">
                <input type="checkbox" id="avec_apero" name="avec_apero" value="1" <?php checked(1, get_option('avec_apero'), true); ?> /> Apérifif
            </div><!-- / .checklist -->

            <div class="checklist">
                <input type="checkbox" id="avec_vin" name="avec_vin" value="1" <?php checked(1, get_option('avec_vin'), true); ?> /> 1/2l de vin par personne
            </div><!-- / .checklist -->

            <div class="checklist">
                <input type="checkbox" id="avec_soft" name="avec_soft" value="1" <?php checked(1, get_option('avec_soft'), true); ?> /> Soft à volonter
            </div><!-- / .checklist -->

            <div class="checklist">
                <input type="checkbox" id="avec_cafe" name="avec_cafe" value="1" <?php checked(1, get_option('avec_cafe'), true); ?> /> Thé ou Café
            </div><!-- / .checklist -->

            <div class="checklist">
                <input type="checkbox" id="avec_digestif" name="avec_digestif" value="1" <?php checked(1, get_option('avec_digestif'), true); ?> /> Digestif
            </div><!-- / .checklist -->


        <?php
} // END => custom_field_checklist_avec_boissons

// CALLBACK FIELD : Tarif  [Option 2 -- AVEC boissons]
function custom_field_tarif_avec_boissons(){
    // lundi
        $avec_lundi_midi = esc_attr( get_option('avec_lundi_midi') );
        $avec_lundi_soir = esc_attr( get_option('avec_lundi_soir') );
        // mardi
        $avec_mardi_midi = esc_attr( get_option('avec_mardi_midi') );
        $avec_mardi_soir = esc_attr( get_option('avec_mardi_soir') );
        // mercredi
        $avec_mercredi_midi = esc_attr( get_option('avec_mercredi_midi') );
        $avec_mercredi_soir = esc_attr( get_option('avec_mercredi_soir') );
        // jeudi
        $avec_jeudi_midi = esc_attr( get_option('avec_jeudi_midi') );
        $avec_jeudi_soir = esc_attr( get_option('avec_jeudi_soir') );
        // vendredi
        $avec_vendredi_midi = esc_attr( get_option('avec_vendredi_midi') );
        $avec_vendredi_soir = esc_attr( get_option('avec_vendredi_soir') );
        // samedi
        $avec_samedi_midi = esc_attr( get_option('avec_samedi_midi') );
        $avec_samedi_soir = esc_attr( get_option('avec_samedi_soir') );
        // dimanche
        $avec_dimanche_midi = esc_attr( get_option('avec_dimanche_midi') );
        $avec_dimanche_soir = esc_attr( get_option('avec_dimanche_soir') );
        ?>

        <table class="table-tarif">
            <thead>
                <th scope="col" class="day"></th>
                <th scope="col" class="tarif">Midi</th>
                <th scope="col" class="tarif">Soir</th>
            </thead>
            <tbody>
                <tr class="item-tarif-day">
                    <td class="day">Lundi</td>
                    <td class="tarif"><input type="text" id="avec_lundi_midi" name="avec_lundi_midi" value="<?php echo( get_option('avec_lundi_midi') ); ?>" /> <span>€</span></td>
                    <td class="tarif"><input type="text" id="avec_lundi_soir" name="avec_lundi_soir" value="<?php echo( get_option('avec_lundi_soir') ); ?>" /> <span>€</span></td>
                </tr>
                <tr class="item-tarif-day">
                    <td class="day">Mardi</td>
                    <td class="tarif"><input type="text" id="avec_mardi_midi" name="avec_mardi_midi" value="<?php echo( get_option('avec_mardi_midi') ); ?>" /> <span>€</span></td>
                    <td class="tarif"><input type="text" id="avec_mardi_soir" name="avec_mardi_soir" value="<?php echo( get_option('avec_mardi_soir') ); ?>" /> <span>€</span></td>
                </tr>
                <tr class="item-tarif-day">
                    <td class="day">Mercredi</td>
                    <td class="tarif"><input type="text" id="avec_mercredi_midi" name="avec_mercredi_midi" value="<?php echo( get_option('avec_mercredi_midi') ); ?>" /> <span>€</span></td>
                    <td class="tarif"><input type="text" id="avec_mercredi_soir" name="avec_mercredi_soir" value="<?php echo( get_option('avec_mercredi_soir') ); ?>" /> <span>€</span></td>
                </tr>
                <tr class="item-tarif-day">
                    <td class="day">Jeudi</td>
                    <td class="tarif"><input type="text" id="avec_jeudi_midi" name="avec_jeudi_midi" value="<?php echo( get_option('avec_jeudi_midi') ); ?>" /> <span>€</span></td>
                    <td class="tarif"><input type="text" id="avec_jeudi_soir" name="avec_jeudi_soir" value="<?php echo( get_option('avec_jeudi_soir') ); ?>" /> <span>€</span></td>
                </tr>
                <tr class="item-tarif-day">
                    <td class="day">Vendredi</td>
                    <td class="tarif"><input type="text" id="avec_vendredi_midi" name="avec_vendredi_midi" value="<?php echo( get_option('avec_vendredi_midi') ); ?>" /> <span>€</span></td>
                    <td class="tarif"><input type="text" id="avec_vendredi_soir" name="avec_vendredi_soir" value="<?php echo( get_option('avec_vendredi_soir') ); ?>" /> <span>€</span></td>
                </tr>
                <tr class="item-tarif-day">
                    <td class="day">Samedi</td>
                    <td class="tarif"><input type="text" id="avec_samedi_midi" name="avec_samedi_midi" value="<?php echo( get_option('avec_samedi_midi') ); ?>" /> <span>€</span></td>
                    <td class="tarif"><input type="text" id="avec_samedi_soir" name="avec_samedi_soir" value="<?php echo( get_option('avec_samedi_soir') ); ?>" /> <span>€</span></td>
                </tr>
                <tr class="item-tarif-day">
                    <td class="day">Dimanche</td>
                    <td class="tarif"><input type="text" id="avec_dimanche_midi" name="avec_dimanche_midi" value="<?php echo( get_option('avec_dimanche_midi') ); ?>" /> <span>€</span></td>
                    <td class="tarif"><input type="text" id="avec_dimanche_soir" name="avec_dimanche_soir" value="<?php echo( get_option('avec_dimanche_soir') ); ?>" /> <span>€</span></td>
                </tr>
            </tbody>
        </table>

        <?php
} // END => custom_field_tarif_avec_boissons


/* ----------------------------------------------------------------------------- */
/* FIELD CALLBACK --> Option 3 -- FULL boissons */
/* ----------------------------------------------------------------------------- */
// CALLBACK FIELD : Cheklist  [Option 3 -- FULL boissons]
function custom_field_checklist_full_boissons(){
    $full_apero = esc_attr(get_option('full_apero'));
    $full_vin = esc_attr(get_option('full_vin'));
    $full_soft = esc_attr(get_option('full_soft'));
    $full_cafe = esc_attr(get_option('full_cafe'));
    $full_digestif = esc_attr(get_option('full_digestif'));
    ?>

        <div class="checklist">
            <input type="checkbox" id="full_apero" name="full_apero" value="1" <?php checked(1, get_option('full_apero'), true); ?> /> Apérifif
        </div><!-- / .checklist -->

        <div class="checklist">
            <input type="checkbox" id="full_vin" name="full_vin" value="1" <?php checked(1, get_option('full_vin'), true); ?> /> 1/2l de vin par personne
        </div><!-- / .checklist -->

        <div class="checklist">
            <input type="checkbox" id="full_soft" name="full_soft" value="1" <?php checked(1, get_option('full_soft'), true); ?> /> Soft à volonter
        </div><!-- / .checklist -->

        <div class="checklist">
            <input type="checkbox" id="full_cafe" name="full_cafe" value="1" <?php checked(1, get_option('full_cafe'), true); ?> /> Thé ou Café
        </div><!-- / .checklist -->

        <div class="checklist">
            <input type="checkbox" id="full_digestif" name="full_digestif" value="1" <?php checked(1, get_option('full_digestif'), true); ?> /> Digestif
        </div><!-- / .checklist -->


    <?php
} // END => custom_field_checklist_full_boissons

// CALLBACK FIELD : Tarif  [Option 3 -- FULL boissons]
function custom_field_tarif_full_boissons(){
    // lundi
    $full_lundi_midi = esc_attr( get_option('full_lundi_midi') );
    $full_lundi_soir = esc_attr( get_option('full_lundi_soir') );
    // mardi
    $full_mardi_midi = esc_attr( get_option('full_mardi_midi') );
    $full_mardi_soir = esc_attr( get_option('full_mardi_soir') );
    // mercredi
    $full_mercredi_midi = esc_attr( get_option('full_mercredi_midi') );
    $full_mercredi_soir = esc_attr( get_option('full_mercredi_soir') );
    // jeudi
    $full_jeudi_midi = esc_attr( get_option('full_jeudi_midi') );
    $full_jeudi_soir = esc_attr( get_option('full_jeudi_soir') );
    // vendredi
    $full_vendredi_midi = esc_attr( get_option('full_vendredi_midi') );
    $full_vendredi_soir = esc_attr( get_option('full_vendredi_soir') );
    // samedi
    $full_samedi_midi = esc_attr( get_option('full_samedi_midi') );
    $full_samedi_soir = esc_attr( get_option('full_samedi_soir') );
    // dimanche
    $full_dimanche_midi = esc_attr( get_option('full_dimanche_midi') );
    $full_dimanche_soir = esc_attr( get_option('full_dimanche_soir') );
    ?>

    <table class="table-tarif">
        <thead>
            <th scope="col" class="day"></th>
            <th scope="col" class="tarif">Midi</th>
            <th scope="col" class="tarif">Soir</th>
        </thead>
        <tbody>
            <tr class="item-tarif-day">
                <td class="day">Lundi</td>
                <td class="tarif"><input type="text" id="full_lundi_midi" name="full_lundi_midi" value="<?php echo( get_option('full_lundi_midi') ); ?>" /> <span>€</span></td>
                <td class="tarif"><input type="text" id="full_lundi_soir" name="full_lundi_soir" value="<?php echo( get_option('full_lundi_soir') ); ?>" /> <span>€</span></td>
            </tr>
            <tr class="item-tarif-day">
                <td class="day">Mardi</td>
                <td class="tarif"><input type="text" id="full_mardi_midi" name="full_mardi_midi" value="<?php echo( get_option('full_mardi_midi') ); ?>" /> <span>€</span></td>
                <td class="tarif"><input type="text" id="full_mardi_soir" name="full_mardi_soir" value="<?php echo( get_option('full_mardi_soir') ); ?>" /> <span>€</span></td>
            </tr>
            <tr class="item-tarif-day">
                <td class="day">Mercredi</td>
                <td class="tarif"><input type="text" id="full_mercredi_midi" name="full_mercredi_midi" value="<?php echo( get_option('full_mercredi_midi') ); ?>" /> <span>€</span></td>
                <td class="tarif"><input type="text" id="full_mercredi_soir" name="full_mercredi_soir" value="<?php echo( get_option('full_mercredi_soir') ); ?>" /> <span>€</span></td>
            </tr>
            <tr class="item-tarif-day">
                <td class="day">Jeudi</td>
                <td class="tarif"><input type="text" id="full_jeudi_midi" name="full_jeudi_midi" value="<?php echo( get_option('full_jeudi_midi') ); ?>" /> <span>€</span></td>
                <td class="tarif"><input type="text" id="full_jeudi_soir" name="full_jeudi_soir" value="<?php echo( get_option('full_jeudi_soir') ); ?>" /> <span>€</span></td>
            </tr>
            <tr class="item-tarif-day">
                <td class="day">Vendredi</td>
                <td class="tarif"><input type="text" id="full_vendredi_midi" name="full_vendredi_midi" value="<?php echo( get_option('full_vendredi_midi') ); ?>" /> <span>€</span></td>
                <td class="tarif"><input type="text" id="full_vendredi_soir" name="full_vendredi_soir" value="<?php echo( get_option('full_vendredi_soir') ); ?>" /> <span>€</span></td>
            </tr>
            <tr class="item-tarif-day">
                <td class="day">Samedi</td>
                <td class="tarif"><input type="text" id="full_samedi_midi" name="full_samedi_midi" value="<?php echo( get_option('full_samedi_midi') ); ?>" /> <span>€</span></td>
                <td class="tarif"><input type="text" id="full_samedi_soir" name="full_samedi_soir" value="<?php echo( get_option('full_samedi_soir') ); ?>" /> <span>€</span></td>
            </tr>
            <tr class="item-tarif-day">
                <td class="day">Dimanche</td>
                <td class="tarif"><input type="text" id="full_dimanche_midi" name="full_dimanche_midi" value="<?php echo( get_option('full_dimanche_midi') ); ?>" /> <span>€</span></td>
                <td class="tarif"><input type="text" id="full_dimanche_soir" name="full_dimanche_soir" value="<?php echo( get_option('full_dimanche_soir') ); ?>" /> <span>€</span></td>
            </tr>
        </tbody>
    </table>

    <?php
} // END => custom_field_tarif_full_boissons
