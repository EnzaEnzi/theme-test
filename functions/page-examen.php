<?php

/*
Name:   Page examen
Description: Page horaire d'examen
Author: Enza Lombardo
Author URI: www.enzalombardo.be
copyright : 2019 © Enza Lombardo
Version: 1.2
*/

/*  ----- Table des matière -----

        I - ADD MENU PAGE
           1. initialisation de la page
           2. construire la page
        II - THEME PAGE
        III - SETTING SECTION AND FIED
           1. initialisation des paramattre
           2. contruire des paramettres
              * register
              * settings
              * fields
        IV - FIELD CALLBACK
           1. callback -> settings
           2. callback -> fields
*/


/* ----------------------------------------- */
/* -----         ADD MENU PAGE         ----- */
/* ----------------------------------------- */

// initialisation de la page -----------------------------
add_action('admin_menu', 'add_page_examen');


// construire la page -----------------------------
function add_page_examen(){

    // Menu 1er niveau
    add_menu_page(
        'Examen',                              // page_title
        'Examen',                              // menu_title
        'manage_options',                      // capability
        'examen',                              // slug_menu
        'examen_theme_create_page',            // function
        'dashicons-clock',                     // icon
        98                                     // position
    );

    // Menu 2e niveau
    add_submenu_page(
        'examen',                              // slug parent
        'Matière',                             // page_title
        'Matière',                             // menu_title
        'manage_options',                      // capability
        'matiere',                            // slug_menu
        'matiere_theme_page'                   // function
    );

}

/* -------------------------------------------- */
/* -----            THEME PAGE            ----- */
/* -------------------------------------------- */

// PAGE 1er NIVEAU -----------------------------
function examen_theme_create_page(){
    if ( ! current_user_can( 'manage_options' ) ) {
        wp_die( __( 'Vous ne disposez pas les autorisations suffisantes pour accéder à cette page.', 'examen' ) );
    }

    ?>

    <div class="wrap">
        <h2 class="wp-heading-inline">Horaire d'examen</h2>
        <?php settings_errors(); ?>

        <div class="form-table form-table-custom">
            <form class="form-custom"  method="post" action="options.php">

                <div class="form-table form-table-custom" >
                    <?php settings_fields( 'examen-group' );?>
                </div>

                <?php
                do_settings_sections( 'examen' );
                submit_button();
                ?>
            </form>

        </div>
    </div>

    <?php
}

// PAGE 2e NIVEAU ----------------------------------
function matiere_theme_page(){
    if ( ! current_user_can( 'manage_options' ) ) {
        wp_die( __( 'Vous ne disposez pas les autorisations suffisantes pour accéder à cette page.', 'matiere' ) );
    }
    ?>
    <div class="wrap">
        <h2 class="wp-heading-inline">Matière d'examen</h2>
        <?php settings_errors(); ?>

        <div class="form-table form-table-custom">
            <form class="form-custom"  method="post" action="options.php">

                <div class="form-table form-table-custom" >
                    <?php settings_fields( 'matiere-group' );?>
                </div>

                <?php
                do_settings_sections( 'matiere' );
                submit_button();
                ?>
            </form>

        </div>
    </div>


    <?php
}


/* ------------------------------------------ */
/* -----    SETTING SECTION AND FIED    ----- */
/* -----          PAGE LEVEL 1          ----- */
/* ------------------------------------------ */


/* ------------ initialisation des paramattre ------------ */

add_action('admin_init', 'examen_custom_settings');

/* ------------ contruire des paramettres ------------ */

function examen_custom_settings(){

    // ---------------------------------------
    //                  LUNDI
    // ---------------------------------------

    /*  -- MIDI -- */
    register_setting('examen-group', 'lundi_examen_midi');
    register_setting('examen-group', 'lundi_examen_midi_de');
    register_setting('examen-group', 'lundi_examen_midi_a');

    /*  -- SOIR -- */
    register_setting('examen-group', 'lundi_examen_soir');
    register_setting('examen-group', 'lundi_examen_soir_de');
    register_setting('examen-group', 'lundi_examen_soir_a');
    /*  -- FERMER-- */

    register_setting('examen-group', 'lundi_examen_fermer');


    // SETTINGS ------------------------------
    add_settings_section(
        'option-examen-lundi',                           // id
        __('Lundi', 'option-examen-lundi'),                     // title
        'option_examen',          // callback
        'examen'                // page
    );

    // FIELDS ------------------------------

    /* -- lundi_examen_midi -- */
    add_settings_field(
        'lundi-midi',                            // id
        __('Midi', 'option-examen-lundi'),     // title
        'custom_field_lundi_examen_midi',               // callback
        'examen',                               // page
        'option-examen-lundi'                          // section
    );

    /* -- lundi_examen_soir-- */
    add_settings_field(
        'lundi-soir',                            // id
        __('Soir', 'option-examen-lundi'),     // title
        'custom_field_lundi_examen_soir',               // callback
        'examen',                               // page
        'option-examen-lundi'                          // section
    );

    /* -- lundi_examen_fermer-- */
    add_settings_field(
        'lundi-fermer',                            // id
        __('Fermer', 'option-examen-lundi'),     // title
        'custom_field_lundi_examen_fermer',               // callback
        'examen',                               // page
        'option-examen-lundi'                          // section
    );

    // ---------------------------------------
    //                  MARDI
    // ---------------------------------------

    /*  -- MIDI -- */
    register_setting('examen-group', 'mardi_examen_midi');
    register_setting('examen-group', 'mardi_examen_midi_de');
    register_setting('examen-group', 'mardi_examen_midi_a');

    /*  -- SOIR -- */
    register_setting('examen-group', 'mardi_examen_soir');
    register_setting('examen-group', 'mardi_examen_soir_de');
    register_setting('examen-group', 'mardi_examen_soir_a');


    /*  -- FERMER-- */
    register_setting('examen-group', 'mardi_examen_fermer');

    // SETTINGS ------------------------------
    add_settings_section(
        'option-examen-mardi',                           // id
        __('Mardi', 'option-examen-mardi'),                     // title
        'option_examen_mardi',          // callback
        'examen'                // page
    );

    // FIELDS ------------------------------

    /* -- mardi_examen_midi -- */
    add_settings_field(
        'mardi-midi',                            // id
        __('Midi', 'option-examen-mardi'),     // title
        'custom_field_mardi_examen_midi',               // callback
        'examen',                               // page
        'option-examen-mardi'                          // section
    );

    /* -- mardi_examen_soir-- */
    add_settings_field(
        'mardi-soir',                            // id
        __('Soir', 'option-examen-mardi'),     // title
        'custom_field_mardi_examen_soir',               // callback
        'examen',                               // page
        'option-examen-mardi'                          // section
    );

    /* -- mardi_examen_fermer-- */
    add_settings_field(
        'mardi-fermer',                            // id
        __('Fermer', 'option-examen-mardi'),     // title
        'custom_field_mardi_examen_fermer',               // callback
        'examen',                               // page
        'option-examen-mardi'                          // section
    );



    // ---------------------------------------
    //               MERCREDI
    // ---------------------------------------
    /*  -- MIDI -- */
    register_setting('examen-group', 'mercredi_examen_midi');
    register_setting('examen-group', 'mercredi_examen_midi_de');
    register_setting('examen-group', 'mercredi_examen_midi_a');

    /*  -- SOIR -- */
    register_setting('examen-group', 'mercredi_examen_soir');
    register_setting('examen-group', 'mercredi_examen_soir_de');
    register_setting('examen-group', 'mercredi_examen_soir_a');

    /*  -- FERMER-- */
    register_setting('examen-group', 'mercredi_examen_fermer');



    // SETTINGS ------------------------------
    add_settings_section(
        'option-examen-mercredi',                           // id
        __('Mercredi', 'option-examen-mercredi'),                     // title
        'option_examen_mercredi',          // callback
        'examen'                // page
    );

    // FIELDS ------------------------------

    /* -- mercredi_examen_midi -- */
    add_settings_field(
        'mercredi-midi',                            // id
        __('Midi', 'option-examen-mercredi'),     // title
        'custom_field_mercredi_examen_midi',               // callback
        'examen',                               // page
        'option-examen-mercredi'                          // section
    );

    /* -- mercredi_examen_soir-- */
    add_settings_field(
        'mercredi-soir',                            // id
        __('Soir', 'option-examen-mercredi'),     // title
        'custom_field_mercredi_examen_soir',               // callback
        'examen',                               // page
        'option-examen-mercredi'                          // section
    );

    /* -- mercredi_examen_fermer-- */
    add_settings_field(
        'mercredi-fermer',                            // id
        __('Fermer', 'option-examen-mercredi'),     // title
        'custom_field_mercredi_examen_fermer',               // callback
        'examen',                               // page
        'option-examen-mercredi'                          // section
    );



    // ---------------------------------------
    //                JEUDI
    // ---------------------------------------

    /*  -- MIDI -- */
    register_setting('examen-group', 'jeudi_examen_midi');
    register_setting('examen-group', 'jeudi_examen_midi_de');
    register_setting('examen-group', 'jeudi_examen_midi_a');

    /*  -- SOIR -- */
    register_setting('examen-group', 'jeudi_examen_soir');
    register_setting('examen-group', 'jeudi_examen_soir_de');
    register_setting('examen-group', 'jeudi_examen_soir_a');

    /*  -- FERMER-- */
    register_setting('examen-group', 'jeudi_examen_fermer');



    // SETTINGS ------------------------------
    add_settings_section(
        'option-examen-jeudi',                           // id
        __('Jeudi', 'option-examen-jeudi'),                     // title
        'option_examen_jeudi',          // callback
        'examen'                // page
    );

    // FIELDS ------------------------------

    /* -- jeudi_examen_midi -- */
    add_settings_field(
        'jeudi-midi',                            // id
        __('Midi', 'option-examen-jeudi'),     // title
        'custom_field_jeudi_examen_midi',               // callback
        'examen',                               // page
        'option-examen-jeudi'                          // section
    );

    /* -- jeudi_examen_soir-- */
    add_settings_field(
        'jeudi-soir',                            // id
        __('Soir', 'option-examen-jeudi'),     // title
        'custom_field_jeudi_examen_soir',               // callback
        'examen',                               // page
        'option-examen-jeudi'                          // section
    );

    /* -- jeudi_examen_fermer-- */
    add_settings_field(
        'jeudi-fermer',                            // id
        __('Fermer', 'option-examen-jeudi'),     // title
        'custom_field_jeudi_examen_fermer',               // callback
        'examen',                               // page
        'option-examen-jeudi'                          // section
    );



    // ---------------------------------------
    //               VENDREDI
    // ---------------------------------------

    /*  -- MIDI -- */
    register_setting('examen-group', 'vendredi_examen_midi');
    register_setting('examen-group', 'vendredi_examen_midi_de');
    register_setting('examen-group', 'vendredi_examen_midi_a');

    /*  -- SOIR -- */
    register_setting('examen-group', 'vendredi_examen_soir');
    register_setting('examen-group', 'vendredi_examen_soir_de');
    register_setting('examen-group', 'vendredi_examen_soir_a');

    /*  -- FERMER-- */
    register_setting('examen-group', 'vendredi_examen_fermer');



    // SETTINGS ------------------------------
    add_settings_section(
        'option-examen-vendredi',                           // id
        __('Vendredi', 'option-examen-vendredi'),                     // title
        'option_examen_vendredi',          // callback
        'examen'                // page
    );

    // FIELDS ------------------------------

    /* -- vendredi_examen_midi -- */
    add_settings_field(
        'vendredi-midi',                            // id
        __('Midi', 'option-examen-vendredi'),     // title
        'custom_field_vendredi_examen_midi',               // callback
        'examen',                               // page
        'option-examen-vendredi'                          // section
    );

    /* -- vendredi_examen_soir-- */
    add_settings_field(
        'vendredi-soir',                            // id
        __('Soir', 'option-examen-vendredi'),     // title
        'custom_field_vendredi_examen_soir',               // callback
        'examen',                               // page
        'option-examen-vendredi'                          // section
    );

    /* -- vendredi_examen_fermer-- */
    add_settings_field(
        'vendredi-fermer',                            // id
        __('Fermer', 'option-examen-vendredi'),     // title
        'custom_field_vendredi_examen_fermer',               // callback
        'examen',                               // page
        'option-examen-vendredi'                          // section
    );


    // ---------------------------------------
    //                SAMEDI
    // ---------------------------------------

    /*  -- MIDI -- */
    register_setting('examen-group', 'samedi_examen_midi');
    register_setting('examen-group', 'samedi_examen_midi_de');
    register_setting('examen-group', 'samedi_examen_midi_a');

    /*  -- SOIR -- */
    register_setting('examen-group', 'samedi_examen_soir');
    register_setting('examen-group', 'samedi_examen_soir_de');
    register_setting('examen-group', 'samedi_examen_soir_a');

    /*  -- FERMER-- */
    register_setting('examen-group', 'samedi_examen_fermer');


    // SETTINGS ------------------------------
    add_settings_section(
        'option-examen-samedi',                           // id
        __('Samedi', 'option-examen-samedi'),                     // title
        'option_examen_samedi',          // callback
        'examen'                // page
    );

    // FIELDS ------------------------------

    /* -- samedi_examen_midi -- */
    add_settings_field(
        'samedi-midi',                            // id
        __('Midi', 'option-examen-samedi'),     // title
        'custom_field_samedi_examen_midi',               // callback
        'examen',                               // page
        'option-examen-samedi'                          // section
    );

    /* -- samedi_examen_soir-- */
    add_settings_field(
        'samedi-soir',                            // id
        __('Soir', 'option-examen-samedi'),     // title
        'custom_field_samedi_examen_soir',               // callback
        'examen',                               // page
        'option-examen-samedi'                          // section
    );

    /* -- samedi_examen_fermer-- */
    add_settings_field(
        'samedi-fermer',                            // id
        __('Fermer', 'option-examen-samedi'),     // title
        'custom_field_samedi_examen_fermer',               // callback
        'examen',                               // page
        'option-examen-samedi'                          // section
    );



    // ---------------------------------------
    //               DIMANCHE
    // ---------------------------------------

    /*  -- MIDI -- */
        register_setting('examen-group', 'dimanche_examen_midi');
        register_setting('examen-group', 'dimanche_examen_midi_de');
        register_setting('examen-group', 'dimanche_examen_midi_a');

        /*  -- SOIR -- */
        register_setting('examen-group', 'dimanche_examen_soir');
        register_setting('examen-group', 'dimanche_examen_soir_de');
        register_setting('examen-group', 'dimanche_examen_soir_a');

        /*  -- FERMER-- */
        register_setting('examen-group', 'dimanche_examen_fermer');


        // SETTINGS ------------------------------
        add_settings_section(
            'option-examen-dimanche',                           // id
            __('Dimanche', 'option-examen-dimanche'),                     // title
            'option_examen_dimanche',          // callback
            'examen'                // page
        );

        // FIELDS ------------------------------

        /* -- dimanche_examen_midi -- */
        add_settings_field(
            'dimanche-midi',                            // id
            __('Midi', 'option-examen-dimanche'),     // title
            'custom_field_dimanche_examen_midi',               // callback
            'examen',                               // page
            'option-examen-dimanche'                          // section
        );

        /* -- dimanche_examen_soir-- */
        add_settings_field(
            'dimanche-soir',                            // id
            __('Soir', 'option-examen-dimanche'),     // title
            'custom_field_dimanche_examen_soir',               // callback
            'examen',                               // page
            'option-examen-dimanche'                          // section
        );

        /* -- dimanche_examen_fermer-- */
        add_settings_field(
            'dimanche-fermer',                            // id
            __('Fermer', 'option-examen-dimanche'),     // title
            'custom_field_dimanche_examen_fermer',               // callback
            'examen',                               // page
            'option-examen-dimanche'                          // section
        );


}

/* ------------------------------------------ */
/* -----         FIELD CALLBACK         ----- */
/* -----          PAGE LEVEL 1          ----- */
/* ------------------------------------------ */


// ---------------------------------------------------------------------
//              LUNDI
// ---------------------------------------------------------------------
function option_examen(){

}

// callback [custom_field_lundi_examen_midi]
function custom_field_lundi_examen_midi(){
    $lundi_examen_midi_de = esc_attr( get_option('lundi_examen_midi_de') );
    $lundi_examen_midi_a = esc_attr( get_option('lundi_examen_midi_a') );

    ?>
    <span>
        <span style="margin-right: 15px;">de</span>
        <input type="time" id="lundi_examen_midi_de" name="lundi_examen_midi_de" value="<?php echo( get_option('lundi_examen_midi_de') ); ?>" />
    </span>
    <span>
        <span style="margin: 0 15px;">à</span>
        <input type="time" id="lundi_examen_midi_a" name="lundi_examen_midi_a" value="<?php echo( get_option('lundi_examen_midi_a') ); ?>" />
    </span>
    <?php
}

// callback [custom_field_lundi_examen_soir]
function custom_field_lundi_examen_soir(){
    $lundi_examen_soir_de = esc_attr( get_option('lundi_examen_soir_de') );
    $lundi_examen_soir_a = esc_attr( get_option('lundi_examen_soir_a') );

    ?>
    <span>
        <span style="margin-right: 15px;">de</span>
        <input type="time" id="lundi_examen_midi_de" name="lundi_examen_soir_de" value="<?php echo( get_option('lundi_examen_soir_de') ); ?>" />
    </span>
    <span>
        <span style="margin: 0 15px;">à</span>
        <input type="time" id="lundi_examen_soir_a" name="lundi_examen_soir_a" value="<?php echo( get_option('lundi_examen_soir_a') ); ?>" />
    </span>
    <?php
}

// callback [custom_field_lundi_examen_fermer]
function custom_field_lundi_examen_fermer(){
    $lundi_examen_fermer = esc_attr( get_option('lundi_examen_fermer') );

    ?>
        <input type="checkbox" id="lundi_examen_fermer" name="lundi_examen_fermer" value="1" <?php checked(1, get_option('lundi_examen_fermer'), true); ?> >
    <?php
}

// ---------------------------------------------------------------------
//              MARDI
// ---------------------------------------------------------------------

function option_examen_mardi(){

}

// callback [custom_field_mardi_examen_midi]
function custom_field_mardi_examen_midi(){
    $mardi_examen_midi_de = esc_attr( get_option('mardi_examen_midi_de') );
    $mardi_examen_midi_a = esc_attr( get_option('mardi_examen_midi_a') );

    ?>
    <span>
        <span style="margin-right: 15px;">de</span>
        <input type="time" id="mardi_examen_midi_de" name="mardi_examen_midi_de" value="<?php echo( get_option('mardi_examen_midi_de') ); ?>" />
    </span>
    <span>
        <span style="margin: 0 15px;">à</span>
        <input type="time" id="mardi_examen_midi_a" name="mardi_examen_midi_a" value="<?php echo( get_option('mardi_examen_midi_a') ); ?>" />
    </span>
    <?php
}

// callback [custom_field_mardi_examen_soir]
function custom_field_mardi_examen_soir(){
    $mardi_examen_soir_de = esc_attr( get_option('mardi_examen_soir_de') );
    $mardi_examen_soir_a = esc_attr( get_option('mardi_examen_soir_a') );

    ?>
    <span>
        <span style="margin-right: 15px;">de</span>
        <input type="time" id="mardi_examen_midi_de" name="mardi_examen_soir_de" value="<?php echo( get_option('mardi_examen_soir_de') ); ?>" />
    </span>
    <span>
        <span style="margin: 0 15px;">à</span>
        <input type="time" id="mardi_examen_soir_a" name="mardi_examen_soir_a" value="<?php echo( get_option('mardi_examen_soir_a') ); ?>" />
    </span>
    <?php
}

// callback [custom_field_mardi_examen_fermer]
function custom_field_mardi_examen_fermer(){
    $mardi_examen_fermer = esc_attr( get_option('mardi_examen_fermer') );

    ?>
        <input type="checkbox" id="mardi_examen_fermer" name="mardi_examen_fermer" value="1" <?php checked(1, get_option('mardi_examen_fermer'), true); ?> >
    <?php
}


// ---------------------------------------------------------------------
//              MERCREDI
// ---------------------------------------------------------------------

function option_examen_mercredi(){

}

// callback [custom_field_mercredi_examen_midi]
function custom_field_mercredi_examen_midi(){
    $mercredi_examen_midi_de = esc_attr( get_option('mercredi_examen_midi_de') );
    $mercredi_examen_midi_a = esc_attr( get_option('mercredi_examen_midi_a') );

    ?>
    <span>
        <span style="margin-right: 15px;">de</span>
        <input type="time" id="mercredi_examen_midi_de" name="mercredi_examen_midi_de" value="<?php echo( get_option('mercredi_examen_midi_de') ); ?>" />
    </span>
    <span>
        <span style="margin: 0 15px;">à</span>
        <input type="time" id="mercredi_examen_midi_a" name="mercredi_examen_midi_a" value="<?php echo( get_option('mercredi_examen_midi_a') ); ?>" />
    </span>
    <?php
}

// callback [custom_field_mercredi_examen_soir]
function custom_field_mercredi_examen_soir(){
    $mercredi_examen_soir_de = esc_attr( get_option('mercredi_examen_soir_de') );
    $mercredi_examen_soir_a = esc_attr( get_option('mercredi_examen_soir_a') );

    ?>
    <span>
        <span style="margin-right: 15px;">de</span>
        <input type="time" id="mercredi_examen_midi_de" name="mercredi_examen_soir_de" value="<?php echo( get_option('mercredi_examen_soir_de') ); ?>" />
    </span>
    <span>
        <span style="margin: 0 15px;">à</span>
        <input type="time" id="mercredi_examen_soir_a" name="mercredi_examen_soir_a" value="<?php echo( get_option('mercredi_examen_soir_a') ); ?>" />
    </span>
    <?php
}

// callback [custom_field_mercredi_examen_fermer]
function custom_field_mercredi_examen_fermer(){
    $mercredi_examen_fermer = esc_attr( get_option('mercredi_examen_fermer') );

    ?>
        <input type="checkbox" id="mercredi_examen_fermer" name="mercredi_examen_fermer" value="1" <?php checked(1, get_option('mercredi_examen_fermer'), true); ?> >
    <?php
}


// ---------------------------------------------------------------------
//              JEUDI
// ---------------------------------------------------------------------

function option_examen_jeudi(){

}

// callback [custom_field_jeudi_examen_midi]
function custom_field_jeudi_examen_midi(){
    $jeudi_examen_midi_de = esc_attr( get_option('jeudi_examen_midi_de') );
    $jeudi_examen_midi_a = esc_attr( get_option('jeudi_examen_midi_a') );

    ?>
    <span>
        <span style="margin-right: 15px;">de</span>
        <input type="time" id="jeudi_examen_midi_de" name="jeudi_examen_midi_de" value="<?php echo( get_option('jeudi_examen_midi_de') ); ?>" />
    </span>
    <span>
        <span style="margin: 0 15px;">à</span>
        <input type="time" id="jeudi_examen_midi_a" name="jeudi_examen_midi_a" value="<?php echo( get_option('jeudi_examen_midi_a') ); ?>" />
    </span>
    <?php
}

// callback [custom_field_jeudi_examen_soir]
function custom_field_jeudi_examen_soir(){
    $jeudi_examen_soir_de = esc_attr( get_option('jeudi_examen_soir_de') );
    $jeudi_examen_soir_a = esc_attr( get_option('jeudi_examen_soir_a') );

    ?>
    <span>
        <span style="margin-right: 15px;">de</span>
        <input type="time" id="jeudi_examen_midi_de" name="jeudi_examen_soir_de" value="<?php echo( get_option('jeudi_examen_soir_de') ); ?>" />
    </span>
    <span>
        <span style="margin: 0 15px;">à</span>
        <input type="time" id="jeudi_examen_soir_a" name="jeudi_examen_soir_a" value="<?php echo( get_option('jeudi_examen_soir_a') ); ?>" />
    </span>
    <?php
}

// callback [custom_field_jeudi_examen_fermer]
function custom_field_jeudi_examen_fermer(){
    $jeudi_examen_fermer = esc_attr( get_option('jeudi_examen_fermer') );

    ?>
        <input type="checkbox" id="jeudi_examen_fermer" name="jeudi_examen_fermer" value="1" <?php checked(1, get_option('jeudi_examen_fermer'), true); ?> >
    <?php
}


// ---------------------------------------------------------------------
//              VENDREDI
// ---------------------------------------------------------------------

function option_examen_vendredi(){

}

// callback [custom_field_vendredi_examen_midi]
function custom_field_vendredi_examen_midi(){
    $vendredi_examen_midi_de = esc_attr( get_option('vendredi_examen_midi_de') );
    $vendredi_examen_midi_a = esc_attr( get_option('vendredi_examen_midi_a') );

    ?>
    <span>
        <span style="margin-right: 15px;">de</span>
        <input type="time" id="vendredi_examen_midi_de" name="vendredi_examen_midi_de" value="<?php echo( get_option('vendredi_examen_midi_de') ); ?>" />
    </span>
    <span>
        <span style="margin: 0 15px;">à</span>
        <input type="time" id="vendredi_examen_midi_a" name="vendredi_examen_midi_a" value="<?php echo( get_option('vendredi_examen_midi_a') ); ?>" />
    </span>
    <?php
}

// callback [custom_field_vendredi_examen_soir]
function custom_field_vendredi_examen_soir(){
    $vendredi_examen_soir_de = esc_attr( get_option('vendredi_examen_soir_de') );
    $vendredi_examen_soir_a = esc_attr( get_option('vendredi_examen_soir_a') );

    ?>
    <span>
        <span style="margin-right: 15px;">de</span>
        <input type="time" id="vendredi_examen_midi_de" name="vendredi_examen_soir_de" value="<?php echo( get_option('vendredi_examen_soir_de') ); ?>" />
    </span>
    <span>
        <span style="margin: 0 15px;">à</span>
        <input type="time" id="vendredi_examen_soir_a" name="vendredi_examen_soir_a" value="<?php echo( get_option('vendredi_examen_soir_a') ); ?>" />
    </span>
    <?php
}

// callback [custom_field_vendredi_examen_fermer]
function custom_field_vendredi_examen_fermer(){
    $vendredi_examen_fermer = esc_attr( get_option('vendredi_examen_fermer') );

    ?>
        <input type="checkbox" id="vendredi_examen_fermer" name="vendredi_examen_fermer" value="1" <?php checked(1, get_option('vendredi_examen_fermer'), true); ?> >
    <?php
}





// ---------------------------------------------------------------------
//              SAMEDI
// ---------------------------------------------------------------------

function option_examen_samedi(){

}

// callback [custom_field_samedi_examen_midi]
function custom_field_samedi_examen_midi(){
    $samedi_examen_midi_de = esc_attr( get_option('samedi_examen_midi_de') );
    $samedi_examen_midi_a = esc_attr( get_option('samedi_examen_midi_a') );

    ?>
    <span>
        <span style="margin-right: 15px;">de</span>
        <input type="time" id="samedi_examen_midi_de" name="samedi_examen_midi_de" value="<?php echo( get_option('samedi_examen_midi_de') ); ?>" />
    </span>
    <span>
        <span style="margin: 0 15px;">à</span>
        <input type="time" id="samedi_examen_midi_a" name="samedi_examen_midi_a" value="<?php echo( get_option('samedi_examen_midi_a') ); ?>" />
    </span>
    <?php
}

// callback [custom_field_samedi_examen_soir]
function custom_field_samedi_examen_soir(){
    $samedi_examen_soir_de = esc_attr( get_option('samedi_examen_soir_de') );
    $samedi_examen_soir_a = esc_attr( get_option('samedi_examen_soir_a') );

    ?>
    <span>
        <span style="margin-right: 15px;">de</span>
        <input type="time" id="samedi_examen_midi_de" name="samedi_examen_soir_de" value="<?php echo( get_option('samedi_examen_soir_de') ); ?>" />
    </span>
    <span>
        <span style="margin: 0 15px;">à</span>
        <input type="time" id="samedi_examen_soir_a" name="samedi_examen_soir_a" value="<?php echo( get_option('samedi_examen_soir_a') ); ?>" />
    </span>
    <?php
}

// callback [custom_field_samedi_examen_fermer]
function custom_field_samedi_examen_fermer(){
    $samedi_examen_fermer = esc_attr( get_option('samedi_examen_fermer') );

    ?>
        <input type="checkbox" id="samedi_examen_fermer" name="samedi_examen_fermer" value="1" <?php checked(1, get_option('samedi_examen_fermer'), true); ?> >
    <?php
}



// ---------------------------------------------------------------------
//              DIMANCHE
// ---------------------------------------------------------------------

function option_examen_dimanche(){

}

// callback [custom_field_dimanche_examen_midi]
function custom_field_dimanche_examen_midi(){
    $dimanche_examen_midi_de = esc_attr( get_option('dimanche_examen_midi_de') );
    $dimanche_examen_midi_a = esc_attr( get_option('dimanche_examen_midi_a') );

    ?>
    <span>
        <span style="margin-right: 15px;">de</span>
        <input type="time" id="dimanche_examen_midi_de" name="dimanche_examen_midi_de" value="<?php echo( get_option('dimanche_examen_midi_de') ); ?>" />
    </span>
    <span>
        <span style="margin: 0 15px;">à</span>
        <input type="time" id="dimanche_examen_midi_a" name="dimanche_examen_midi_a" value="<?php echo( get_option('dimanche_examen_midi_a') ); ?>" />
    </span>
    <?php
}

// callback [custom_field_dimanche_examen_soir]
function custom_field_dimanche_examen_soir(){
    $dimanche_examen_soir_de = esc_attr( get_option('dimanche_examen_soir_de') );
    $dimanche_examen_soir_a = esc_attr( get_option('dimanche_examen_soir_a') );

    ?>
    <span>
        <span style="margin-right: 15px;">de</span>
        <input type="time" id="dimanche_examen_midi_de" name="dimanche_examen_soir_de" value="<?php echo( get_option('dimanche_examen_soir_de') ); ?>" />
    </span>
    <span>
        <span style="margin: 0 15px;">à</span>
        <input type="time" id="dimanche_examen_soir_a" name="dimanche_examen_soir_a" value="<?php echo( get_option('dimanche_examen_soir_a') ); ?>" />
    </span>
    <?php
}

// callback [custom_field_dimanche_examen_fermer]
function custom_field_dimanche_examen_fermer(){
    $dimanche_examen_fermer = esc_attr( get_option('dimanche_examen_fermer') );

    ?>
        <input type="checkbox" id="dimanche_examen_fermer" name="dimanche_examen_fermer" value="1" <?php checked(1, get_option('dimanche_examen_fermer'), true); ?> >
    <?php
}


/* ------------------------------------------ */
/* -----    SETTING SECTION AND FIED    ----- */
/* -----          PAGE LEVEL 2          ----- */
/* ------------------------------------------ */

// initialisation des paramattre -----------------
add_action('admin_init', 'matiere_custom_settings');

function matiere_custom_settings(){

    // REGISTER ------------------------------
    register_setting('matiere-group', 'matiere_adresse');
    register_setting('matiere-group', 'matiere_map');
    register_setting('matiere-group', 'matiere_phone');
    register_setting('matiere-group', 'matiere_facebook');
    register_setting('matiere-group', 'matiere_twitter');
    register_setting('matiere-group', 'matiere_instagram');
    register_setting('matiere-group', 'matiere_visa');
    register_setting('matiere-group', 'matiere_mastercard');
    register_setting('matiere-group', 'matiere_american');
    register_setting('matiere-group', 'matiere_maestro');

    // SETTINGS ------------------------------
    /* --- Section 1 --- */
    add_settings_section(
        'section-matiere-location',                           // id
        __('Coordonnée', 'section-matiere-location'),         // title
        'option_section_matiere_location',                    // callback
        'matiere'                                             // page
    );

    /* --- Section 2 --- */
    add_settings_section(
        'section-matiere-social',                             // id
        __('Réseau sociaux', 'section-matiere-social'),       // title
        'option_section_matiere_social',                      // callback
        'matiere'                                             // page
    );

    /* --- Section 3 --- */
    add_settings_section(
        'section-matiere-carte',                             // id
        __('Mode de paiement', 'section-matiere-carte'),       // title
        'option_section_matiere_carte',                      // callback
        'matiere'                                             // page
    );

    // FIELDS --------------------------------
    /* --- matiere_adresse --- */
    add_settings_field(
        'matiere-adresse',                             // id
        __('Adresse', 'section-matiere-location'),     // title
        'custom_field_matiere_adresse',                // callback
        'matiere',                                     // page
        'section-matiere-location'                     // section
    );

    /* --- matiere_map --- */
    add_settings_field(
        'matiere-map',                                   // id
        __('Google map', 'section-matiere-location'),    // title
        'custom_field_matiere_map',                      // callback
        'matiere',                                       // page
        'section-matiere-location'                       // section
    );

    /* --- matiere_phone --- */
    add_settings_field(
        'matiere-phone',                               // id
        __('Telephone', 'section-matiere-location'),     // title
        'custom_field_matiere_phone',                  // callback
        'matiere',                                     // page
        'section-matiere-location'                     // section
    );

    /* --- matiere_facebook --- */
    add_settings_field(
        'matiere-facebook',                            // id
        __('URL compte Facebook', 'section-matiere-social'),       // title
        'custom_field_matiere_facebook',               // callback
        'matiere',                                     // page
        'section-matiere-social'                       // section
    );

    /* --- matiere_twitter --- */
    add_settings_field(
        'matiere-twitter',                             // id
        __('URL compte Twitter', 'section-matiere-social'),      // title
        'custom_field_matiere_twitter',               // callback
        'matiere',                                    // page
        'section-matiere-social'                      // section
    );

    /* --- matiere_instagram --- */
    add_settings_field(
        'matiere-instagram',                            // id
        __('URL compte Instagram', 'section-matiere-social'),        // title
        'custom_field_matiere_instagram',               // callback
        'matiere',                                      // page
        'section-matiere-social'                        // section
    );

    /* --- matiere_instagram --- */
    add_settings_field(
        'matiere-carte',                            // id
        __('Carte', 'section-matiere-carte'),        // title
        'custom_field_matiere_carte',               // callback
        'matiere',                                      // page
        'section-matiere-carte'                        // section
    );

}


/* ------------------------------------------ */
/* -----         FIELD CALLBACK         ----- */
/* -----          PAGE LEVEL 2          ----- */
/* ------------------------------------------ */

// callback 1er section
function option_section_matiere_location(){

}

// callback 2e section
function option_section_matiere_social(){

}

// callback 3e section
function option_section_matiere_carte(){

}

// callback field
function custom_field_matiere_adresse(){
    $matiere_adresse = esc_attr( get_option('matiere_adresse') );
    ?>
        <input type="text" id="matiere_adresse" name="matiere_adresse" value="<?php echo(get_option('matiere_adresse')); ?>" />
    <?php
}

// callback field
function custom_field_matiere_map(){
    $matiere_map = esc_attr( get_option('matiere_map') );
    ?>
        <textarea name="matiere_map" id="matiere_map" cols="80"><?php echo esc_attr(get_option('matiere_map')); ?></textarea>
    <?php
}


// callback field
function custom_field_matiere_phone(){
    $matiere_phone = esc_attr( get_option('matiere_phone') );
    ?>
        <input type="text" id="matiere_phone" name="matiere_phone" value="<?php echo(get_option('matiere_phone')); ?>" />
    <?php
}


// callback field
function custom_field_matiere_facebook(){
    $matiere_facebook = esc_attr( get_option('matiere_facebook') );
    ?>
        <input type="text" id="matiere_facebook" name="matiere_facebook" value="<?php echo(get_option('matiere_facebook')); ?>" />
    <?php
}

// callback field
function custom_field_matiere_twitter(){
    $matiere_twitter = esc_attr( get_option('matiere_twitter') );
    ?>
        <input type="text" id="matiere_twitter" name="matiere_twitter" value="<?php echo(get_option('matiere_twitter')); ?>" />
    <?php
}

// callback field
function custom_field_matiere_instagram(){
    $matiere_instagram = esc_attr( get_option('matiere_instagram') );
    ?>
        <input type="text" id="matiere_instagram" name="matiere_instagram" value="<?php echo(get_option('matiere_instagram')); ?>" />
    <?php
}



function custom_field_matiere_carte(){
    $matiere_visa = esc_attr( get_option('matiere_visa') );
    $matiere_mastercard = esc_attr( get_option('matiere_mastercard') );
    $matiere_american = esc_attr( get_option('matiere_american') );
    $matiere_maestro = esc_attr( get_option('matiere_maestro') );

    ?>
        <div>
            <input type="checkbox" id="matiere_visa" name="matiere_visa" value="1" <?php checked(1, get_option('matiere_visa'), true); ?> />
            <span>Visa</span>
        </div>

        <div>
            <input type="checkbox" id="matiere_mastercard" name="matiere_mastercard" value="1" <?php checked(1, get_option('matiere_mastercard'), true); ?> />
            <span>MasterCard</span>
        </div>

        <div>
            <input type="checkbox" id="matiere_american" name="matiere_american" value="1" <?php checked(1, get_option('matiere_american'), true); ?> />
            <span>American Express</span>
        </div>

        <div>
            <input type="checkbox" id="matiere_maestro" name="matiere_maestro" value="1" <?php checked(1, get_option('matiere_maestro'), true); ?> />
            <span>Maestro</span>
        </div>

    <?php
}
