
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">

<div class="wrap" id="info-id">
    <h1>Info identité</h1>
    <?php settings_errors(); ?>
    <form method="post" action="options.php">
        <?php settings_fields( 'enza-settings-group' ); ?>
        <?php do_settings_sections( 'enza-settings-group' ); ?>
        <table class="form-table" id="style-option-page">
            <tr >
                <th scope="row">Nom</th>
                <td>
                    <input type="text" name="nom" value="<?php echo esc_attr( get_option('nom') ); ?>" />
                </td>
            </tr>

            <tr >
                <th scope="row">Prénom</th>
                <td>
                    <input type="text" name="prenom" value="<?php echo esc_attr( get_option('prenom') ); ?>" />
                </td>
            </tr>

            <tr >
                <th scope="row">Age</th>
                <td>
                    <input type="text" name="age" value="<?php echo esc_attr( get_option('age') ); ?>" />
                </td>
            </tr>
        </table>
    </form>
</div>

<?php submit_button(); ?>
