<?php

// settings_page() displays the page content for the Enza Réglages submenu
function title_accueil() {

	register_setting( 'enza-settings-group', 'my_name' );
	register_setting( 'enza-settings-group', 'first_my_name' );

?>

<div>
	<h4>Coordonnés</h4>
	<div>
		<label for="my_name">Nom</label>
		<input id="my_name" type="text" my_name="my_name" value="<?php echo esc_attr( get_option('my_name') ); ?>"  />
	</div>
	<div>
		<label for="first_my_name">Prénom</label>
		<input id="first_my_name" type="text" my_name="first_my_name" value="<?php echo esc_attr( get_option('first_my_name') ); ?>"  />
	</div>
</div>

<?php submit_button(); ?>


<?php

}
