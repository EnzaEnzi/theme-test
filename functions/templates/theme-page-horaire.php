<div class="wrap">
    <?php
    if ( ! current_user_can( 'manage_options' ) ) {
        wp_die( __( 'Vous ne disposez pas des autorisations suffisantes pour accéder à cette page.', 'info-general' ) );
    }


    ?>

    <h1 class="wp-heading-inline">Les horaires du restaurant</h1>

    <?php settings_errors(); ?>
    <form method="post" action="options.php">
        <?php
        settings_fields( 'horaire-group' );
        do_settings_sections( 'horaires' );
        submit_button();
        ?>
    </form>
</div>
