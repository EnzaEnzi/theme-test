<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">

<div class="wrap wrap-horaire">
    <h1>Heure d'ouverture</h1>
    <form method="post" action="options.php">
        <?php settings_fields( 'horaire-settings-group' ); ?>
        <?php do_settings_sections( 'horaire-settings-group' ); ?>
        <table class="form-table" id="style-option-page">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">

    <table class="table table-bordered">
        <thead>
            <tr>
                <th scope="col">Jour</th>
                <th scope="col">Service du Midi</th>
                <th scope="col">Service du Soir</th>
                <th scope="col">Fermeture</th>
            </tr>
        </thead>
        <tbody>
            <tr class="item-day">
                <td class="day">Lundi</td>
                <td>
                    <span>
                        <label for="lundi_midi_de"> de </label>
                        <input id="lundi_midi_de" type="time" name="lundi_midi_de" value="<?php echo esc_attr( get_option('lundi_midi_de')); ?>"/>
                    </span>
                    <span>
                        <label for="lundi_midi_a"> à </label>
                        <input id="lundi_midi_a" type="time" name="lundi_midi_a" value="<?php echo esc_attr( get_option('lundi_midi_a')); ?>"/>
                    </span>
                </td>
                <td>
                    <span>
                        <label for="lundi_soir_de"> de </label>
                        <input id="lundi_soir_de" type="time" name="lundi_soir_de" value="<?php echo esc_attr( get_option('lundi_soir_de')); ?>"/>
                    </span>
                    <span>
                        <label for="lundi_soir_a"> à </label>
                        <input id="lundi_soir_a" type="time" name="lundi_soir_a" value="<?php echo esc_attr( get_option('lundi_soir_a'));; ?>"/>
                    </span>
                </td>
                <td></td>
            </tr><!-- ./ itemday -->

        </tbody>
    </table>
    </form>
</div>

<?php submit_button(); ?>
