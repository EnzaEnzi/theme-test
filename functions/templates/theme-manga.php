

	<?php settings_errors(); ?>
	<form method="post" action="options.php">
		<?php settings_fields( 'manga-setting-group' );?>
	 	<?php settings_fields( 'auteur-setting-group' );?>
		<?php settings_fields( 'anime-setting-group' ); ?>

		<?php do_settings_sections( 'mangas' ); ?>
		<?php submit_button(); ?>
	</form>
