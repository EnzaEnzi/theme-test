<div class="wrap">
    <?php
    if ( ! current_user_can( 'manage_options' ) ) {
        wp_die( __( 'Vous ne disposez pas des autorisations suffisantes pour accéder à cette page.', 'info-general' ) );
    }


    ?>

    <h1 class="wp-heading-inline">Les informations du restaurant</h1>

    <?php settings_errors(); ?>
    <form class="form-custom" method="post" action="options.php">
        <?php
        // settings_fields( 'location-group' );
        // // settings_fields( 'social-group' );
        // do_settings_sections( 'info-general' );
        ?>

        <div class="form-table form-table-custom" >
            <?php settings_fields( 'location-group' );?>
        </div>

        <div class="form-table form-table-custom" >
            <?php settings_fields( 'social-group' );?>
        </div>

        <?php do_settings_sections( 'info-general' ); ?>
        <?php submit_button(); ?>
    </form>

</div>
