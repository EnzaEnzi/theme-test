<?php

/* -------------------------------------------------------------------------- */
/* ADD MENU PAGE */
/* -------------------------------------------------------------------------- */

// initialisation de la page ---------------------------------------------------
add_action("admin_menu", "add_new_menu_items");

// construire la page ----------------------------------------------------------
function add_new_menu_items(){
    add_menu_page(
        "Theme Options",
        "Theme Options",
        "manage_options",
        "theme-options",
        "theme_options_page",
        "",
        100
    );
} // end -> dd_new_menu_items

/* -------------------------------------------------------------------------- */
/* THEME PAGE */
/* -------------------------------------------------------------------------- */

function theme_options_page(){
    ?>
    <div class="wrap">
        <h2 class="wp-heading-inline">Upload file</h2>
        <div class="description">description de la page ici</div>
        <?php settings_errors(); ?>

        <form method="post" action="options.php" enctype="multipart/form-data">
            <div class="">
                <?php settings_fields("picture_group");?>
            </div>

            <?php
                do_settings_sections("theme-options");
                submit_button();
             ?>
        </form>
    </div>
    <?php
} // end -> theme_options_page


/* -------------------------------------------------------------------------- */
/* PAGE LEVEL 1 -->  SETTING SECTION AND FIED */
/* -------------------------------------------------------------------------- */

// initialisation des paramattre -----------------------------------------------
add_action("admin_init", "display_options");

// contruire des paramettres ---------------------------------------------------
function display_options(){

    /* --- SECTION --- */
    add_settings_section(
        'header_section', // ID
        __('Header Options', 'header_section'), // TITLE
        'display_header_options_content', // CALLBACK
        'theme-options' // PAGE
    ); // end -> section : header_section


    /* --- FIELDS --- */
    //add a new setting for file upload
    add_settings_field(
        'background_picture', // ID
        __('Picture File Upload', 'header_section'), // LABEL
        'background_form_element', // CALLBACK FUNCTION
        'theme-options', // MENU PAGE SLUG
        'header_section' // SECTION ID
    ); // end -> field : background_picture



    //register a callback which will retirve the url to the file and then save the value.
    //register_setting only saves the value attribute of the form field....if you need anything more than that then use the callback to find the value t be stored.
    register_setting(
        'picture_group',    // group (element display in the form)
        'background_picture', // field ID
        'handle_file_upload' // Callback function
    ); // end -> register : handle_file_upload

} // end -> display_options

function handle_file_upload($options){
    //check if user had uploaded a file and clicked save changes button
    if(!empty($_FILES["background_picture"]["tmp_name"])){
        $urls = wp_handle_upload($_FILES["background_picture"], array('test_form' => FALSE));
        $temp = $urls["url"];
        return $temp;
    } // end -> if(!empty($_FILES["background_picture"]["tmp_name"]))

    //no upload. old file url is the new value.
    return get_option("background_picture");

} // END => handle_file_upload


function display_header_options_content(){
    ?>
        <p>Mettre une description de la section ICI</p>
    <?php
}
function background_form_element(){
    //echo form element for file upload

    ?>
    <div class="">
        <input type="file" name="background_picture" id="background_picture" value="<?php echo get_option('background_picture'); ?>" />
        <?php echo get_option("background_picture"); ?>
    </div>
    <div class="">
        <img src="<?php echo get_option("background_picture"); ?>" alt="" style="width: 200px; height:200px;">
    </div>
    <?php
} // END => background_form_element
