
    <!-- ===================== DEBUT FOOTER ===================== -->
    <footer>
        <p><?php bloginfo('name'); ?> © 2019 |  Designed by <a href="http://enzalombardo.be/" target="_blank">Enza LOMBARDO</a></p>
    </footer>
    <!-- ===================== FIN FOOTER ===================== -->

    <?php wp_footer(); ?>
</body>
</html>
