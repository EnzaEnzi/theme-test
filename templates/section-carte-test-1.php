<section id="filter-carte" style="padding-top:5%; margin-bottom:3rem;">

    <div class="filters filter-button-group">
        </ul><!-- / -->
        <div class="row">
            <div class="btn-filter col-2" data-filter="*">
                <img src="<?php echo get_template_directory_uri().'/img/icon/icon-carte-menu.png;' ?>" alt=""><br />
                <span>ALL</span>
            </div>
            <div class="btn-filter col-2" data-filter=".porc">
                <img src="<?php echo get_template_directory_uri().'/img/icon/icon-carte-menu.png;' ?>" alt=""><br />
                <span>Menu à la carte</span>
            </div>
            <div class="btn-filter col-2" data-filter=".canard">
                <img src="<?php echo get_template_directory_uri().'/img/icon/icon-carte-france.png'; ?>" alt=""><br />
                <span>Cuisine française</span>
            </div>
            <div class="btn-filter col-2" data-filter=".max-pezzali">
                <img src="<?php echo get_template_directory_uri().'/img/icon/icon-carte-thai.png'; ?>" alt=""><br />
                <span>Cuisine thaïlandaise</span>
            </div>
            <div class="btn-filter col-2" data-filter=".cuisine-chinois">
                <img src="<?php echo get_template_directory_uri().'/img/icon/icon-carte-chine.png'; ?>" alt=""><br />
                <span>Cuisine Chinoise</span>
            </div>
            <div class="btn-filter col-2" data-filter=".specialite-maison">
                <img src="<?php echo get_template_directory_uri().'/img/icon/icon-carte-maison.png'; ?>" alt=""><br />
                <span>Spécialité maison</span>
            </div>
        </div>
    </div><!-- / .filters .filter-button-group -->

    <div class="content theme grid">
        <div class="single-content container"  style="">
        <?php
            wp_reset_postdata();

            $args = array(
                'post_type'      => 'albums',
                'posts_per_page' => 5,
                'orderby'        => 'id',
                'order'          => 'DESC'
            );
            $my_query = new WP_query($args);
            if($my_query->have_posts()) : while($my_query->have_posts()) : $my_query->the_post();
         ?>

         <div class="<?php echo get_post_meta($post->ID, 'slug_album', true); ?> grid-item col align-self-center">
                <h3><?php the_title(); ?></h3>
                <table>
                    <thead>
                        <tr class="titre">
                            <th class="td-numb">Num.</th>
                            <th class="td-nom">Nom</th>
                            <th class="td-price">Prix</th>
                        </tr>
                    </thead><!-- / -->
                    <tbody>
                        <?php
                            $repeatable_fields = get_post_meta($post->ID, 'repeatable_fields', true);
                            foreach ($repeatable_fields as $field) :

                                ?>
                                    <tr>
                                        <td><?php echo $field['numb']; ?></td>
                                        <td><?php echo $field['nom']; ?></td>
                                        <td><?php echo $field['price'];  ?></td>
                                    </tr>
                                <?php
                            endforeach;
                         ?>
                    </tbody><!-- / -->
                </table><!-- / -->
            </div>
                <?php endwhile; endif;  wp_reset_postdata(); ?>
         </div><!-- / .sigle-content .grid-item -->
    </div><!-- / .content .grid -->
</section><!-- / #filter-carte -->
