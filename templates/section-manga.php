<section>
    <h1>Fiche manga</h1>

    <div class="row">
        <div class="col-4">
            <div class="card">
                <div class="card-header">
                    <h2 class="card-title">info tome</h2>
                </div>
                <div class="card-body">
                    <table class="tabel table-bordered" style="width: 100%;">
                        <tbody>
                            <tr>
                                <th scope="row">Nom </th>
                                <td><?php echo esc_attr( get_option('nom_manga') ); ?></td>
                            </tr>
                            <tr>
                                <th scope="row">Auteur </th>
                                <td><?php echo esc_attr( get_option('auteur_manga') ); ?></td>
                            </tr>
                            <tr>
                                <th scope="row">Genre </th>
                                <td><?php echo esc_attr( get_option('genre_manga') ); ?></td>
                            </tr>
                            <tr>
                                <th scope="row">Statut </th>
                                <td><?php echo esc_attr( get_option('statut_manga') ); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <div class="card-header">
                    <h2 class="card-title">info anime</h2>
                </div>
                <div class="card-body">
                    <table class="tabel table-bordered" style="width: 100%;">
                        <tbody>
                            <tr>
                                <th scope="row">Le manga est-il anime </th>
                                <td><?php echo esc_attr( get_option('manga_anime') ); ?></td>
                            </tr>
                            <tr>
                                <th scope="row">Nombre d'épisode </th>
                                <td><?php echo esc_attr( get_option('episode_anime') ); ?></td>
                            </tr>
                            <tr>
                                <th scope="row">Sortie de m'anime</th>
                                <td><?php echo esc_attr( get_option('annee_anime') ); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <div class="card-header">
                    <h2 class="card-title">info auteur</h2>
                </div>
                <div class="card-body">
                    <table class="tabel table-bordered" style="width: 100%;">
                        <tbody>
                            <tr>
                                <th scope="row">Sexe </th>
                                <td><?php echo esc_attr( get_option('sexe_auteur') ); ?></td>
                            </tr>
                            <tr>
                                <th scope="row">Née le </th>
                                <td><?php echo esc_attr( get_option('birthday_auteur') ); ?></td>
                            </tr>
                            <tr>
                                <th scope="row">Agé de </th>
                                <td><?php echo esc_attr( get_option('age_auteur') ); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="card-body">
                    <table class="tabel">
                        <tbody>
                            <tr>
                                <th scope="row"></th>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
