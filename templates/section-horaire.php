<section style=" display:none; height: 30vh; margin-top: 1rem; margin-bottom: 1rem;">
    <h2>Horaire</h2>
    <p>Codex WordPress - Adding Administration Menus</p>
    <ul>
        <li>
            Lundi :
            <?php
                if(checked(1, get_option('lundi_examen_fermer'), false)){
                    ?>
                    <span>FERMER</span>
                    <?php
                }else{
                    ?>
                        <span><?php echo get_option('lundi_examen_midi_de'); ?> - <?php echo get_option('lundi_examen_midi_a'); ?></span> |
                        <span><?php echo get_option('lundi_examen_soir_de'); ?> - <?php echo get_option('lundi_examen_soir_a'); ?></span>
                    <?php
                }

             ?>
        </li>
        <li>
            mardi :
            <?php
                if(checked(1, get_option('mardi_examen_fermer'), false)){
                    ?>
                    <span>FERMER</span>
                    <?php
                }else{
                    ?>
                        <span><?php echo get_option('mardi_examen_midi_de'); ?> - <?php echo get_option('mardi_examen_midi_a'); ?></span> |
                        <span><?php echo get_option('mardi_examen_soir_de'); ?> - <?php echo get_option('mardi_examen_soir_a'); ?></span>
                    <?php
                }

             ?>

        </li>
        <li>
            Mercredi :
            <?php
                if(checked(1, get_option('mercredi_examen_fermer'), false)){
                    ?>
                    <span>FERMER</span>
                    <?php
                }else{
                    ?>
                        <span><?php echo get_option('mercredi_examen_midi_de'); ?> - <?php echo get_option('mercredi_examen_midi_a'); ?></span> |
                        <span><?php echo get_option('mercredi_examen_soir_de'); ?> - <?php echo get_option('mercredi_examen_soir_a'); ?></span>
                    <?php
                }

             ?>
        </li>
        <li>
            Jeudi :
            <?php
                if(checked(1, get_option('jeudi_examen_fermer'), false)){
                    ?>
                    <span>FERMER</span>
                    <?php
                }else{
                    ?>
                        <span><?php echo get_option('jeudi_examen_midi_de'); ?> - <?php echo get_option('jeudi_examen_midi_a'); ?></span> |
                        <span><?php echo get_option('jeudi_examen_soir_de'); ?> - <?php echo get_option('jeudi_examen_soir_a'); ?></span>
                    <?php
                }

             ?>
        </li>
        <li>
            Vendredi :
            <?php
                if(checked(1, get_option('vendredi_examen_fermer'), false)){
                    ?>
                    <span>FERMER</span>
                    <?php
                }else{
                    ?>
                        <span><?php echo get_option('vendredi_examen_midi_de'); ?> - <?php echo get_option('vendredi_examen_midi_a'); ?></span> |
                        <span><?php echo get_option('vendredi_examen_soir_de'); ?> - <?php echo get_option('vendredi_examen_soir_a'); ?></span>
                    <?php
                }

             ?>
        </li>
        <li>
            Samedi :
            <?php
                if(checked(1, get_option('samedi_examen_fermer'), false)){
                    ?>
                    <span>FERMER</span>
                    <?php
                }else{
                    ?>
                        <span><?php echo get_option('samedi_examen_midi_de'); ?> - <?php echo get_option('samedi_examen_midi_a'); ?></span> |
                        <span><?php echo get_option('samedi_examen_soir_de'); ?> - <?php echo get_option('samedi_examen_soir_a'); ?></span>
                    <?php
                }

             ?>
        </li>

        <li>
            Dimanche :
            <?php
                if(checked(1, get_option('dimanche_examen_fermer'), false)){
                    ?>
                    <span>FERMER</span>
                    <?php
                }else{
                    ?>
                        <span><?php echo get_option('dimanche_examen_midi_de'); ?> - <?php echo get_option('dimanche_examen_midi_a'); ?></span> |
                        <span><?php echo get_option('dimanche_examen_soir_de'); ?> - <?php echo get_option('dimanche_examen_soir_a'); ?></span>
                    <?php
                }

             ?>
        </li>
    </ul>
</section>
