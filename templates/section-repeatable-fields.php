<section style=" display:none;  margin-top: 1rem; margin-bottom: 1rem;">
    <h2>Test metabox repeatable fields</h2>

    <?php
        wp_reset_postdata();

        $args = array(
            'post_type' => 'albums',
            'posts_per_page' => -1,
            'orderby' => 'id'
        );
        $my_query = new WP_query($args);
        if($my_query->have_posts()) : while($my_query->have_posts()) : $my_query->the_post();
     ?>
    <div>
        <h4><?php the_title(); ?></h4>
         <table>
             <thead>
                 <tr>
                     <th>Numero</th>
                     <th>Nom</th>
                     <th>Prix</th>
                 </tr>
             </thead><!-- / -->
             <tbody>
                 <?php
                     $repeatable_fields = get_post_meta($post->ID, 'repeatable_fields', true);
                     foreach ($repeatable_fields as $field) :

                         ?>
                             <tr>
                                 <td><?php echo $field['numb']; ?></td>
                                 <td><?php echo $field['nom']; ?></td>
                                 <td><?php echo $field['price'];  ?></td>
                             </tr>
                         <?php
                     endforeach;
                  ?>
             </tbody><!-- / -->
         </table><!-- / -->


    </div>
    <?php //var_dump($field); ?>
    <?php endwhile; endif;  wp_reset_postdata(); ?>
</section>
