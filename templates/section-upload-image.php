<section id="test-upload-img">
    <h2>section : album</h2>

    <div class="row" style="border:1px solid red;">
        <div class="col-6">get_option('file_avatar');</div>
        <div class="col-3"><img style="width: 200px; height:200px;" src="<?php echo get_option('file_avatar'); ?>" alt="" /></div>
    </div>
    <div class="row" style="border:1px solid red;">
        <div class="col-6">get_template_directory().get_option('file_avatar');</div>
        <div class="col-3"><img style="width: 200px; height:200px;" src="<?php echo get_template_directory().get_option('file_avatar'); ?>" alt=""></div>
    </div>
    <div class="row" style="border:1px solid red;">
        <div class="col-6">get_option('file_avatar_albumpage'); if($avatar != ''){echo htmlspecialchars($avatar);}</div>
        <div class="col-3"><img style="width: 200px; height:200px;" src="<?php $avatar = get_option('file_avatar_albumpage'); if($avatar != ''){echo htmlspecialchars($avatar);}?>" alt=""></div>
    </div>
    <div class="row" style="border:1px solid red;">
        <div class="col-6">$avatar = get_option('file_avatar_albumpage'); if($avatar != ''){echo htmlspecialchars($avatar);}</div>
        <div class="col-3"><?php $avatar = get_option('file_avatar_albumpage'); if($avatar != ''){echo htmlspecialchars($avatar);}?></div>
    </div>
    <div class="row" style="border:1px solid red;">
        <div class="col-6">get_template_directory_uri().'/uploads/2019/04/'.get_option('file_avatar_albumpage');</div>
        <div class="col-3">
            <img style="width: 200px; height:200px;" src="<?php
                echo get_template_directory_uri()
                .'/uploads/2019/04/'
                . get_option('file_avatar_albumpage');
             ?>" alt="">
        </div>
    </div>
    <div class="row" style="border:1px solid red;">
        <div class="col-6">
            echo get_option("background_picture")
        </div>
        <div class="col-3">
            <img style="width: 200px; height:200px;" src="<?php echo get_option("background_picture"); ?>" alt="">
        </div>
    </div>
    <div class="row" style="border:1px solid red;">
        <div class="col-6">
            echo get_option("bg_image_theme")
        </div>
        <div class="col-3">
            <img style="width: 200px; height:200px;" src="<?php echo get_option("bg_image_theme"); ?>" alt="">
        </div>
    </div>



    <?php if(checked(1, get_option('display_bg_disney'), false)){ ?>
        <div class="row" style="border:1px solid red;">
            <div class="col-6">
                echo get_option("bg_disney)
            </div>
            <div class="col-3">
                <img style="width: 200px; height:200px;" src="<?php echo get_option("bg_disney"); ?>" alt="">
            </div>
        </div>
    <?php } else { ?>
        <strong>Pas d'image pour cette section</strong>
    <?php } ?>



    <?php if(checked(1, get_option('config_affiche_avatar'), false)){ ?>
        <div class="row">
            <div class="img col-3">
                <img src="<?php echo get_template_directory_uri().'/img/lotus.jpg' ?>" alt="">
            </div>
            <div class="col-9">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Quisque sapien velit, aliquet eget commodo nec, auctor a
                    sapien. Nam eu neque vulputate diam rhoncus faucibus.
                    Curabitur quis varius libero. Lorem.
                </p>
            </div>
        </div>
    <?php } else { ?>
        rien
    <?php } ?>

</section>
