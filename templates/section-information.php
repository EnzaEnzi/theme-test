<section style=" display:none; height: 40vh; margin-top: 1rem; margin-bottom: 1rem;">
    <h2>information</h2>
    <div class="row">
        <div class="col-7">
            <div class="">
                <h3>Coordonnée</h3>
                <ul>
                    <li><?php echo get_option( 'matiere_adresse' ); ?></li>
                    <li><?php echo get_option( 'matiere_phone' ); ?></li>
                </ul>
            </div>

            <div class="">
                <h3>Réseau sociaux</h3>
                <ul>
                    <li><a href="<?php echo get_option('matiere_facebook'); ?>" target="_blank">Facebook</a></li>
                    <li><a href="<?php echo get_option('matiere_twitter'); ?>" target="_blank">Twitter</a></li>
                    <li><a href="<?php echo get_option('matiere_instagram'); ?>" target="_blank">Instagram</a></li>
                </ul>
            </div>

            <div>
                <h3>Mode de paiement</h3>
                <ul>
                    <li>
                        <?php
                            if(checked(1, get_option('matiere_visa'), false)){
                                ?>
                                <span>VISA</span>
                                <?php
                            }
                         ?>
                    </li>

                    <li>
                        <?php
                            if(checked(1, get_option('matiere_mastercard'), false)){
                                ?>
                                <span>MASTERCARD</span>
                                <?php
                            }
                         ?>
                    </li>

                    <li>
                        <?php
                            if(checked(1, get_option('matiere_american'), false)){
                                ?>
                                <span>AMERICAN EXPRESS</span>
                                <?php
                            }
                         ?>
                    </li>

                    <li>
                        <?php
                            if(checked(1, get_option('matiere_maestro'), false)){
                                ?>
                                <span>MAESTRO</span>
                                <?php
                            }
                         ?>
                    </li>

                </ul>
            </div>
        </div>
        <div class="col-5">
            <iframe src="<?php echo get_option('matiere_map'); ?>" width="" height=""></iframe>
        </div>
    </div>
</section>
