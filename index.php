<?php get_header(); ?>

<!-- ===================== FIN HEADER ===================== -->
<main class="container" role="main">

    <?php //require get_template_directory() . '/templates/section-carte-test-1.php'; ?>

    <?php require get_template_directory() . '/templates/section-fitre-carte-test-2.php' ?>

    <?php require get_template_directory() . '/templates/section-information.php'; ?>

    <?php require get_template_directory() . '/templates/section-horaire.php'; ?>

    <?php require get_template_directory() . '/templates/section-manga.php'; ?>

    <?php require get_template_directory() . '/templates/section-repeatable-fields.php'; ?>

    <?php require get_template_directory() . '/templates/section-upload-image.php'; ?>


    <section id="config-test" style="background: aqua;">
        <h2>Config test</h2>

        <?php $desc = get_option('config_description'); if($desc != ''){echo htmlspecialchars($desc);}?>
        <br/><br/>
        <?php echo  esc_attr(get_option('config_description')); ?>
        <br/><br/>
    </section>

    <!-- START : section-banner -->
    <?php
        if(checked(1, get_option('banner_hiden'), false)){
            ?>
            <?php
        } else {
            ?>
            <section id="section-banner">
                <h1><?php echo get_option('banner_title'); ?></h1>
                <p><?php echo get_option('banner_description'); ?></p>

                <?php
                    if(checked(1, get_option('banner_btn_actif'), false)){

                        ?>
                            <div>
                                <a href=" href="<?php echo get_option('banner_btn_url'); ?>" target="_blank"">
                                    <?php echo get_option('banner_btn_txt'); ?>
                                </a>
                            </div>
                        <?php

                    }
                 ?>
            </section>
            <?php
        }
     ?>

     <!-- START : section-buffet -->
    <?php
        if(checked(1, get_option('buffet_hiden'), false)){
            ?>
            <?php
        } else {
            ?>
            <section id="section_buffet">
                <h1><?php echo get_option('buffet_title'); ?></h1>
                <p><?php echo get_option('buffet_description'); ?></p>

                <?php
                    if(checked(1, get_option('buffet_btn_actif'), false)){

                        ?>
                            <div>
                                <a href=" href="<?php echo get_option('buffet_btn_url'); ?>" target="_blank"">
                                    <?php echo get_option('buffet_btn_txt'); ?>
                                </a>
                            </div>
                        <?php

                    }
                 ?>
            </section>
            <?php
        }
     ?>

     <!-- START : section-carte -->
     <?php
         if(checked(1, get_option('carte_hiden'), false)){
             ?>
             <?php
         } else {
             ?>
             <section id="section-carte">
                 <h1><?php echo get_option('carte_title'); ?></h1>
                 <p><?php echo get_option('carte_description'); ?></p>

                 <?php
                     if(checked(1, get_option('carte_btn_actif'), false)){

                         ?>
                             <div>
                                 <a href=" href="<?php echo get_option('carte_btn_url'); ?>" target="_blank"">
                                     <?php echo get_option('carte_btn_txt'); ?>
                                 </a>
                             </div>
                         <?php

                     }
                  ?>
             </section>
             <?php
         }
      ?>

      <!-- START : section-event -->
      <?php
          if(checked(1, get_option('event_hiden'), false)){
              ?>
              <?php
          } else {
              ?>
              <section id="section-event">
                  <h1><?php echo get_option('event_title'); ?></h1>
                  <div class="row">
                      <?php
                          wp_reset_postdata();

                          $args = array(
                              'post_type' => 'albums',
                              'posts_per_page' => -1,
                              'orderby' => 'id'
                          );
                          $my_query = new WP_query($args);
                          if($my_query->have_posts()) : while($my_query->have_posts()) : $my_query->the_post();
                       ?>
                          <div class="col-4">
                              <h4><?php the_title(); ?></h4>
                              <table>
                                  <thead>
                                      <tr>
                                          <th>Num.</th>
                                          <th>Nom</th>
                                          <th>Prix</th>
                                      </tr>
                                  </thead><!-- / -->
                                  <tbody>
                                      <?php
                                          $repeatable_fields = get_post_meta($post->ID, 'repeatable_fields', true);
                                          foreach ($repeatable_fields as $field) :

                                              ?>
                                                  <tr>
                                                      <td><?php echo $field['numb']; ?></td>
                                                      <td><?php echo $field['nom']; ?></td>
                                                      <td><?php echo $field['price'];  ?></td>
                                                  </tr>
                                              <?php
                                          endforeach;
                                       ?>
                                  </tbody><!-- / -->
                              </table><!-- / -->
                          </div>

                      <?php endwhile; endif;  wp_reset_postdata(); ?>
                   </div>
              </section>
              <?php
          }
       ?>


</main>
<!-- ===================== FIN MAIN ===================== -->

<?php get_footer(); ?>
